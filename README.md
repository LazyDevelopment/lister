[![Coverity Scan Build Status](https://scan.coverity.com/projects/20990/badge.svg)](https://scan.coverity.com/projects/lister1)

## Lister
**Lister** is a console app, designed to help you manage your watch-lists.
 It uses SQLite DB file to store the data. All data except of names of
 watch-lists is encrypted and only you know the password: the app doesn't
 store it, so if you'll forget it, you'll loose access to your data.

Typical **use-case** is when you visit some site and read an interesting
artice, and that page contains some links to related articles that you
want to read too. Lister will help to store all such URLs to interesting
articles for you in the list and will let you read them later when you'll
have time for that.

## Functionality
Lister's UI is designed as a **wizard** letting you interactively navigate
to the steps, you need, and guiding you through the workflow. So while
it is console application, it stays user-frendly interactive wizard.

High level functionality is:

1. Lister is cross-platform application and will work on any system with
Java 15 or newer installed

2. It allows you to create, rename and delete the lists.

3. Each list has its regex - the common representation of all items,
stored there. Only strings that match to this regex, will be stored in
the list

4. Only unique items will be stored in the list. If you try to store an
item, which is already stored in the list or was there and you requested
it before, Lister will tell you that it is duplicate and won't store it

5. As I mentioned, each list has stored items that you want to get in
future and viewed items, that you already got from the list. It is crucial
because at some point you'll forget which items you viewed and which are
new, so you may go in circles storing and reading the same content. Lister
will not allow this case

6. You could import items from TXT file to Lister's list and items from
list to TXT file. The text file should contain items, separated by new
string char (that means, each item should be in its own string). Each
importing item will be validated using list's regex and only matching ones
will be stored. If you want to import also list of viewed items, you need
to store them in the file, named as file with not viewed items + ".viewed.txt"
*(example: you have your list of not viewed items in "c:\tmp\list.txt",
so the list of already viewed items should be in "c:\tmp\list.txt.viewed.txt")*
If you export items from the list, they will be stored in the TXT file,
path to which you'll provide and all viewed items - in the file with the
same name + ".viewed.txt"

7. And **the main function** is: if you switched to "Work with items" mode,
you could let the app running on the background and open your favorite
browser. If you want to get some item from the list, just copy corresponding
key word to system clipboard (which exactly - Lister will instruct you
when you'll switch to this mode), if you want to store some new item into
the list, just copy its URL to clipboard and Lister will try to match it
to list's regex and check whether it is unique. If it matches, Lister will
tell you whether it is stored or is duplicate, if not - Lister will just
ignore that string, so you still could use your system clipboard for your
needs - running on background Lister won't do unexpected and unwanted
things until it finds something, addressed to it. That is why you need to
provide good regex for the lists.

8. And when I wrote that Lister will tell you something, I meant that
exactly, as it will play corresponding sounds with some words and phrases
when running in "Work with items" mode

## Building the app
You could download already prepared and built version of the app from
this BitBucket repository from the **Downloads** section. There you'll
find different versions of the app and here is how to find appropriate one:

- Name starts with name of the app: "Lister-"

- Next the version comes: "1.3". That is the version of the development
branch, so typically it will contain builds under development and after
development is finished, stable build for the release

- That is why next sub-version is added to all builds under development:
"-SNAPSHOT". That means that if the file doesn't contain this special word,
it is stable

- "-jar-with-dependencies" means that this file contains all the data, it
needs to work (except of DB file, of course - it will be reused if exists
in the same folder or re-created empty)

- ".jar" - extention of the file points to file type: Java archive

Alternatively, you could **download source code** of repository, any
available tag or branch from the **Downloads** section or checkout any
commit from GIT. In this case you'll need to build an executable file from
source code on your system. To do that follow next guide:

1. Download and install latest available **Java 15 JDK** from:
[https://jdk.java.net/15/](<https://jdk.java.net/15/>)

2. Download and unzip to some folder **Apache Maven** from:
[https://maven.apache.org/download.cgi](<https://maven.apache.org/download.cgi>)

    *Tip: use some folder without version in name, for example
"c:\Program Files\maven" - that will simplify maven update in future*

3. Create or set next **system variables** (examples of values in brackets,
make sure to set all paths with spaces in ""):

    - *JAVA_HOME_15* = <path to installed JDK\> ("c:\Program Files\Java\jdk-15.0.1")

    - *JAVA_HOME* = %JAVA_HOME_15% (this will simplify switching between
      different versions of Java)

    - *M2_HOME* = <path to folder with extracted Maven\> ("c:\Program Files\maven")

    - *M2* = <path to Maven binaries\> (%M2_HOME%\bin)

    - Add next string to *Path* variable's value: ";%JAVA_HOME%\bin\\;%M2%\\"

4. Open new Command-line shell window (for example, CMD), navigate to
unzipped folder with this project (containing "pom.xml" file) and **run**
next command:

    **mvn clean verify**

    It will build the project and execute tests to verify that it was built
correctly.

5. Navigate to the *target* sub-directory and find
"Lister-<version\>-jar-with-dependencies.jar" - that is your built application

## Running the app
To execute the Lister navigate to the folder, where the jar is located and
**run next command**:

    java -jar Lister-<version>-jar-with-dependencies.jar

*You could create a script to run it. For example, Lister.bat with next content:*

    @echo off
    call java -jar Lister-1.0.jar
    pause

## JavaDoc
Repository contains the *JavaDock* folder, which contains built version
of HTML JavaDoc - documentation of all packages, classes and methods of
the app. Currently it is built manually, so it may be outdated while
version is under development, but should be correct when release is
prepared.

You could find more details about JavaDoc in Oracle documentation:
[https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javadoc.html](<https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javadoc.html>)

If you will want to rebuild it, make sure to include Overview Comment
File, located in root project directory, to the javadoc command:
[https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javadoc.html#overviewcomment](<https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javadoc.html#overviewcomment>)

## Coverity scan
This project is registered on Coverity (Click on badge on top of Readme
to go to build analysis results). To analyze new changes you have to be
assigned to this project on Coverity.
