package edu.lazydevelopment.lister.security;

import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.logic.ListerObjects;
import edu.lazydevelopment.lister.menu.MainMenu;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.AES256TextEncryptor;
import org.jetbrains.annotations.NotNull;
import org.passay.*;
import org.slf4j.Logger;

import java.io.Console;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Backgrounds.ANSI_BG_BLACK;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;

/**
 * Creates and authenticates existing user by validating typed password using encrypted string.
 */
public final class Login {

  private static final String SETTINGS = "_settings";
  private char[] pass;
  private char[] pass2 = null;

  /**
   * Reads password string from user and returns it as array of chars. Tries to use safe method to
   * type password without showing it in console, but if that is not possible, uses common reading
   * string method.
   *
   * @param isNew {@code true} if new user should be created; {@code false} if existing user is
   *              going to be authorized
   * @param lisOb instance of class with common objects
   * @return password as array of chars
   * @throws IOException if unable to get user input or user made too many errors
   */
  private char[] getPass(final boolean isNew, @NotNull final ListerObjects lisOb)
      throws IOException {
    final Console console = lisOb.getSysConsole();
    final ReadPasswordFromSysIn readPasswordFromSysIn = new ReadPasswordFromSysIn();

    if (isNew) {
      lisOb.getLogger().info("""
              You'll be asked for new password. Please do not use your passwords from other resources and try to create some unique, strong, not guessable but easy-to-remember password. Here are the rules, using which it will be validated:
              {}{}       - contain at least 10 symbols;
                     - contain at least 4 digits;
                     - contain at least 1 UPPERCASE char;
                     - contain at least 2 lowercase char;
                     - contain at least 1 non-alphanumeric char;
                     - not contain 3 or more repeated chars;
                     - not contain numeric sequence longer than 3 digits;
                     - not contain alphabetical sequence longer than 3 chars;
                     - not contain keyboard (qwerty) sequence longer than 3 chars;
              {}""".indent(0),
          ANSI_BRIGHT_WHITE, ANSI_BG_BLACK, ANSI_RESET);

      byte errcount = typePass(lisOb, readPasswordFromSysIn);
      if (errcount >= 10) {
        MainMenu.setFinish(true);
        throw new IOException("Too many errors. Goodbye!");
      }
      if (pass2 != null) {
        Arrays.fill(pass2, '1');
      }
      return pass;
    } else {
      return console == null ? readPasswordFromSysIn.getPassword(lisOb)
          : console.readPassword();
    }
  }

  /**
   * Asks user to type the password.
   *
   * @param lisOb                 instance of class with common objects
   * @param readPasswordFromSysIn instance of class for alternative interaction with console
   * @return # of user tries to type correct password
   * @throws IOException if unable to interact with console
   */
  private byte typePass(@NotNull final ListerObjects lisOb,
                        final ReadPasswordFromSysIn readPasswordFromSysIn) throws IOException {
    final Logger logger = lisOb.getLogger();
    final Console console = lisOb.getSysConsole();
    boolean ok;
    byte errcount = 0;
    do {
      logger.info("Type your password: ");
      pass = console == null ? readPasswordFromSysIn.getPassword(lisOb)
          : console.readPassword();
      if (pass == null || !passValidation(pass, logger)) {
        errcount++;
        ok = false;
        continue;
      }
      logger.info("Type your password 2nd time for verification: ");
      pass2 = console == null ? readPasswordFromSysIn.getPassword(lisOb)
          : console.readPassword();
      ok = Arrays.equals(pass, pass2);
      if (!ok) {
        logger.warn("Passwords don't match. Try again.");
        errcount++;
      }
    } while (errcount < 10 && !ok);
    return errcount;
  }

  /**
   * Password strength validation method. Valid password should:<br> - contain at least 10
   * symbols;<br> - contain at least 4 digits;<br> - contain at least 1 UPPERCASE char;<br> -
   * contain at least 2 lowercase char;<br> - contain at least 1 non-alphanumeric char;<br> - not
   * contain 3 or more repeated chars;<br> - not contain numeric sequence longer than 3 digits;<br>
   * - not contain alphabetical sequence longer than 3 chars;<br> - not contain keyboard (qwerty)
   * sequence longer than 3 chars;<br>
   *
   * @param pass   {@code char[]} password to be validated
   * @param logger to output usual messages an errors
   * @return {@code true} if password is valid; {@code false} if it is not
   */
  private boolean passValidation(@NotNull final char[] pass, final Logger logger) {
    List<Rule> ruleList = new ArrayList<>();

    LengthRule lengthRule = new LengthRule();
    lengthRule.setMinimumLength(10);
    ruleList.add(lengthRule);
    ruleList.add(new CharacterRule(EnglishCharacterData.Digit, 4));
    ruleList.add(new CharacterRule(EnglishCharacterData.UpperCase));
    ruleList.add(new CharacterRule(EnglishCharacterData.LowerCase, 2));
    ruleList.add(new CharacterRule(EnglishCharacterData.Special));
    ruleList.add(new RepeatCharacterRegexRule(3));
    ruleList.add(new IllegalSequenceRule(EnglishSequenceData.Numerical, 4, false));
    ruleList.add(new IllegalSequenceRule(EnglishSequenceData.Alphabetical, 4, false));
    ruleList.add(new IllegalSequenceRule(EnglishSequenceData.USQwerty, 4, false));

    PasswordValidator validator = new PasswordValidator(ruleList);
    StringBuilder s = new StringBuilder();
    for (char pas : pass) {
      s.append(pas);
    }
    final RuleResult result = validator.validate(new PasswordData(s.toString()));
    s = new StringBuilder();
    s.append("trash");

    if (result.isValid()) {
      logger.info("Strong password\n");
      return true;
    } else {
      logger.warn("Weak password:");
      for (String msg : validator.getMessages(result)) {
        logger.warn(msg);
      }
      logger.warn("\n");
      return false;
    }
  }

  /**
   * Creates new user by asking for password and then encrypting validation string using that
   * password and storing it in {@code _settings} table.
   *
   * @param lisOb instance of class with common objects
   * @return instance of Encryptor with password for this user
   * @throws SQLException in case of problems with DB
   * @throws IOException  in case of problems with user's shell
   */
  @NotNull
  public AES256TextEncryptor createLogin(@NotNull final ListerObjects lisOb)
      throws SQLException, IOException {
    final Read read = lisOb.getRead();
    final Store store = lisOb.getStore();

    if (!read.getTables(lisOb).contains(SETTINGS)
        && !store.createTableList(lisOb, SETTINGS)) {
      throw new SQLException("Unable to create settings DB");
    }

    AES256TextEncryptor textEncryptor = new AES256TextEncryptor();
    textEncryptor.setPasswordCharArray(getPass(true, lisOb));

    if (read.getPassValidationString(lisOb.getConnect(), null).isEmpty()
        && !store.storeItem(lisOb.getConnect(), "Validation string for password check",
        SETTINGS, textEncryptor)) {
      throw new SQLException("Unable to store pass phrase in settings DB");
    }
    return textEncryptor;
  }

  /**
   * Authorizes user by asking for password and trying to decrypt validation string using that
   * password.
   *
   * @param lisOb instance of class with common objects
   * @return instance of Encryptor with password for this user
   */
  public AES256TextEncryptor authorize(@NotNull final ListerObjects lisOb) {
    final Logger logger = lisOb.getLogger();
    boolean ok;
    byte errcount = 0;
    AES256TextEncryptor encryptor = new AES256TextEncryptor();

    try {
      do {
        ok = false;
        encryptor = new AES256TextEncryptor();
        logger.info("Type your password to login: ");
        pass = getPass(false, lisOb);
        if (pass != null && pass.length > 0) {
          encryptor.setPasswordCharArray(pass);
          ok = validatePass(lisOb, encryptor);
        }
        if (!ok) {
          errcount++;
        }
      } while (errcount < 10 && !ok);
      if (errcount >= 10) {
        MainMenu.setFinish(true);
        throw new IOException("Too many errors. Goodbye!");
      }
    } catch (Exception e) {
      MainMenu.setFinish(true);
      logger.error("Unable to validate password", e);
    }
    return encryptor;
  }

  /**
   * Validates typed password by trying to decrypt saved string.
   *
   * @param lisOb     instance of class with common objects
   * @param encryptor instance of Encryptor with password to be validated
   * @return {@code true} if password is valid and {@code false} if not
   * @throws SQLException in case of problems with DB
   */
  private boolean validatePass(@NotNull final ListerObjects lisOb,
                               final AES256TextEncryptor encryptor) throws SQLException {
    boolean ok;

    try {
      ok = lisOb.getRead().getPassValidationString(lisOb.getConnect(), encryptor)
          .equals("Validation string for password check");
    } catch (EncryptionOperationNotPossibleException ex) {
      ok = false;
    }
    return ok;
  }
}
