package edu.lazydevelopment.lister.security;

import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.Arrays;

/**
 * Backup implementation of reading password from user if safer method is not possible to use.<br>
 * Found in
 * <a href=http://www.cse.chalmers.se/edu/year/2017/course/TDA602/Eraserlab/pwdmasking.html>
 * http://www.cse.chalmers.se/edu/year/2017/course/TDA602/Eraserlab/pwdmasking.html</a>
 */
public final class ReadPasswordFromSysIn {

  /**
   * Reads password from user and returns it as array of chars.
   *
   * @param lisOb instance of class with common objects
   * @return The password as array of chars
   * @throws IOException if unable to get user's input
   */
  @NotNull
  char[] getPassword(@NotNull final ListerObjects lisOb) throws IOException {
    final Logger logger = lisOb.getLogger();
    InputStream stdIn = lisOb.getStdIn();

    logger.info("Attention! Typed password will be visible. Make sure nobody can see it");
    char[] lineBuffer;
    char[] buf = new char[128];

    int room = buf.length;
    int offset = 0;
    int c;
    boolean stop = false;

    while (!stop) {
      c = stdIn.read();
      switch (c) {
        case -1, '\n' -> stop = true;
        case '\r' -> {
          int c2 = stdIn.read();
          if ((c2 != '\n') && (c2 != -1)) {
            stdIn = unreadInputStream(stdIn, c2);
          } else {
            stop = true;
          }
        }
        default -> {
          if (--room < 0) {
            lineBuffer = new char[offset + 128];
            room = lineBuffer.length - offset - 1;
            System.arraycopy(buf, 0, lineBuffer, 0, offset);
            Arrays.fill(buf, ' ');
            buf = lineBuffer;
          }
          buf[offset++] = (char) c;
        }
      }
    }
    if (offset == 0) {
      return new char[0];
    }
    char[] ret = new char[offset];
    System.arraycopy(buf, 0, ret, 0, offset);
    Arrays.fill(buf, ' ');
    return ret;
  }

  /**
   * Unreads char from InputStream instance.
   *
   * @param stdIn instance of InputStream
   * @param c2    {@code int} value of char
   * @return instance of PushbackInputStream
   * @throws IOException if unable to work with the stream
   */
  @NotNull
  private InputStream unreadInputStream(InputStream stdIn, final int c2) throws IOException {
    stdIn = new PushbackInputStream(stdIn);
    ((PushbackInputStream) stdIn).unread(c2);
    return stdIn;
  }
}
