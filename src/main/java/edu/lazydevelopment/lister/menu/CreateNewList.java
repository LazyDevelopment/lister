package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.logic.ListerLists;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.util.List;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;
import static edu.lazydevelopment.lister.menu.MainMenu.isFinish;

/**
 * Part of user interface, related to new list creation.
 */
public final class CreateNewList {

  /**
   * Hiding default constructor because of security issue.
   */
  private CreateNewList() {
  }

  /**
   * Menu step implementation for new list creation.
   *
   * @param lisOb instance of class with common objects
   */
  static void createList(@NotNull final ListerObjects lisOb) {
    final Logger logger = lisOb.getLogger();
    final ListerLists lists = lisOb.getLists();
    final List<String> existingLists = lists.showLists(lisOb);

    logger.info("""

        {}Type name of new list.{} It shouldn't contain any special chars and should be unique.
        'exit' or '0' means exit""".indent(7), ANSI_BRIGHT_WHITE, ANSI_RESET);
    final String listName = lisOb.getConIn().getNameOfNewList(lisOb, existingLists);
    if (listName == null) {
      return;
    }

    if (!lisOb.getStore().createTableList(lisOb, listName)) {
      return;
    }

    if (!lists.storeRegEx(lisOb, listName) || isFinish()) {
      return;
    }
    logger.info("You successfully created new list '{}{}{}' and stored regex for items",
        ANSI_BRIGHT_YELLOW, listName, ANSI_RESET);
  }
}
