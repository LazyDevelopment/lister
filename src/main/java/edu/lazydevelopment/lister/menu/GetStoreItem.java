package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;

/**
 * Part of user interface, related to retrieving some individual elements from list to clipboard,
 * storing them from clipboard to list.
 */
public final class GetStoreItem {

  /**
   * Hiding default constructor because of security issue.
   */
  private GetStoreItem() {
  }

  /**
   * Controls system clipboard and based on user's command stores item to list or retrieves it to
   * clipboard.
   *
   * @param lisOb instance of class with common objects
   * @param list  with which user wants to work
   */
  static void workWithList(@NotNull final ListerObjects lisOb, final String list) {
    final Logger logger = lisOb.getLogger();
    Map<String, List<String>> result;

    try {
      result = lisOb.getLists().readListData(lisOb, list);
    } catch (SQLException e) {
      logger.error("Unable to get regex and/or already stored items from this list", e);
      return;
    }

    logger.info("""

            You are going to get and/or store items from the '{}{}{}'.
            Sound is {}{}{} now.
            It contains now {}{}{} items and {}{}{} viewed items.
            Regex for this list is: {}{}{}

            If you'll copy something to the system clipboard, it will be validated using regex and in case of match, will be stored in the list. The clipboard was just cleaned.
            To control the app copy to clipboard commands inside ' :

            '{}sound{}' - switch state of the sound (enable/disable it)
            '{}get{}' - copy last item from the list to clipboard and remove it from the list;
            '{}first{}' - copy 1st item and remove it from the list;
            '{}stop{}' - get out from this mode to previous menu""".indent(7),
        ANSI_BRIGHT_YELLOW, list, ANSI_RESET, ANSI_BRIGHT_YELLOW,
        lisOb.isSoundEnabled(lisOb) ? "enabled" : "disabled", ANSI_RESET,  ANSI_BRIGHT_YELLOW,
        result.get("itemsStored").size(), ANSI_RESET, ANSI_BRIGHT_YELLOW,
        result.get("itemsViewed").size(), ANSI_RESET, ANSI_BRIGHT_YELLOW,
        result.get("regex").get(0), ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET, ANSI_BRIGHT_WHITE,
        ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET);

    lisOb.getClipboard().clipboardOps(lisOb, list, result);
  }
}
