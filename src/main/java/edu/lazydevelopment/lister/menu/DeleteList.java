package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;

/**
 * Part of user interface, related to deleting list functionality.
 */
public final class DeleteList {

  /**
   * Hiding default constructor because of security issue.
   */
  private DeleteList() {
  }

  /**
   * Menu step implementation for delete list function.
   *
   * @param lisOb instance of class with common objects
   * @param list  which should be removed
   */
  static void deleteList(@NotNull final ListerObjects lisOb, final String list) {
    final Logger logger = lisOb.getLogger();
    final Store store = lisOb.getStore();

    logger.info("""
            You are going to remove the '{}{}{}' list.
            Are you sure you want to do that? 
            {}Type 'yes' or 'no'. 'exit' or '0' means exit{}"""
            .indent(7),
        ANSI_BRIGHT_YELLOW, list, ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET);
    if (lisOb.getConIn().getConfirmation(lisOb)) {
      return;
    }

    if (store.dropTableList(lisOb, list)) {
      logger.info("'{}{}{}' was removed from DB", ANSI_BRIGHT_YELLOW, list, ANSI_RESET);
    } else {
      logger.warn("Unable to remove '{}{}{}' list from DB", ANSI_BRIGHT_YELLOW, list, ANSI_RESET);
    }
  }
}
