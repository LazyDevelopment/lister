package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;

/**
 * Starting point of user menu, parent of all sub-menus, contains methods and fields, used to
 * control user interaction and flow.
 */
public final class MainMenu {

  private static boolean finish = false;

  /**
   * Hiding default constructor because of security issue.
   */
  private MainMenu() {
  }

  /**
   * Returns state of application finish flag: whether the app should be closed.
   *
   * @return {@code true} if Lister should be closed; {@code false} if not
   */
  public static boolean isFinish() {
    return finish;
  }

  /**
   * Sets the finish variable to {@code true}, which means that the app should be closed, or {@code
   * false}, which means that the app should continue current workflow.
   *
   * @param state the value, to which the variable need to be set
   */
  public static void setFinish(final boolean state) {
    MainMenu.finish = state;
  }

  /**
   * The main menu method.
   *
   * @param lisOb instance of class with common objects
   */
  public static void show(@NotNull final ListerObjects lisOb) {
    final Logger logger = lisOb.getLogger();
    boolean ok;
    byte input;
    byte errcount = 0;

    while (!isFinish()) {
      logger.info("""

          This is main menu. Select what do you want to do:
          {}1. Create new list
          2. Select an existing list
          3. Enable / disable sound
          -----------------------------
          'exit' or '0' - Close the app
          {}
          Type your choice""".indent(7), ANSI_BRIGHT_WHITE, ANSI_RESET);

      input = 0;
      do {
        ok = true;
        try {
          input = lisOb.getConIn().getByteValue(lisOb);
        } catch (Exception e) {
          logger.warn("Something went wrong. Try again");
          errcount++;
          ok = false;
        }
      } while (errcount < 10 && !ok);
      if (isFinish() || errcount >= 10) {
        break;
      }

      switch (input) {
        case 1 -> {
          CreateNewList.createList(lisOb);
          errcount = 0;
        }
        case 2 -> {
          SelectList.selectList(lisOb);
          errcount = 0;
        }
        case 3 -> {
          ManageSound.manageSound(lisOb);
          errcount = 0;
        }
        default -> {
          logger.warn("Wrong choice. Try again\n       +++++++++++++++++++++++\n");
          errcount++;
        }
      }
    }
    if (errcount >= 10) {
      logger.error("Too many errors. Goodbye!");
    }
  }
}
