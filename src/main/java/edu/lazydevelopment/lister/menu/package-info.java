/**
 * Provides classes to guide user through application workflow showing menus and interact with user
 * by other dialogue messages.
 */
package edu.lazydevelopment.lister.menu;
