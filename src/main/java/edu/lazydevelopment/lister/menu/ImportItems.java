package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.logic.ImportExportData;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.sql.SQLException;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;

/**
 * Part of user interface, related to import items from TXT file to the list.
 */
public final class ImportItems {

  /**
   * Hiding default constructor because of security issue.
   */
  private ImportItems() {
  }

  /**
   * Menu step to import items from file.
   *
   * @param lisOb instance of class with common objects
   * @param list  where items will be stored
   */
  static void importItems(@NotNull final ListerObjects lisOb, final String list) {
    final ImportExportData importExportData = lisOb.getImExData();
    final Logger logger = lisOb.getLogger();
    String regex;
    try {
      regex = lisOb.getRead().getRegex(lisOb.getConnect(), list, lisOb.getEncryptor());
    } catch (SQLException e) {
      logger.error("Unable to get regex for this list", e);
      return;
    }
    logger.info("""
            You are going to import items from text file into '{}{}{}' list.
                   Regex for this list is: {}{}{}

                   Type path to the file with not viewed items:
                   'exit' or '0' means exit""".indent(0), ANSI_BRIGHT_YELLOW, list, ANSI_RESET,
        ANSI_BRIGHT_YELLOW, regex, ANSI_RESET);

    String path = importExportData.getFilePath(lisOb);
    boolean fileCorrect;

    if (path.equals("0")) {
      return;
    }
    fileCorrect = lisOb.getFilesOps().doesFileExist(path + ".viewed.txt", logger);
    if (!fileCorrect) {
      logger.warn("File '{}{}.viewed.txt{}' doesn't exist, so viewed items won't be imported",
          ANSI_BRIGHT_YELLOW, path, ANSI_RESET);
    }

    if (fileCorrect) {
      logger.info("""

              File '{}{}.viewed.txt{}' was successfully opened.
              Its content will be processed line by line and stored in the '{}{}{}'.
              All lines will be outputted to this console with processing status. All lines, not stored in the list, will be outputted additionally
              """.indent(7),
          ANSI_BRIGHT_YELLOW, path, ANSI_RESET, ANSI_BRIGHT_YELLOW, list, ANSI_RESET);
      if (importExportData
          .importItemsFromFileToDB(lisOb, list, path + ".viewed.txt", true)) {
        logger.info("\n       Import procedure finished for file with viewed items\n");
      } else {
        logger.warn("\n       Import procedure was interrupted with errors\n");
      }
    }
    logger.info("""

            File '{}{}{}' was successfully opened.
            Its content will be processed line by line and stored in the '{}{}{}'.
            All lines will be outputted to this console with processing status. All lines, not stored in the list, will be outputted additionally
            """.indent(7),
        ANSI_BRIGHT_YELLOW, path, ANSI_RESET, ANSI_BRIGHT_YELLOW, list, ANSI_RESET);
    if (importExportData.importItemsFromFileToDB(lisOb, list, path, false)) {
      logger.info("\n       Import procedure finished for main file\n");
    } else {
      logger.warn("\n       Import procedure was interrupted with errors\n");
    }
  }
}
