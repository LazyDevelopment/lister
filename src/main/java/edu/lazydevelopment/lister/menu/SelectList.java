package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.logic.ListerLists;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.List;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;
import static edu.lazydevelopment.lister.menu.MainMenu.setFinish;

/**
 * Part of user interface, related to selecting active list and choosing an action for it.
 */
public final class SelectList {

  private static byte errcount;

  /**
   * Hiding default constructor because of security issue.
   */
  private SelectList() {
  }

  /**
   * Menu step to select active list and choose an action for it.
   *
   * @param lisOb instance of class with common objects
   */
  static void selectList(@NotNull final ListerObjects lisOb) {
    errcount = 0;
    final Logger logger = lisOb.getLogger();
    final Read read = lisOb.getRead();
    final ListerLists listerLists = lisOb.getLists();

    final List<String> lists = listerLists.showLists(lisOb);
    if (lists.isEmpty()) {
      logger.warn("\n       You can't select not existing lists");
      return;
    }

    logger.info("\n       {}Select one of these lists by typing its number:{}",
        ANSI_BRIGHT_WHITE, ANSI_RESET);
    Byte input = lisOb.getConIn().getUserChoice(lisOb, lists.size());
    if (input == -1) {
      logger.warn("Too many errors!");
      return;
    }
    if (input == 0) {
      setFinish(true);
      return;
    }

    final String selectedList = lists.get(input - 1);
    try {
      logger.info("You selected '{}{}{}' list.\n       It has {}{}{} items and {}{}{} viewed items",
          ANSI_BRIGHT_YELLOW, selectedList, ANSI_RESET,
          ANSI_BRIGHT_YELLOW, read.getAllItems(lisOb, selectedList).size(), ANSI_RESET,
          ANSI_BRIGHT_YELLOW, read.getAllItems(lisOb, selectedList + "_viewed").size(), ANSI_RESET);
    } catch (SQLException e) {
      logger.error("Unable to get items from the list", e);
    }

    if (listerLists.showRegEx(lisOb, selectedList)) {
      return;
    }

    chooseWhatToDoWithList(lisOb, selectedList);
  }

  /**
   * Let user to choose what to do next with selected list.
   *
   * @param lisOb        instance of class with common objects
   * @param selectedList name of current list
   */
  private static void chooseWhatToDoWithList(@NotNull final ListerObjects lisOb,
                                             final String selectedList) {
    final Logger logger = lisOb.getLogger();
    byte input;

    logger.info("""
            Now you could start working with the list '{}{}{}'
            You could:
            {}1. Get items from the list and store them there
            2. Import items to the list from some TXT file, where each item is in new line
            3. Export all items from the list to some TXT file, separated by 'new line' char
            4. Change regex for this list
            5. Rename the list
            6. Delete the list
            {}Type your choice (remember that 'exit' or '0' means exit)""".indent(7),
        ANSI_BRIGHT_YELLOW, selectedList, ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET);
    errcount = 0;
    input = -1;
    boolean ok;
    do {
      try {
        input = lisOb.getConIn().getByteValue(lisOb);
      } catch (Exception e) {
        logger.warn("Something went wrong. Try again", e);
      }
      ok = input >= 0 && input <= 6;
      if (!ok) {
        logger.warn("Wrong choice. Try again\n");
        errcount++;
      }
    } while (errcount < 10 && !ok);
    if (errcount >= 10) {
      return;
    }

    switch (input) {
      case 0:
        break;
      case 1:
        GetStoreItem.workWithList(lisOb, selectedList);
        break;
      case 2:
        ImportItems.importItems(lisOb, selectedList);
        break;
      case 3:
        ExportItems.exportItems(lisOb, selectedList);
        break;
      case 4:
        if (!lisOb.getLists().storeRegEx(lisOb, selectedList)) {
          logger.warn("Unable to update regex for this list");
        }
        break;
      case 5:
        RenameList.renameList(lisOb, selectedList);
        break;
      case 6:
        DeleteList.deleteList(lisOb, selectedList);
        break;
      default:
    }
  }
}
