package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.logic.ConsoleInput;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.util.List;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;

/**
 * Part of user interface, related to renaming the list.
 */
public final class RenameList {

  /**
   * Hiding default constructor because of security issue.
   */
  private RenameList() {
  }

  /**
   * Menu step implementation for rename list function.
   *
   * @param lisOb instance of class with common objects
   * @param list  which should be renamed
   */
  static void renameList(@NotNull final ListerObjects lisOb, final String list) {
    final Logger logger = lisOb.getLogger();
    final ConsoleInput conin = lisOb.getConIn();

    logger.info("""
        You are going to rename the '{}{}{}' list.
        Are you sure you want to do that?
        {}Type 'yes' or 'no'. 'exit' or '0' means exit{}""".indent(7),
        ANSI_BRIGHT_YELLOW, list, ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET);

    if (conin.getConfirmation(lisOb)) {
      return;
    }

    final List<String> existingLists = lisOb.getLists().showLists(lisOb);
    logger.info("""

            Type new name for the '{}{}{}'. It shouldn't contain any special chars and should be unique.
            'exit' or '0' means exit""".indent(7),
        ANSI_BRIGHT_YELLOW, list, ANSI_RESET);

    String newName = conin.getNameOfNewList(lisOb, existingLists);
    if (newName == null) {
      return;
    }

    if (!lisOb.getStore().renameTableList(lisOb, list, newName)) {
      logger.warn("Unable to rename the list");
    }
  }
}
