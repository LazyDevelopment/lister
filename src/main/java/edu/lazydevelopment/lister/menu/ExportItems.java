package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.files.FilesOps;
import edu.lazydevelopment.lister.logic.ImportExportData;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.List;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;

/**
 * Part of user interface, related to export items from list to file.
 */
public final class ExportItems {

  /**
   * Hiding default constructor because of security issue.
   */
  private ExportItems() {
  }

  /**
   * Menu step implementation for exporting all items from the list to TXT files. Viewed items will
   * be exported to the file with the same name + '.viewed.txt'.
   *
   * @param lisOb instance of class with common objects
   * @param list  from which items should be exported
   */
  static void exportItems(@NotNull final ListerObjects lisOb, final String list) {
    final Logger logger = lisOb.getLogger();
    final FilesOps filesOps = lisOb.getFilesOps();
    final Read read = lisOb.getRead();
    final ImportExportData importExportData = lisOb.getImExData();

    logger.info("""
            You are going to export items from '{}{}{}' list into text file.

                   {}Type path to the file.{} If it doesn't exist, it will be created:
                   'exit' or '0' means exit""".indent(0), ANSI_BRIGHT_YELLOW, list, ANSI_RESET,
        ANSI_BRIGHT_WHITE, ANSI_RESET);

    String input = importExportData.getValidPath(lisOb);
    if (input == null) {
      return;
    }

    List<String> items;
    List<String> itemsViewed;
    try {
      items = read.getAllItems(lisOb, list);
      itemsViewed = read.getAllItems(lisOb, list + "_viewed");
    } catch (SQLException e) {
      logger.error("Unable to get items from the list", e);
      return;
    }
    logger.info("""

            List '{}{}{}' contains {}{}{} items and {}{}{} viewed items.

            Writing items to file...""".indent(7),
        ANSI_BRIGHT_YELLOW, list, ANSI_RESET, ANSI_BRIGHT_YELLOW, items.size(), ANSI_RESET,
        ANSI_BRIGHT_YELLOW, itemsViewed.size(), ANSI_RESET);

    if (filesOps.exportItemsToFile(logger, input, items) == items.size()) {
      logger.info("""

              All items were exported successfully:
              {}{}{} item(s) is (are) written to the '{}{}{}' file.""".indent(7),
          ANSI_BRIGHT_YELLOW, items.size(), ANSI_RESET, ANSI_BRIGHT_YELLOW, input, ANSI_RESET);
    } else {
      logger.warn("""
          
          Some errors occurred during exporting items to '{}{}{}' file. Not all items may be exported successfully"""
          .indent(7), ANSI_BRIGHT_YELLOW, input, ANSI_RESET);
    }

    if (!itemsViewed.isEmpty()) {
      logger.info("Writing viewed items to file...");
      if (filesOps.exportItemsToFile(logger, input + ".viewed.txt", itemsViewed)
          == itemsViewed.size()) {
        logger.info("""

                All viewed items were exported successfully:
                {}{}{} item(s) is (are) written to the '{}{}.viewed.txt{}' file.""".indent(7),
            ANSI_BRIGHT_YELLOW, itemsViewed.size(), ANSI_RESET,
            ANSI_BRIGHT_YELLOW, input, ANSI_RESET);
      } else {
        logger.warn("""
            
            Some errors occurred during exporting items to '{}{}{}' file. Not all items may be exported successfully"""
            .indent(7), ANSI_BRIGHT_YELLOW, input, ANSI_RESET);
      }
    }
  }
}
