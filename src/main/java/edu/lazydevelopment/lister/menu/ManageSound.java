package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;

/**
 * Part of user interface, related to enabling or disabling the sound.
 */
public final class ManageSound {
  /**
   * Hiding default constructor because of security issue.
   */
  private ManageSound() {
  }

  /**
   * Menu step implementation for enabling or disabling the sound.
   *
   * @param lisOb instance of class with common objects
   */
  static void manageSound(@NotNull final ListerObjects lisOb) {
    final Logger logger = lisOb.getLogger();
    String soundState = lisOb.isSoundEnabled(lisOb) ? "enabled" : "disabled";
    byte errcount = 0;
    byte input = -1;
    boolean ok;

    logger.info("""

            Now sound in application is {}{}{}.
            You could:
            {}1. Change the state of the sound to opposite one;
            2. Get back to main menu without any changes;
            {}Type your choice (remember that 'exit' or '0' means exit)""".indent(7),
        ANSI_BRIGHT_YELLOW, soundState, ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET);

    do {
      try {
        input = lisOb.getConIn().getByteValue(lisOb);
      } catch (Exception e) {
        logger.warn("Something went wrong. Try again", e);
      }
      ok = input >= 0 && input <= 2;
      if (!ok) {
        logger.warn("Wrong choice. Try again\n");
        errcount++;
      }
    } while (errcount < 10 && !ok);
    if (errcount >= 10) {
      logger.warn("Too many errors!");
      return;
    }

    actions(lisOb, input);
  }

  /**
   * Performs the action according to user's input.
   *
   * @param lisOb instance of class with common objects
   * @param input user's input
   */
  private static void actions(@NotNull ListerObjects lisOb, byte input) {
    switch (input) {
      case 0:
        MainMenu.setFinish(true);
        break;
      case 2:
        break;
      case 1:
        lisOb.switchSoundState(lisOb);
        break;
      default:
    }
  }
}
