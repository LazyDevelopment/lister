package edu.lazydevelopment.lister.logic;

import com.github.fracpete.jclipboardhelper.ClipboardHelper;
import edu.lazydevelopment.lister.sounds.SoundsCollection;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BLUE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_GREEN;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_PURPLE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_YELLOW;

/**
 * Provides methods to work with system clipboard.
 */
public class Clipboard {

  private String regex;
  private Set<String> itemsStored;
  private Set<String> itemsViewed;
  private String tmp = "";
  private byte outputRowsCount = 0;

  /**
   * Clipboard management method.
   *
   * @param lisOb  contains common objects for the app
   * @param list   name of current list
   * @param result {@code Map<String, List<String>>} with previously requested data about the list
   *               from DB
   */
  public void clipboardOps(@NotNull final ListerObjects lisOb, final String list,
                           @NotNull final Map<String, List<String>> result) {
    itemsStored = new HashSet<>(result.get("itemsStored"));
    itemsViewed = new HashSet<>(result.get("itemsViewed"));
    regex = result.get("regex").get(0);

    ClipboardHelper.copyToClipboard("");
    String clipboard;
    lisOb.getSounds().playStart(lisOb);
    do {
      if (ClipboardHelper.canPasteStringFromClipboard()) {
        clipboard = ClipboardHelper.pasteStringFromClipboard();
        if (clipboard != null && !clipboard.equals(tmp) && !clipboard.equals("")
            && processClipboard(lisOb, list, clipboard)) {
          return;
        } else if (clipboard == null) {
          lisOb.getLogger().error("Unable to get string from clipboard. Try again.");
          lisOb.getSounds().playError(lisOb);
        }
      }

      if (outputRowsCount >= 15) {
        outputRowsCount = 0;
        lisOb.getLogger()
            .info("""
                  Note: '{}get{}' last item from list; '{}first{}' item;
                        '{}stop{}' this or switch '{}sound{}' on/off
                  """.indent(0),
                ANSI_BRIGHT_WHITE, ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET,
                ANSI_BRIGHT_WHITE, ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET);
      }
      try {
        Thread.sleep(500);
      } catch (InterruptedException ignored) {
        Thread.currentThread().interrupt();
      }
    } while (true);
  }

  /**
   * Processes string from clipboard.
   *
   * @param lisOb     instance of class with common objects
   * @param list      with which user wants to work
   * @param clipboard content of clipboard, which should be processed
   * @return {@code true} if Lister should stop clipboard processing; {@code false} if not
   */
  private boolean processClipboard(final ListerObjects lisOb, final String list,
                                   @NotNull final String clipboard) {
    if ((clipboard.trim().equalsIgnoreCase("stop")
        || clipboard.trim().equalsIgnoreCase("sound")
        || clipboard.trim().equalsIgnoreCase("get")
        || clipboard.trim().equalsIgnoreCase("first"))) {
      return processCommands(lisOb, list, clipboard.trim());
    } else {
      return clipboard.matches(regex)
          && !storeItemToList(lisOb, list, clipboard);
    }
  }

  /**
   * Processes user commands from clipboard.
   *
   * @param lisOb     instance of class with common objects
   * @param list      with which user wants to work
   * @param clipboard content of clipboard, which should be processed
   * @return {@code true} if Lister should stop clipboard processing; {@code false} if not
   */
  private boolean processCommands(final ListerObjects lisOb, final String list,
                                  @NotNull final String clipboard) {
    if (clipboard.equalsIgnoreCase("stop")) {
      lisOb.getLogger().info("Clipboard processing stopped");
      lisOb.getSounds().playFinish(lisOb);
      itemsStored.clear();
      itemsViewed.clear();
      ClipboardHelper.copyToClipboard("");
      return true;
    } else if (clipboard.equalsIgnoreCase("sound")) {
      lisOb.switchSoundState(lisOb);
      ClipboardHelper.copyToClipboard("");
      return false;
    } else if (clipboard.equalsIgnoreCase("get")
        && !getItemToClipboard(true, lisOb, list)) {
      return true;
    } else {
      return clipboard.equalsIgnoreCase("first")
          && !getItemToClipboard(false, lisOb, list);
    }
  }

  /**
   * Checks whether new item isn't duplicate to already stored and viewed items and if not, stores
   * it in the list.
   *
   * @param lisOb     instance of class with common objects
   * @param list      where item is going to be stored
   * @param clipboard string from system clipboard to be stored as new item
   * @return {@code true} if processed successfully; {@code false} in case of SQLException
   */
  private boolean storeItemToList(@NotNull final ListerObjects lisOb, final String list,
                                  final String clipboard) {
    final Logger logger = lisOb.getLogger();
    final SoundsCollection sounds = lisOb.getSounds();
    tmp = clipboard;

    try {
      if (!itemsStored.contains(tmp)
          && !itemsViewed.contains(tmp)
          && lisOb.getStore().storeItem(lisOb.getConnect(), tmp, list, lisOb.getEncryptor())) {
        itemsStored.add(tmp);
        sounds.playStored(lisOb);
        logger.info("{}Just stored{}: {}", ANSI_GREEN, ANSI_RESET, tmp);
      } else if (itemsStored.contains(tmp)) {
        logger.info("{}Duplicate{}:   {}", ANSI_YELLOW, ANSI_RESET, tmp);
        sounds.playDuplicate(lisOb);
      } else if (itemsViewed.contains(tmp)) {
        logger.info("{}Viewed{}:      {}", ANSI_PURPLE, ANSI_RESET, tmp);
        sounds.playWatched(lisOb);
      } else {
        logger.error("Item {} was stored in DB incorrectly", tmp);
        sounds.playError(lisOb);
      }
    } catch (SQLException e) {
      logger.error("Unable to store item to the list", e);
      sounds.playError(lisOb);
      return false;
    }
    outputRowsCount++;
    return true;
  }

  /**
   * Retrieves an item from the list and copies it to system clipboard.
   *
   * @param isLast true if last item should be retrieved; false if 1st one
   * @param lisOb  instance of class with common objects
   * @param list   from which item is going to be retrieved
   * @return {@code true} if processed successfully; {@code false} in case of SQLException
   */
  private boolean getItemToClipboard(final boolean isLast, @NotNull final ListerObjects lisOb,
                                     final String list) {
    final Logger logger = lisOb.getLogger();
    final SoundsCollection sounds = lisOb.getSounds();

    try {
      tmp = lisOb.getRead().getSomeItemAndRemove(lisOb, list, isLast);
    } catch (SQLException e) {
      logger.error("Unable to get item from the list", e);
      sounds.playError(lisOb);
      return false;
    }
    if (!tmp.equals("")) {
      ClipboardHelper.copyToClipboard(tmp);
      itemsViewed.add(tmp);
      itemsStored.remove(tmp);
      sounds.playCopied(lisOb);
      logger.info("{}Just copied{}: {}", ANSI_BLUE, ANSI_RESET, tmp);
      outputRowsCount++;
    } else {
      ClipboardHelper.copyToClipboard("");
      sounds.playError(lisOb);
    }
    return true;
  }
}
