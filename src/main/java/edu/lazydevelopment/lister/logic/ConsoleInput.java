package edu.lazydevelopment.lister.logic;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.util.List;
import java.util.Scanner;

import static edu.lazydevelopment.lister.menu.MainMenu.isFinish;
import static edu.lazydevelopment.lister.menu.MainMenu.setFinish;

/**
 * Provides methods to manage console inputs.
 */
public class ConsoleInput {

  /**
   * Reads input data from user's shell, checks whether exit is requested and parses input string to
   * Byte.
   *
   * @param lisOb instance of class with common objects
   * @return 0 for exit or [-128, 127] depending on user's input
   * @throws NumberFormatException if the string does not contain a parsable {@code byte}
   */
  public byte getByteValue(@NotNull final ListerObjects lisOb) {
    Scanner scanner = new Scanner(lisOb.getStdIn());
    final String input = scanner.nextLine().trim();
    if (input.equalsIgnoreCase("exit") || input.equals("0")) {
      lisOb.getLogger().info("Goodbye!");
      setFinish(true);
      return 0;
    } else {
      return Byte.parseByte(input);
    }
  }

  /**
   * Reads input data from user's shell, checks whether exit is requested and returns input string.
   *
   * @param lisOb instance of class with common objects
   * @return {@code "0"} if exit was requested or all the string, got from user
   */
  public String getStringValue(@NotNull final ListerObjects lisOb) {
    Scanner scanner = new Scanner(lisOb.getStdIn());
    final String input = scanner.nextLine().trim();
    if (input.equalsIgnoreCase("exit") || input.equals("0")) {
      lisOb.getLogger().info("Goodbye!");
      setFinish(true);
      return "0";
    } else {
      return input;
    }
  }

  /**
   * Returns order number of list, selected by user from shown list of available lists.
   *
   * @param lisOb     instance of class with common objects
   * @param listsSize size of the list of available lists
   * @return # of selected list
   */
  public Byte getUserChoice(@NotNull final ListerObjects lisOb, final int listsSize) {
    final Logger logger = lisOb.getLogger();
    byte input = 0;
    byte errcount = 0;
    boolean ok = false;

    do {
      try {
        input = lisOb.getConIn().getByteValue(lisOb);
      } catch (Exception e) {
        logger.warn("Something went wrong. Try again", e);
        errcount++;
        continue;
      }
      ok = input >= 0 && input <= listsSize;
      if (!ok) {
        logger.warn("Wrong choice. Try again");
        errcount++;
      }
    } while (errcount < 10 && !ok);
    if (errcount >= 10) {
      logger.warn("Too many errors!");
      input = -1;
    }
    return input;
  }

  /**
   * Asks user to confirm operation.
   *
   * @param lisOb instance of class with common objects
   * @return {@code true} if not confirmed; {@code false} if confirmed
   */
  public boolean getConfirmation(@NotNull final ListerObjects lisOb) {
    String input;
    byte errcount = 0;
    do {
      input = lisOb.getConIn().getStringValue(lisOb);
      errcount++;
    } while (!input.equals("0")
        && !input.equalsIgnoreCase("yes")
        && !input.equalsIgnoreCase("no")
        && errcount < 10);
    return input.equals("0")
        || input.equalsIgnoreCase("no")
        || errcount >= 10;
  }

  /**
   * Asks user to type valid unique name for new list.
   *
   * @param lisOb         instance of class with common objects
   * @param existingLists {@code List<String>} of already created lists
   * @return {@code String} of name for new list
   */
  public String getNameOfNewList(@NotNull final ListerObjects lisOb,
                                 @NotNull final List<String> existingLists) {
    String listName;
    byte errcount = 0;
    boolean errCondition;
    do {
      listName = lisOb.getConIn().getStringValue(lisOb);
      errcount++;
      if (existingLists.contains(listName)) {
        lisOb.getLogger().warn("List with this name already exists. Choose another name.");
      }

      errCondition = errcount < 10;
    } while ((listName
        .matches("^(?:.*)[_`|~!№@#$%&*()+=\b\\[{\\]}\\\\'<,.>?/\";:\\s^-](?:.*)$")
        && errCondition)
        || (listName.isEmpty() && errCondition)
        || (existingLists.contains(listName) && errCondition));
    if (isFinish() || errcount >= 10) {
      return null;
    }
    return listName;
  }
}
