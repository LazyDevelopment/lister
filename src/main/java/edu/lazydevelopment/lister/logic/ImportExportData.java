package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.files.FilesOps;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_GREEN;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_RED;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_GREEN;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_RED;

/**
 * Provides methods to manage import and export of data.
 */
public class ImportExportData {

  private static final String VIEWED = "_viewed";
  private byte errcount = 0;

  /**
   * Asks user to type path to source file and validates that path.
   *
   * @param lisOb instance of class with common objects
   * @return path to file or "0" in case of exit
   */
  public String getFilePath(@NotNull final ListerObjects lisOb) {
    final Logger logger = lisOb.getLogger();
    String path;
    errcount = 0;
    boolean fileCorrect;
    do {
      path = lisOb.getConIn().getStringValue(lisOb);
      if (path.equals("0")) {
        return path;
      }
      fileCorrect = lisOb.getFilesOps().doesFileExist(path, logger);
      if (!fileCorrect) {
        errcount++;
      }
    } while (!fileCorrect && errcount < 10);
    if (errcount >= 10) {
      logger.warn("Too many errors");
      return "0";
    }
    return path;
  }

  /**
   * Stores all rows from TXT file (mentioned in path), which match regex, to provided list in DB.
   *
   * @param lisOb    instance of class with common objects
   * @param list     where items will be stored
   * @param path     of opened file, from which items will be imported
   * @param isViewed {@code true} if the file contains viewed items; {@code false} if not
   * @return {@code true} if import was finished; {@code false} if it was interrupted
   */
  public boolean importItemsFromFileToDB(@NotNull final ListerObjects lisOb, final String list,
                                         final String path, final boolean isViewed) {
    String regex;
    Set<String> itemsStored;
    Set<String> itemsViewed;
    final Read read = lisOb.getRead();
    final Logger logger = lisOb.getLogger();
    final String digits = "digits";

    try {
      itemsStored = new HashSet<>(read.getAllItems(lisOb, list));
      itemsViewed = new HashSet<>(read.getAllItems(lisOb, list + VIEWED));
      regex = read.getRegex(lisOb.getConnect(), list, lisOb.getEncryptor());
    } catch (SQLException e) {
      logger.error("Unable to get already stored items or RegEx from this list", e);
      return false;
    }

    Map<String, List<String>> fromFile = lisOb.getFilesOps()
        .importLinesFromFileToList(path, logger, regex, itemsStored, itemsViewed);
    errcount = Byte.parseByte(fromFile.get(digits).get(0));
    if (errcount == -1) {
      return false;
    }
    int linesProcessed = Integer.parseInt(fromFile.get(digits).get(1));
    int linesStored = Integer.parseInt(fromFile.get(digits).get(2));
    List<String> failed = fromFile.get("failed");
    List<String> toStore = fromFile.get("toStore");
    boolean res;

    if (!toStore.isEmpty()) {
      res = storeProcessedData(lisOb, list, isViewed, toStore);
    } else {
      logger.warn("\n       No lines were prepared to be imported. nothing to do");
      return false;
    }

    if (res) {
      logger.info("""

              All lines of that file are processed and file is closed:
              Processed: {}{}{}
              Stored :{}{}{}
              Errors : {}{}{}
              Not stored lines for any reason: {}{}{}

              Here are all skipped lines:""".indent(7),
          ANSI_GREEN, linesProcessed, ANSI_RESET,
          ANSI_BRIGHT_GREEN, linesStored, ANSI_RESET,
          ANSI_BRIGHT_RED, errcount, ANSI_RESET,
          ANSI_RED, failed.size(), ANSI_RESET);
      for (String s : failed) {
        logger.warn("{}{}{}", ANSI_BRIGHT_YELLOW, s, ANSI_RESET);
      }
    }
    return res;
  }

  /**
   * Stores processed data from toStore list into corresponding DB table.
   *
   * @param lisOb    instance of class with common objects
   * @param list     where items will be stored
   * @param isViewed {@code true} if the file contains viewed items; {@code false} if not
   * @param toStore  {@code List<String>} of items to be stored
   * @return {@code true} if finished successfully; {@code false} if exception was caught
   */
  private boolean storeProcessedData(@NotNull final ListerObjects lisOb, final String list,
                                     final boolean isViewed, final List<String> toStore) {
    final Store store = lisOb.getStore();
    if (isViewed) {
      try {
        if (!store.storeItemsList(lisOb, toStore, list + VIEWED)) {
          storeErr(lisOb, list + VIEWED, toStore);
          return false;
        }
      } catch (SQLException e) {
        lisOb.getLogger().error("Unable to prepare list of not stored items", e);
        return false;
      }
    } else {
      try {
        if (!store.storeItemsList(lisOb, toStore, list)) {
          storeErr(lisOb, list, toStore);
          return false;
        }
      } catch (SQLException e) {
        lisOb.getLogger().error("Unable to prepare list of not stored items", e);
        return false;
      }
    }
    return true;
  }

  /**
   * Prepares and outputs list of items, not stored in DB.
   *
   * @param lisOb   instance of class with common objects
   * @param list    where items meant to be stored
   * @param toStore list of items, got from the file, except of duplicates
   * @throws SQLException if unable to read items from DB
   */
  private void storeErr(@NotNull final ListerObjects lisOb, final String list,
                        final List<String> toStore) throws SQLException {
    final Logger logger = lisOb.getLogger();

    logger.warn("Unable to store items in DB. {}Next items were not stored:{}",
        ANSI_BRIGHT_YELLOW, ANSI_RESET);
    for (String item : lisOb.getRead().getAllItems(lisOb, list)) {
      toStore.remove(item);
    }
    for (String item : toStore) {
      logger.warn("{}{}{}", ANSI_BRIGHT_YELLOW, item, ANSI_RESET);
    }
    logger.warn("{}{}{} items not stored", ANSI_BRIGHT_YELLOW, toStore.size(), ANSI_RESET);
  }

  /**
   * Asks user to provide valid file path for export data and validates that file could be written.
   *
   * @param lisOb instance of class with common objects
   * @return {@code String} of valid writable file path or {@code null} in case of errors or exit
   */
  public String getValidPath(@NotNull final ListerObjects lisOb) {
    final FilesOps filesOps = lisOb.getFilesOps();
    final Logger logger = lisOb.getLogger();
    String input;
    errcount = 0;
    boolean fileCorrect;
    boolean fileViewedCorrect;

    do {
      input = lisOb.getConIn().getStringValue(lisOb);
      if (input.equals("0")) {
        return null;
      }
      fileCorrect = filesOps.canFileBeWrittenOrCreated(input, logger);
      fileViewedCorrect = filesOps.canFileBeWrittenOrCreated(input + ".viewed.txt", logger);
      if (!fileCorrect || !fileViewedCorrect) {
        errcount++;
      }
    } while ((!fileCorrect || !fileViewedCorrect) && errcount < 10);
    if (errcount >= 10) {
      logger.warn("Too many errors");
      return null;
    }
    return input;
  }
}
