/**
 * Provides classes, implementing business logic of the application.
 */
package edu.lazydevelopment.lister.logic;
