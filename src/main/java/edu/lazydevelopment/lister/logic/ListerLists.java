package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.db.Store;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_WHITE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;
import static edu.lazydevelopment.lister.menu.MainMenu.isFinish;
import static edu.lazydevelopment.lister.menu.MainMenu.setFinish;

/**
 * Provides methods to work with lists.
 */
public class ListerLists {

  /**
   * Shows RegEx for current list to user if it exists or asks to create new one.
   *
   * @param lisOb        instance of class with common objects
   * @param selectedList name of current list
   * @return {@code true} if list selection menu should be closed and user will return to main menu;
   * {@code false} if the flow could be continued
   */
  public boolean showRegEx(@NotNull final ListerObjects lisOb, final String selectedList) {
    String listRegex;
    final Logger logger = lisOb.getLogger();

    try {
      listRegex = lisOb.getRead().getRegex(lisOb.getConnect(), selectedList, lisOb.getEncryptor());
    } catch (SQLException e) {
      logger.error("Unable to get regex for this list because of DB connection problems", e);
      return true;
    }

    if (listRegex == null || listRegex.isEmpty()) {
      logger.info("Regex for this list is empty. You need to set it now.");
      return !lisOb.getLists().storeRegEx(lisOb, selectedList) || isFinish();
    }
    logger.info("Regex for this list is '{}{}{}'\n", ANSI_BRIGHT_YELLOW, listRegex, ANSI_RESET);
    return false;
  }

  /**
   * Outputs numbered list of existing lists to let user choose 1 of them.
   *
   * @param lisOb instance of class with common objects
   * @return {@code List<String>} of existing lists
   */
  public List<String> showLists(@NotNull final ListerObjects lisOb) {
    final Logger logger = lisOb.getLogger();
    String orderNumber;

    try {
      if (lisOb.getConnect() != null) {
        List<String> listTables = lisOb.getRead().getTables(lisOb);
        listTables.remove("_settings");
        if (!listTables.isEmpty()) {
          logger.info("You have next lists:");
          for (int i = 0; i < listTables.size(); i++) {
            orderNumber = String.valueOf(i + 1);
            logger.info("{}{}. {}{}", ANSI_BRIGHT_YELLOW, orderNumber, listTables.get(i),
                ANSI_RESET);
          }
        } else {
          logger.info("There are no lists");
        }
        return listTables;
      }
    } catch (SQLException e) {
      logger.error("Unable to get list of tables in DB", e);
    }
    setFinish(true);
    return new LinkedList<>();
  }

  /**
   * Asks for Regex and stores it in the list.
   *
   * @param lisOb    instance of class with common objects
   * @param listName where Regex should be stored
   * @return {@code true} if Regex stored successfully; {@code false} if too many errors, unable to
   *        store Regex or user asked for exit
   */
  public boolean storeRegEx(@NotNull final ListerObjects lisOb, final String listName) {
    final Logger logger = lisOb.getLogger();
    final Store store = lisOb.getStore();

    logger.info("""

        {}Type regex, to match items, stored in the list.{} You could use 'https://uiregex.com/us' to build it. Regex shouldn't include starting and trailing /, only string in between. Example: ^https\\:\\/\\/www\\.name\\.ru\\/(?:.*)$
        'exit' means exit""".indent(7), ANSI_BRIGHT_WHITE, ANSI_RESET);
    String listRegex;
    byte errcount = 0;
    boolean ok;
    do {
      listRegex = lisOb.getConIn().getStringValue(lisOb);
      errcount++;
      if (listRegex.equals("0")) {
        return false;
      }
      ok = listRegex.startsWith("^") && listRegex.endsWith("$");
    } while (!ok && errcount < 10);
    if (errcount >= 10) {
      logger.warn("Too many errors!");
      return false;
    }

    try {
      if (lisOb.getRead().getRegex(lisOb.getConnect(), listName, lisOb.getEncryptor()).isEmpty()) {
        return store.storeItem(lisOb.getConnect(), listRegex, listName, lisOb.getEncryptor());
      } else {
        if (!store.updateRegex(listRegex, lisOb, listName)) {
          logger.warn("Unable to update regex");
          return false;
        }
      }
    } catch (SQLException e) {
      logger.error("Unable to store list's regex", e);
      return false;
    }
    return true;
  }

  /**
   * Reads Regex, existing items and previously viewed items from the list.
   *
   * @param lisOb instance of class with common objects
   * @param list  from which data should be read
   * @return {@code Map<String, List<String>>} with {@code singletonList()} of regex; itemsStored;
   *        itemsViewed
   * @throws SQLException if something couldn't be read
   */
  public Map<String, List<String>> readListData(@NotNull final ListerObjects lisOb,
                                                final String list) throws SQLException {
    final Read read = lisOb.getRead();
    Map<String, List<String>> result = new HashMap<>();

    result.put("regex", Collections.singletonList(read.getRegex(lisOb.getConnect(), list,
        lisOb.getEncryptor())));
    result.put("itemsStored", read.getAllItems(lisOb, list));
    result.put("itemsViewed", read.getAllItems(lisOb, list + "_viewed"));

    return result;
  }
}
