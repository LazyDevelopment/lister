package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.db.Connect;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.files.FilesOps;
import edu.lazydevelopment.lister.menu.MainMenu;
import edu.lazydevelopment.lister.security.Login;
import edu.lazydevelopment.lister.sounds.SoundsCollection;
import org.jasypt.util.text.AES256TextEncryptor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;

/**
 * Contains all common objects for the app and provides access to them.
 */
public class ListerObjects {

  private SoundsCollection sounds;
  private Connect connect;
  private AES256TextEncryptor encryptor;
  private Boolean soundState;
  private final Logger logger;
  private final InputStream stdIn;
  private final Read read;
  private final Store store;
  private final Login login;
  private final ConsoleInput conIn;
  private final FilesOps filesOps;
  private final ListerLists lists;
  private final Clipboard clipboard;
  private final ImportExportData imExData;
  private final Console sysConsole;

  /**
   * Constructor initiates objects, which could be initiated without any additional configuration.
   */
  public ListerObjects() {
    logger = LoggerFactory.getLogger("ListerLogger");
    stdIn = System.in;
    read = new Read();
    store = new Store();
    login = new Login();
    conIn = new ConsoleInput();
    filesOps = new FilesOps();
    lists = new ListerLists();
    clipboard = new Clipboard();
    imExData = new ImportExportData();
    sysConsole = System.console();
  }

  /**
   * Package-private method to set variable only for testing purposes.
   * Should NOT be used outside of tests.
   *
   * @param newState new value for {@code soundState} flag
   */
  void rawSetSoundStateFlag(final Boolean newState) {
    this.soundState = newState;
  }

  /**
   * Getter for Sound state setting.
   *
   * @param lisOb instance of class with common objects
   * @return {@code true} for "sound enabled" and {@code false} for "sound disabled"
   */
  public boolean isSoundEnabled(@NotNull final ListerObjects lisOb) {
    final Logger localLogger = lisOb.getLogger();
    if (soundState == null) {
      try {
        soundState = lisOb.getRead().getSoundState(lisOb);
        return soundState;
      } catch (SQLException e) {
        localLogger.error("Unable to get sound state from DB", e);
        MainMenu.setFinish(true);
        return true;
      } catch (NoSuchMethodException e) {
        localLogger.error("Value of sound state variable is wrong", e);
        MainMenu.setFinish(true);
        return true;
      } catch (NoSuchFieldException e) {
        if (lisOb.setSoundState(true, lisOb)) {
          return soundState;
        } else {
          localLogger.error("Value of sound state didn't exist and unable to be created", e);
          MainMenu.setFinish(true);
          return true;
        }
      }
    } else {
      return soundState;
    }
  }

  /**
   * Setter for Sound state setting.
   *
   * @param soundState {@code true} for "sound enabled" and {@code false} for "sound disabled"
   * @param lisOb instance of class with common objects
   * @return {@code true} if value was successfully set; {@code false} if not
   */
  public boolean setSoundState(final boolean soundState, final ListerObjects lisOb) {
    if (soundState && lisOb.getSounds() == null) {
      try {
        lisOb.setSounds(new SoundsCollection());
        lisOb.getSounds().playHello(lisOb);
      } catch (IOException e) {
        lisOb.getLogger().error("Unable to init audio, disabling sound", e);
        return lisOb.setSoundState(false, lisOb);
      }
    }
    try {
      if (lisOb.getStore().storeSoundState(soundState, lisOb)) {
        lisOb.soundState = soundState;
        return true;
      } else {
        lisOb.getLogger()
            .error("Unable to store sound state in DB: value in DB != to requested one");
        MainMenu.setFinish(true);
        return false;
      }
    } catch (SQLException e) {
      lisOb.getLogger().error("Unable to store sound state in DB", e);
      MainMenu.setFinish(true);
      return false;
    }
  }

  /**
   * Switches the state of the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void switchSoundState(@NotNull final ListerObjects lisOb) {
    if (lisOb.setSoundState(!lisOb.isSoundEnabled(lisOb), lisOb)) {
      lisOb.getLogger().info("Now {}sound{} in application is {}{}{}",
          ANSI_BRIGHT_YELLOW, ANSI_RESET, ANSI_BRIGHT_YELLOW,
          lisOb.isSoundEnabled(lisOb) ? "enabled" : "disabled", ANSI_RESET);
    } else {
      lisOb.getLogger().error("Unable to change sound state");
    }
  }

  /**
   * Getter for Console object with methods to manage user input.
   *
   * @return {@code Console} object
   */
  public Console getSysConsole() {
    return sysConsole;
  }

  /**
   * Getter for ImportExportData object with methods to manage import \ export items.
   *
   * @return {@code ImportExportData} object
   */
  public ImportExportData getImExData() {
    return imExData;
  }

  /**
   * Getter for Clipboard object with methods to work with clipboard.
   *
   * @return {@code Clipboard} object
   */
  public Clipboard getClipboard() {
    return clipboard;
  }

  /**
   * Getter for ListerLists object with methods to work with lists.
   *
   * @return {@code ListerLists} object
   */
  public ListerLists getLists() {
    return lists;
  }

  /**
   * Getter for Encryptor, used to encrypt and decrypt data in DB.
   *
   * @return {@code StrongTextEncryptor} object
   */
  public AES256TextEncryptor getEncryptor() {
    return encryptor;
  }

  /**
   * Setter for Encryptor, used to encrypt and decrypt data in DB.
   *
   * @param encryptor {@code StrongTextEncryptor} object
   */
  public void setEncryptor(final AES256TextEncryptor encryptor) {
    this.encryptor = encryptor;
  }

  /**
   * Getter for FilesOps object with methods to work with files.
   *
   * @return {@code FilesOps} object
   */
  public FilesOps getFilesOps() {
    return filesOps;
  }

  /**
   * Getter for Connect object with methods to connect to DB.
   *
   * @return {@code Connect} object
   */
  public Connect getConnect() {
    return connect;
  }

  /**
   * Setter for Connect object with methods to connect to DB.
   *
   * @param connect {@code Connect} object
   */
  public void setConnect(final Connect connect) {
    this.connect = connect;
  }

  /**
   * Getter for SoundsCollection object with methods to play pre-configured sounds.
   *
   * @return {@code SoundsCollection} object
   */
  public SoundsCollection getSounds() {
    return sounds;
  }

  /**
   * Setter for SoundsCollection object with methods to play pre-configured sounds.
   *
   * @param sounds {@code SoundsCollection} object
   */
  public void setSounds(final SoundsCollection sounds) {
    this.sounds = sounds;
  }

  /**
   * Getter for Logger object, which provides logging functionality.
   *
   * @return {@code Logger} object
   */
  public Logger getLogger() {
    return logger;
  }

  /**
   * Getter for InputStream STD IN object, which provides input from STD in functionality.
   *
   * @return {@code InputStream} object
   */
  public InputStream getStdIn() {
    return stdIn;
  }

  /**
   * Getter for Read object with methods to read data from DB.
   *
   * @return {@code Read} object
   */
  public Read getRead() {
    return read;
  }

  /**
   * Getter for Store object with methods to store data into DB.
   *
   * @return {@code Store} object
   */
  public Store getStore() {
    return store;
  }

  /**
   * Getter for Login object with methods to manage user session.
   *
   * @return {@code Login} object
   */
  public Login getLogin() {
    return login;
  }

  /**
   * Getter for ConsoleInput with methods to process user input from console.
   *
   * @return {@code ConsoleInput} object
   */
  public ConsoleInput getConIn() {
    return conIn;
  }
}
