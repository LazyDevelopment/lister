package edu.lazydevelopment.lister.sounds;

import edu.lazydevelopment.lister.logic.ListerObjects;
import kuusisto.tinysound.Sound;
import kuusisto.tinysound.TinySound;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

/**
 * Contains methods and fields to play sounds.
 */
public class SoundsCollection {

  private final Sound copied;
  private final Sound error;
  private final Sound stored;
  private final Sound hello;
  private final Sound start;
  private final Sound finish;
  private final Sound duplicate;
  private final Sound watched;
  private final Sound soundOn;

  /**
   * Initializes sound engine and loads sounds to memory.
   *
   * @throws IOException if it is not possible
   */
  public SoundsCollection() throws IOException {
    int i = 0;
    while (!TinySound.isInitialized() && i < 100) {
      TinySound.init();
      i++;
    }

    copied = load("Copied.ogg");
    error = load("Error.ogg");
    stored = load("Stored.ogg");
    hello = load("Hello.ogg");
    start = load("Start.ogg");
    finish = load("Finish.ogg");
    duplicate = load("Dublicate.ogg");
    watched = load("Watched.ogg");
    soundOn = load("Sound-on.ogg");
  }

  /**
   * Smart loading of sound from file to memory.
   *
   * @param path to the file
   * @return Sound object with sound inside
   * @throws IOException if it is not possible
   */
  @NotNull
  private Sound load(final String path) throws IOException {
    Sound sound;
    int i = 0;
    do {
      sound = TinySound.loadSound(path);
      i++;
    } while (sound == null && i < 10);
    if (sound == null) {
      throw new IOException("Tried to load '" + path + "' sound 10 times, but it is still Null");
    }
    return sound;
  }

  /**
   * Garbage collector, which unloads all sounds from memory and shuts down sound engine.
   */
  public void unload() {
    copied.unload();
    error.unload();
    stored.unload();
    hello.unload();
    start.unload();
    finish.unload();
    duplicate.unload();
    watched.unload();
    soundOn.unload();
    while (TinySound.isInitialized()) {
      TinySound.shutdown();
    }
  }

  /**
   * Plays the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void playHello(@NotNull final ListerObjects lisOb) {
    if (lisOb.isSoundEnabled(lisOb)) {
      hello.play();
    }
  }

  /**
   * Plays the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void playSoundOn(@NotNull final ListerObjects lisOb) {
    if (lisOb.isSoundEnabled(lisOb)) {
      soundOn.play();
    }
  }

  /**
   * Plays the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void playWatched(@NotNull final ListerObjects lisOb) {
    if (lisOb.isSoundEnabled(lisOb)) {
      watched.play();
    }
  }

  /**
   * Plays the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void playDuplicate(@NotNull final ListerObjects lisOb) {
    if (lisOb.isSoundEnabled(lisOb)) {
      duplicate.play();
    }
  }

  /**
   * Plays the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void playStart(@NotNull final ListerObjects lisOb) {
    if (lisOb.isSoundEnabled(lisOb)) {
      start.play();
    }
  }

  /**
   * Plays the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void playFinish(@NotNull final ListerObjects lisOb) {
    if (lisOb.isSoundEnabled(lisOb)) {
      finish.play();
    }
  }

  /**
   * Plays the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void playCopied(@NotNull final ListerObjects lisOb) {
    if (lisOb.isSoundEnabled(lisOb)) {
      copied.play();
    }
  }

  /**
   * Plays the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void playError(@NotNull final ListerObjects lisOb) {
    if (lisOb.isSoundEnabled(lisOb)) {
      error.play();
    }
  }

  /**
   * Plays the sound.
   *
   * @param lisOb instance of class with common objects
   */
  public void playStored(@NotNull final ListerObjects lisOb) {
    if (lisOb.isSoundEnabled(lisOb)) {
      stored.play();
    }
  }
}
