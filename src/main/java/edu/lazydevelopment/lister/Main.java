package edu.lazydevelopment.lister;

import ch.qos.logback.classic.LoggerContext;
import edu.lazydevelopment.lister.db.Connect;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.logic.ListerObjects;
import edu.lazydevelopment.lister.menu.MainMenu;
import edu.lazydevelopment.lister.security.Login;
import edu.lazydevelopment.lister.sounds.SoundsCollection;
import org.jasypt.util.text.AES256TextEncryptor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.*;

/**
 * Main class.
 */
public final class Main {

  /**
   * Hiding default constructor because of security issue.
   */
  private Main() {
  }

  /**
   * Starting point of app execution.
   *
   * @param args string of any arguments, typed when app was executed. Currently are ignored
   */
  public static void main(@NotNull final String[] args) {
    final ListerObjects lisOb = new ListerObjects();
    final Logger logger = lisOb.getLogger();
    if (args.length > 0) {
      logger.info("{}Lister{} is an interactive app. You don't need to pass any command line "
              + "arguments to the execution string. {}Just run 'java -jar <filename>.jar'{}",
          ANSI_BRIGHT_BLUE, ANSI_RESET, ANSI_BRIGHT_WHITE, ANSI_RESET);
      exit(lisOb, 0);
    }

    logger.info("This is {}Lister.{} Enjoy {}:-){}\n",
        ANSI_BRIGHT_BLUE, ANSI_BRIGHT_GREEN, ANSI_BRIGHT_YELLOW, ANSI_RESET);

    lisOb.setConnect(connectToDb(lisOb));
    try {
      if (lisOb.getConnect() == null) {
        logger.error("Connection to DB is null");
        exit(lisOb, 1);
      } else if (lisOb.getConnect().getConnection().isReadOnly()) {
        logger.error("Connection to DB is read-only");
        exit(lisOb, 1);
      }
    } catch (SQLException e) {
      logger.error("Unable to get state of DB connection", e);
      exit(lisOb, 1);
    }

    lisOb.setEncryptor(doLogin(lisOb));
    if (MainMenu.isFinish()) {
      exit(lisOb, 1);
    }

    lisOb.getLists().showLists(lisOb);
    if (MainMenu.isFinish()) {
      exit(lisOb, 1);
    }

    if (lisOb.isSoundEnabled(lisOb)) {
      try {
        lisOb.setSounds(new SoundsCollection());
        lisOb.getSounds().playHello(lisOb);
      } catch (IOException e) {
        logger.error("Unable to init audio, disabling sound", e);
        if (!lisOb.setSoundState(false, lisOb)) {
          logger.error("Unable to disable sound, exiting");
          exit(lisOb, 1);
        }
      }
    }

    MainMenu.show(lisOb);

    exit(lisOb, MainMenu.isFinish() ? 0 : 1);
  }

  /**
   * Creates or authorizes user.
   *
   * @param lisOb instance of class with common objects
   * @return instance of Encryptor object with valid password set, will be used to encrypt and
   *        decrypt data in DB
   */
  private static AES256TextEncryptor doLogin(@NotNull final ListerObjects lisOb) {
    final Read read = lisOb.getRead();
    final Connect cn = lisOb.getConnect();
    final Login login = lisOb.getLogin();

    try {
      if (read.getTables(lisOb).contains("_settings")
          && !read.getRegex(cn, "_settings", null).isEmpty()) {
        return login.authorize(lisOb);
      } else {
        return login.createLogin(lisOb);
      }
    } catch (Exception e) {
      lisOb.getLogger().error("Unable to login", e);
      MainMenu.setFinish(true);
    }
    return new AES256TextEncryptor();
  }

  /**
   * Last method, which is called before app is closed. Unloads sounds, closes DB connection, runs
   * Java garbage collector and closes the app with status 0 or 1
   *
   * @param lisOb  instance of class with common objects
   * @param status 0 if user asked to exit or 1 if it is called because of error
   */
  private static void exit(@NotNull final ListerObjects lisOb, final int status) {
    final SoundsCollection sounds = lisOb.getSounds();
    final Connect cn = lisOb.getConnect();

    if (sounds != null) {
      sounds.unload();
    }
    if (cn != null) {
      try {
        cn.close();
      } catch (SQLException e) {
        lisOb.getLogger().error("Unable to close connection to DB", e);
      }
    }
    LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    loggerContext.stop();
    System.exit(status);
  }

  /**
   * Sets up DB connection.
   *
   * @param lisOb instance of class with common objects
   * @return instance of DB connection object
   */
  private static Connect connectToDb(@NotNull final ListerObjects lisOb) {
    final Logger logger = lisOb.getLogger();
    Connect cn = null;

    try {
      cn = new Connect("lister.db");
      if (!cn.getConnection().isClosed()) {
        logger.info("DB is connected\n");
      }
    } catch (ClassNotFoundException e) {
      logger.error("Class 'org.sqlite.JDBC' couldn't be found during DB connect", e);
    } catch (SQLException e) {
      logger.error("Unable to connect to DB", e);
    }
    return cn;
  }
}
