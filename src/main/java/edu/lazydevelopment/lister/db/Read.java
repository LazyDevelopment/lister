package edu.lazydevelopment.lister.db;

import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jasypt.util.text.AES256TextEncryptor;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Contains methods to read data from DB.
 */
public class Read {

  private static final String WRONG_CONNECTION = "DB connection is wrong";
  private static final String VIEWED = "_viewed";
  private static final String SELECT_ALL_FROM = "select * from ";

  /**
   * Returns list of tables (except of system table and all tables with viewed items).
   *
   * @param lisOb instance of class with common objects
   * @return {@code List<String>} of tables in DB
   * @throws SQLException if not able to work with DB
   */
  public List<String> getTables(@NotNull final ListerObjects lisOb) throws SQLException {
    final Connect cn = lisOb.getConnect();

    if (!cn.verifyConnection()) {
      throw new SQLException(WRONG_CONNECTION);
    }

    List<String> list = new LinkedList<>();
    try (ResultSet result = cn.getStatement()
        .executeQuery("SELECT name FROM sqlite_master WHERE type='table';")) {
      String n;
      while (result.next()) {
        n = result.getString("name");
        if (!n.contains(VIEWED) && !n.contains("sqlite_sequence")) {
          list.add(n);
        }
      }
    }
    if (list.isEmpty()) {
      lisOb.getLogger().info("There are no lists");
    }

    cn.close();
    return list;
  }

  /**
   * Reads first or last item (indicated by {@code isLastItem} switch) from the list if it is not
   * Regex, removes it from there, stores it in corresponding list of viewed items and returns the
   * item as a String.
   *
   * @param lisOb      instance of class with common objects
   * @param list       from which first item should be retrieved
   * @param isLastItem if {@code true} - reads last item; if {@code false} - reads 1st item
   * @return the item as a {@code String} or empty string if the list is empty
   * @throws SQLException if unable to work with DB; unable to decrypt the item or it is {@code ""};
   *                      unable to store the item to viewed list
   */
  public String getSomeItemAndRemove(@NotNull final ListerObjects lisOb, final String list,
                                     final boolean isLastItem) throws SQLException {
    final Connect cn = lisOb.getConnect();
    final AES256TextEncryptor encryptor = lisOb.getEncryptor();

    String item = "";
    String id = null;
    StringBuilder sql = new StringBuilder();
    if (!lisOb.getRead().getAllItems(lisOb, list).isEmpty()) {
      if (!cn.verifyConnection()) {
        throw new SQLException(WRONG_CONNECTION);
      }
      if (isLastItem) {
        sql.append(SELECT_ALL_FROM).append(list).append(" order by id desc limit 1;");
        try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
          try (ResultSet result = ps.executeQuery()) {
            item = encryptor.decrypt(result.getString("item"));
            id = result.getString("id");
          }
        }
      } else {
        sql.append(SELECT_ALL_FROM).append(list).append(" order by id asc limit 2;");
        try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
          try (ResultSet result = ps.executeQuery()) {
            result.next();
            result.next();

            item = encryptor.decrypt(result.getString("item"));
            id = result.getString("id");
          }
        }
      }
    }

    if (id != null && !Objects.equals(item, "") && Integer.parseInt(id) > 1) {
      if (!cn.verifyConnection()) {
        throw new SQLException(WRONG_CONNECTION);
      }
      sql.setLength(0);
      sql.append("delete from ").append(list).append(" where id = ").append(id).append(";");
      try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
        ps.execute();
      }
      if (!lisOb.getStore().storeItem(lisOb.getConnect(), item, list + VIEWED,
          lisOb.getEncryptor())) {
        cn.close();
        throw new SQLException("Unable to store '" + item + "' in the '" + list + "_viewed' list");
      }
      cn.close();
      return item;
    } else {
      lisOb.getLogger().warn("This list is empty, nothing to get");
      cn.close();
      return "";
    }
  }

  /**
   * Just reads all items from the list and returns them as a List.
   *
   * @param lisOb instance of class with common objects
   * @param list  from which items should be retrieved
   * @return {@code List<String>} with all items except of Regex from the list in order as they were
   *              stored
   * @throws SQLException if unable to work with DB
   */
  public List<String> getAllItems(@NotNull final ListerObjects lisOb, final String list)
      throws SQLException {
    final Connect cn = lisOb.getConnect();
    final AES256TextEncryptor encryptor = lisOb.getEncryptor();

    if (!cn.verifyConnection()) {
      throw new SQLException(WRONG_CONNECTION);
    }

    List<String> res;
    StringBuilder sql = new StringBuilder();
    if (list.contains(VIEWED)) {
      sql.append(SELECT_ALL_FROM).append(list).append(" order by id asc;");
      try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
        try (ResultSet result = ps.executeQuery()) {
          res = extractItems(encryptor, result);
        }
      }
    } else {
      sql.append(SELECT_ALL_FROM).append(list).append(" where id <> 1 order by id asc;");
      try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
        try (ResultSet result = ps.executeQuery()) {
          res = extractItems(encryptor, result);
        }
      }
    }
    cn.close();
    return res;
  }

  /**
   * Extracts all items from {@code ResultSet} object to {@code List<String>}.
   *
   * @param encryptor instance of Encryptor object with valid password set to decrypt items
   * @param result    instance of {@code ResultSet} with result of SQL execution
   * @return instance of {@code List<String>}, where items will be stored
   * @throws SQLException if unable t process result
   */
  @NotNull
  private List<String> extractItems(final AES256TextEncryptor encryptor,
                                    @NotNull final ResultSet result) throws SQLException {
    List<String> list = new LinkedList<>();
    while (result.next()) {
      list.add(encryptor.decrypt(result.getString("item")));
    }
    return list;
  }

  /**
   * Returns RegEx for provided list.
   *
   * @param cn        Instance of DB connector
   * @param list      from which Regex should be retrieved
   * @param encryptor Instance of Encryptor object with valid password set to decrypt Regex
   * @return Regex as a {@code String} or encrypted string if encryptor is Null, {@code ""} if DB
   *                  connection is closed
   * @throws SQLException if unable to work with DB
   */
  public String getRegex(@NotNull final Connect cn, final String list,
                         final AES256TextEncryptor encryptor) throws SQLException {
    if (!cn.verifyConnection()) {
      throw new SQLException(WRONG_CONNECTION);
    }

    boolean isClosed;
    String encrypted = "";
    try (PreparedStatement ps = cn.getConnection()
            .prepareStatement(SELECT_ALL_FROM + list + " order by id asc limit 1;")) {
      try (ResultSet result = ps.executeQuery()) {
        isClosed = result.isClosed();
        if (!isClosed) {
          encrypted = result.getString("item");
        }
      }
    }

    cn.close();
    if (isClosed) {
      return "";
    }
    if (encryptor == null) {
      return encrypted;
    } else {
      return encryptor.decrypt(encrypted);
    }
  }

  /**
   * Returns password validation string from settings table.
   *
   * @param cn        Instance of DB connector
   * @param encryptor Instance of Encryptor object with valid password set to decrypt
   *                  password validation string
   * @return          password validation string
   * @throws SQLException if unable to work with DB
   */
  public String getPassValidationString(final Connect cn, final AES256TextEncryptor encryptor)
      throws SQLException {
    return this.getRegex(cn, "_settings", encryptor);
  }

  /**
   * Returns value of Sound state setting.
   *
   * @param lisOb     Instance of class with common objects
   * @return          {@code true} if sound enabled; {@code false} if disabled
   * @throws SQLException if unable to work with DB
   * @throws NoSuchFieldException if Sound option is not set
   * @throws NoSuchMethodException if Sound option has wrong value
   */
  public boolean getSoundState(@NotNull final ListerObjects lisOb)
      throws SQLException, NoSuchFieldException, NoSuchMethodException {
    final Connect cn = lisOb.getConnect();
    final AES256TextEncryptor encryptor = lisOb.getEncryptor();

    if (!cn.verifyConnection()) {
      throw new SQLException(WRONG_CONNECTION);
    }

    boolean isClosed;
    String encrypted = "";
    try (PreparedStatement ps = cn.getConnection()
        .prepareStatement("SELECT item FROM _settings WHERE id = 2;")) {
      try (ResultSet result = ps.executeQuery()) {
        isClosed = result.isClosed();
        if (!isClosed) {
          encrypted = result.getString("item");
        }
      }
    }
    cn.close();

    if (isClosed) {
      throw new NoSuchFieldException("Sound option is not set in DB");
    }
    String soundState = encryptor.decrypt(encrypted);
    if (soundState.equalsIgnoreCase("sound enabled")) {
      return true;
    } else if (soundState.equalsIgnoreCase("sound disabled")) {
      return false;
    } else {
      throw new NoSuchMethodException(String.format("Sound option has wrong value in DB: %s",
          soundState));
    }
  }
}
