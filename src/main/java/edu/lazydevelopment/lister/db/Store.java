package edu.lazydevelopment.lister.db;

import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jasypt.util.text.AES256TextEncryptor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;

/**
 * Contains methods to store data in DB.
 */
public class Store {

  private static final String WRONG_CONNECTION = "DB connection is wrong";
  private static final String SETTINGS = "_settings";
  private static final String VIEWED = "_viewed";
  private static final String UNABLE_TO_WORK_WITH_DB = "Unable to work with DB";

  /**
   * Stores provided list of items in provided table (list) in DB during 1 transaction. Then reads
   * all items from that table (list) and validates that each provided item is stored there.
   *
   * @param lisOb instance of class with common objects
   * @param items {@code List<String>} of items to be stored
   * @param list  where the item should be stored
   * @return {@code true} if all items are stored successfully; {@code false} if unable to store
   *              some item or at least 1 item is missing after store items transaction
   * @throws SQLException if unable to work with DB
   */
  public boolean storeItemsList(@NotNull final ListerObjects lisOb, final List<String> items,
                                final String list) throws SQLException {
    final Connect cn = lisOb.getConnect();

    if (!cn.verifyConnection()) {
      throw new SQLException(WRONG_CONNECTION);
    }

    if (cn.getConnection().getAutoCommit()) {
      cn.getConnection().setAutoCommit(false);
    }

    final AES256TextEncryptor encryptor = lisOb.getEncryptor();
    for (String item : items) {
      try (PreparedStatement ps = cn.getConnection()
          .prepareStatement("insert into " + list
              + " (item) values ('" + encryptor.encrypt(item) + "')")) {
        ps.execute();
      } catch (SQLException e) {
        cn.getConnection().rollback();
        cn.getConnection().setAutoCommit(true);
        lisOb.getLogger().error("Unable to insert {}{}{} into DB as part of transaction",
            ANSI_BRIGHT_YELLOW, item, ANSI_RESET);
        return false;
      }
    }
    if (!cn.getConnection().getAutoCommit()) {
      cn.getConnection().commit();
      cn.getConnection().setAutoCommit(true);
    }
    cn.close();

    final List<String> storedItems = lisOb.getRead().getAllItems(lisOb, list);
    StringBuilder val = new StringBuilder();
    for (String item : items) {
      if (storedItems.contains(item)) {
        val.append("+");
      } else {
        val.append("-");
      }
    }
    return !val.toString().contains("-");
  }

  /**
   * Renames a table, also renames corresponding table for viewed items if that is a list.
   *
   * @param lisOb   instance of class with common objects
   * @param oldName of table to be renamed
   * @param newName of that table
   * @return {@code true} if table(s) is(are) renamed; {@code false} if unable to work with DB
   */
  public boolean renameTableList(@NotNull final ListerObjects lisOb, final String oldName,
                                 final String newName) {
    final String alterTable = "alter table ";
    final String renameTo = " rename to ";
    final Connect cn = lisOb.getConnect();

    try {
      if (!cn.verifyConnection()) {
        throw new SQLException(WRONG_CONNECTION);
      }

      StringBuilder sql = new StringBuilder();
      sql.append(alterTable).append(oldName).append(renameTo).append(newName).append(";");
      try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
        ps.execute();
      }
      if (!oldName.contains(SETTINGS)) {
        sql.setLength(0);
        sql.append(alterTable).append(oldName).append(VIEWED).append(renameTo).append(newName)
            .append(VIEWED).append(";");
        try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
          ps.execute();
        }
      }
      cn.close();
    } catch (SQLException e) {
      lisOb.getLogger().error(UNABLE_TO_WORK_WITH_DB, e);
      try {
        cn.close();
      } catch (SQLException ignored) {
        //ignored
      }
      return false;
    }
    return true;
  }

  /**
   * Removes a table, also removes corresponding table for viewed items if that is a list.
   *
   * @param lisOb instance of class with common objects
   * @param name  of the table to be removed
   * @return {@code true} if table(s) is(are) removed; {@code false} if unable to work with DB
   */
  public boolean dropTableList(@NotNull final ListerObjects lisOb, final String name) {
    final String dropTableIfExists = "drop table if exists ";
    final Connect cn = lisOb.getConnect();

    try {
      if (!cn.verifyConnection()) {
        throw new SQLException(WRONG_CONNECTION);
      }

      StringBuilder sql = new StringBuilder();
      sql.append(dropTableIfExists).append(name).append(";");
      try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
        ps.execute();
      }
      if (!name.contains(SETTINGS)) {
        sql.setLength(0);
        sql.append(dropTableIfExists).append(name).append(VIEWED).append(";");
        try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
          ps.execute();
        }
      }
      cn.close();
    } catch (SQLException e) {
      lisOb.getLogger().error(UNABLE_TO_WORK_WITH_DB, e);
      try {
        cn.close();
      } catch (SQLException ignored) {
        // ignored
      }
      return false;
    }
    return true;
  }

  /**
   * Creates new table in DB. If the table is a list, also creates table for viewed items
   *
   * @param lisOb instance of class with common objects
   * @param name  of the table, which should be created
   * @return {@code true} if table(s) is(are) created; {@code false} if unable to work with DB
   */
  public boolean createTableList(@NotNull final ListerObjects lisOb, final String name) {
    final String createTableBegin = "create table if not exists ";
    final String createTableEnd = " (item unique on conflict ignore, "
        + "id integer primary key on conflict ignore autoincrement);";
    final Connect cn = lisOb.getConnect();

    try {
      if (!cn.verifyConnection()) {
        throw new SQLException(WRONG_CONNECTION);
      }

      StringBuilder sql = new StringBuilder();
      sql.append(createTableBegin).append(name).append(createTableEnd);
      try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
        ps.execute();
      }
      if (!name.contains(SETTINGS)) {
        sql.setLength(0);
        sql.append(createTableBegin).append(name).append(VIEWED).append(createTableEnd);
        try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
          ps.execute();
        }
      }
      cn.close();
    } catch (SQLException e) {
      lisOb.getLogger().error(UNABLE_TO_WORK_WITH_DB, e);
      try {
        cn.close();
      } catch (SQLException ignored) {
        //  ignored
      }
      return false;
    }
    return true;
  }

  /**
   * Updates regex of the list if new one isn't the same as current one.
   *
   * @param newRegex which should be stored
   * @param lisOb    instance of class with common objects
   * @param list     where regex should be updated
   * @return {@code true} if regex was updated; {@code false} if not
   * @throws SQLException if unable to work with DB
   */
  public boolean updateRegex(final String newRegex, @NotNull final ListerObjects lisOb,
                             final String list)
      throws SQLException {
    final Connect cn = lisOb.getConnect();
    final Read read = lisOb.getRead();
    final AES256TextEncryptor encryptor = lisOb.getEncryptor();

    if (!read.getRegex(cn, list, encryptor).equals(newRegex)) {
      if (!cn.verifyConnection()) {
        throw new SQLException(WRONG_CONNECTION);
      }
      try (PreparedStatement ps = cn.getConnection()
          .prepareStatement("UPDATE " + list + " SET item = '" + encryptor.encrypt(newRegex)
              + "' WHERE id = 1;")) {
        ps.execute();
      }
      cn.close();
      return newRegex.equals(read.getRegex(cn, list, encryptor));
    } else {
      lisOb.getLogger().info("New regex is the same as currently stored. Nothing to do");
      return false;
    }
  }

  /**
   * Stores provided item as an encrypted string in provided list, reads it from there and validates
   * that it is the same as original one.
   *
   * @param cn        instance of DB connection object
   * @param item      String to be encrypted and stored
   * @param list      where the item should be stored
   * @param encryptor to encrypt and decrypt data in DB using valid password
   * @return {@code true} if stored string is equal to original one; {@code false} if not
   * @throws SQLException if unable to work with DB
   */
  public boolean storeItem(@NotNull final Connect cn, final String item, final String list,
                           final AES256TextEncryptor encryptor)
      throws SQLException {
    if (!cn.verifyConnection()) {
      throw new SQLException(WRONG_CONNECTION);
    }

    final String encryptedItem = encryptor.encrypt(item);

    StringBuilder sql = new StringBuilder();
    sql.append("insert into ").append(list).append(" (item) values ('").append(encryptedItem)
        .append("');");
    try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
      ps.execute();
    }

    String requestedItem;
    boolean res;
    sql.setLength(0);
    sql.append("select * from ").append(list).append(" order by id desc limit 1;");
    try (PreparedStatement ps = cn.getConnection().prepareStatement(sql.toString())) {
      try (ResultSet result = ps.executeQuery()) {
        requestedItem = result.getString("item");
        res = encryptedItem.equals(requestedItem);
      }
    }
    cn.close();
    return res;
  }

  /**
   * Creates or updates the sound state setting.
   *
   * @param soundToBeOn  {@code true} for "sound enabled" and {@code false} for "sound disabled"
   * @param lisOb         instance of class with common objects
   * @return              {@code true} if stored string is equal to requested one;
   *                      {@code false} if not
   * @throws SQLException if unable to work with DB
   */
  public boolean storeSoundState(final boolean soundToBeOn, @NotNull final ListerObjects lisOb)
      throws SQLException {
    final Connect cn = lisOb.getConnect();
    final Read read = lisOb.getRead();
    final AES256TextEncryptor encryptor = lisOb.getEncryptor();
    final Logger logger = lisOb.getLogger();
    final String soundState = soundToBeOn ? "sound enabled" : "sound disabled";
    boolean storedState;

    try {
      storedState = read.getSoundState(lisOb);
    } catch (NoSuchFieldException fe) {
      logger.warn(fe.getMessage());
      return lisOb.getStore().storeItem(cn, soundState, SETTINGS, encryptor);
    } catch (NoSuchMethodException me) {
      logger.error(me.getMessage());
      throw new SQLException();
    }
    if (storedState != soundToBeOn) {
      if (!cn.verifyConnection()) {
        throw new SQLException(WRONG_CONNECTION);
      }
      try (PreparedStatement ps = cn.getConnection()
          .prepareStatement("UPDATE " + SETTINGS + " SET item = '" + encryptor.encrypt(soundState)
          + "' WHERE id = 2;")) {
        ps.execute();
      }
      cn.close();
    } else {
      lisOb.getSounds().playSoundOn(lisOb);
      return true;
    }
    lisOb.getSounds().playSoundOn(lisOb);

    return soundToBeOn == getRequestedItem(lisOb);
  }

  /**
   * Validates what was stored in DB by storeSoundState().
   *
   * @param lisOb instance of class with common objects
   * @return sound state from DB
   * @throws SQLException if unable to store value for sound state settings
   */
  private boolean getRequestedItem(@NotNull ListerObjects lisOb) throws SQLException {
    boolean requestedItem;
    try {
      requestedItem = lisOb.getRead().getSoundState(lisOb);
    } catch (ReflectiveOperationException e) {
      lisOb.getLogger().error("Unable to store value for sound state settings", e);
      throw new SQLException();
    }
    return requestedItem;
  }
}
