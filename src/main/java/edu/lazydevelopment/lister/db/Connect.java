package edu.lazydevelopment.lister.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Implements DB connection functionality: connect, reconnect, get connection or statement, close
 * connection.
 */
public class Connect {

  private Connection connection;
  private Statement statement;
  private String dbUrl = "jdbc:sqlite:";

  /**
   * Constructor for DB connector: creates DB connection during object's creation, runs DB integrity
   * check validation and does vacuum DB.
   *
   * @param pathToDbFile path to valid SQLite DB file
   * @throws ClassNotFoundException if Class {@code org.sqlite.JDBC} couldn't be found
   * @throws SQLException           if DB is read-only or DB integrity check is failed
   */
  public Connect(final String pathToDbFile) throws ClassNotFoundException, SQLException {
    Class.forName("org.sqlite.JDBC");

    dbUrl = dbUrl + pathToDbFile;
    connection = DriverManager.getConnection(dbUrl);
    statement = connection.createStatement();
    statement.setQueryTimeout(3);

    String ok;
    try (ResultSet result = statement.executeQuery("PRAGMA integrity_check;")) {
      ok = result.getString(1);
    }
    if (!ok.equals("ok")) {
      throw new SQLException("DB integrity check is failed: " + ok);
    }

    statement.execute("VACUUM;");

    close();
  }

  /**
   * Reconnects DB connection.
   *
   * @return {@code true} if connection is open; {@code false} if it is closed
   * @throws SQLException if DB connection is read-only
   */
  boolean reconnect() throws SQLException {
    if (connection.isClosed() || connection.isReadOnly()) {
      connection = DriverManager.getConnection(dbUrl);
      statement = connection.createStatement();
      statement.setQueryTimeout(3);
    }
    return !connection.isClosed();
  }

  /**
   * Verifies DB connection and tries to reconnect if it is closed or read-only.
   *
   * @return {@code true} if connection is ok; {@code false} if it is still closed or read-only
   * @throws SQLException if not able to work with DB
   */
  boolean verifyConnection() throws SQLException {
    if (connection.isClosed() && !reconnect()) {
      return false;
    }
    if (!connection.isValid(3) && !reconnect()) {
      return false;
    }
    if (connection.isReadOnly()) {
      reconnect();
      return !connection.isReadOnly();
    }
    return true;
  }

  /**
   * Getter for current instance of Connection object.
   *
   * @return instance of Connection object
   */
  public Connection getConnection() {
    return connection;
  }

  /**
   * Getter for current instance of Statement object.
   *
   * @return instance of Statement object
   */
  Statement getStatement() {
    return statement;
  }

  /**
   * Commits all not yet committed data if any and closes connection to DB.
   *
   * @throws SQLException if not able to:<br> - get Connection;<br> - get AutoCommit state;<br> -
   *                      commit data;<br> - get Connection state;<br> - close Connection<br>
   */
  public void close() throws SQLException {
    if (!getConnection().isClosed()) {
      if (!getConnection().getAutoCommit()) {
        getConnection().commit();
      }
      getConnection().close();
    }
  }
}
