/**
 * Provides classes to work with file system (except of DB and logging related).
 */
package edu.lazydevelopment.lister.files;
