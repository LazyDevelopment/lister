package edu.lazydevelopment.lister.files;

import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_BRIGHT_YELLOW;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_CYAN;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_GREEN;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_PURPLE;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.ANSI_YELLOW;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;

/**
 * Contains methods to work with files (except of DB and logging).
 */
public class FilesOps {

  /**
   * Writes items from the {@code List<String>} to file, mentioned in provided path.
   *
   * @param logger         to output errors
   * @param outputFilePath path to file, where items will be outputted (usually file)
   * @param items          {@code List<String>} of items to be written
   * @return number of outputted items
   */
  public int exportItemsToFile(final Logger logger, final String outputFilePath,
                               final List<String> items) {
    int i = 0;
    try (BufferedSink sink = Okio.buffer(Okio.sink(Paths.get(outputFilePath), CREATE, APPEND))) {
      for (String item : items) {
        sink.writeUtf8(item).writeUtf8(System.lineSeparator());
        i++;
      }
      sink.flush();
    } catch (Exception e) {
      logger.error("Unable to write data to the file. {}{}{}"
              + " lines may be written before this error occurred",
          ANSI_BRIGHT_YELLOW, i, ANSI_RESET, e);
      return i;
    }
    return i;
  }

  /**
   * Tries to open file with provided path to read.
   *
   * @param path   of file to be opened
   * @param logger to output warnings
   * @return {@code true} if file was opened successfully; {@code false} if not
   */
  public boolean doesFileExist(final String path, final Logger logger) {
    try (BufferedSource source = Okio.buffer(Okio.source(Paths.get(path), READ))) {
      String line = source.readUtf8Line();
      if (line == null) {
        logger.warn("File '{}{}{}' is empty", ANSI_BRIGHT_YELLOW, path, ANSI_RESET);
      }
      return true;
    } catch (Exception e) {
      logger.warn("File '{}{}{}' not found or couldn't be opened. Try again",
          ANSI_BRIGHT_YELLOW, path, ANSI_RESET, e);
      return false;
    }
  }

  /**
   * Tries to open file with provided path (or create it) and write new line ({@code '\n'}) there.
   *
   * @param path   of file to be opened
   * @param logger logger to output warnings
   * @return {@code true} if file was opened successfully; {@code false} if not
   */
  public boolean canFileBeWrittenOrCreated(final String path, final Logger logger) {
    try (BufferedSink sink = Okio.buffer(Okio.sink(Paths.get(path), CREATE, APPEND))) {
      sink.writeUtf8(System.lineSeparator());
      sink.flush();
      return true;
    } catch (Exception e) {
      logger.warn("File '{}{}{}' couldn't be opened with write permissions or created. Try again",
          ANSI_BRIGHT_YELLOW, path, ANSI_RESET, e);
      return false;
    }
  }

  /**
   * Imports lines from provided file into {@code List<String>}.
   *
   * @param path        of file, from which lines need to be imported
   * @param logger      to output usual messages and errors
   * @param regex       for current list
   * @param itemsStored {@code Set<String>} of already stored lines
   * @param itemsViewed {@code Set<String>} of already viewed lines
   * @return {@code Map<String, List<String>>}, which contains only {@code List<String> digits} with
   *        only {@code errcount} equal to "-1" in case of inability to work with file;
   *        {@code List<String digits} containing {@code errcount, linesProcessed, linesStored} as
   *        strings in that order, {@code List<String> failed} with duplicated lines,
   *        {@code List<String> toStore} with lines to be stored
   */
  public Map<String, List<String>> importLinesFromFileToList(final String path, final Logger logger,
                                                             final String regex,
                                                             final Set<String> itemsStored,
                                                             final Set<String> itemsViewed) {
    byte errcount = 0;
    int linesProcessed = 0;
    int linesStored = 0;
    List<String> failed = new LinkedList<>();
    List<String> toStore = new LinkedList<>();
    List<String> digits = new LinkedList<>();
    Map<String, List<String>> result = new HashMap<>();
    try (BufferedSource source = Okio.buffer(Okio.source(Paths.get(path), READ))) {
      for (String currentLine; (currentLine = source.readUtf8Line()) != null; ) {
        linesProcessed++;
        if (processCurrentLine(logger, currentLine, regex, failed, toStore,
            itemsStored, itemsViewed)) {
          linesStored++;
        } else {
          errcount++;
        }
      }
    } catch (Exception e) {
      logger.error("File not found or couldn't be opened", e);
      digits.add("-1");
      result.put("digits", digits);
      return result;
    }
    digits.add(Byte.toString(errcount));
    digits.add(Integer.toString(linesProcessed));
    digits.add(Integer.toString(linesStored));
    result.put("digits", digits);
    result.put("failed", failed);
    result.put("toStore", toStore);
    return result;
  }

  /**
   * Processes current line from file and adds it to corresponding list.
   *
   * @param logger      to output usual messages and errors
   * @param currentLine to be processed
   * @param regex       for current list
   * @param failed      {@code List<String>} of duplicated lines
   * @param toStore     {@code List<String>} of lines to be stored into list
   * @param itemsStored {@code Set<String>} of already stored lines
   * @param itemsViewed {@code Set<String>} of already viewed lines
   * @return {@code true} if # of stored lines need to be incremented; {@code false} if not
   */
  private boolean processCurrentLine(final Logger logger, @NotNull final String currentLine,
                                     final String regex, final List<String> failed,
                                     final List<String> toStore, final Set<String> itemsStored,
                                     final Set<String> itemsViewed) {
    boolean increment = false;
    if (currentLine.matches(regex)) {
      if (!itemsStored.contains(currentLine) && !itemsViewed.contains(currentLine)) {
        toStore.add(currentLine);
        increment = true;
        itemsStored.add(currentLine);
        logger.info("{}Success{}: {}", ANSI_GREEN, ANSI_RESET, currentLine);
      } else if (itemsStored.contains(currentLine)) {
        logger.info("{}Duplicate{}:   {}", ANSI_YELLOW, ANSI_RESET, currentLine);
        failed.add(currentLine);
      } else {
        logger.info("{}Viewed{}:      {}", ANSI_PURPLE, ANSI_RESET, currentLine);
        failed.add(currentLine);
      }
    } else {
      logger.info("{}NoMatch{}: {}", ANSI_CYAN, ANSI_RESET, currentLine);
      failed.add(currentLine);
    }
    return increment;
  }
}
