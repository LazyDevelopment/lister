package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.files.FilesOps;
import edu.lazydevelopment.lister.logic.ImportExportData;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ExportItems} class.
 */
public class ExportItemsTest extends GeneralTest {

  @Mock
  private ListerObjects obMocked;
  @Mock
  private FilesOps filesOpsMocked;
  @Mock
  private Read readMocked;
  @Mock
  private ImportExportData imExDataMocked;

  @BeforeMethod
  public void reset() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testExportItems_correct() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(imExDataMocked.getValidPath(obMocked)).thenReturn("/valid/path");
    List<String> itemsStored = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewed = Arrays.asList("1st viewed item", "2nd viewed item");
    when(readMocked.getAllItems(obMocked, "list")).thenReturn(itemsStored);
    when(readMocked.getAllItems(obMocked, "list_viewed")).thenReturn(itemsViewed);
    when(filesOpsMocked.exportItemsToFile(logger, "/valid/path", itemsStored))
        .thenReturn(2);
    when(filesOpsMocked.exportItemsToFile(logger, "/valid/path.viewed.txt", itemsViewed))
        .thenReturn(2);

    ExportItems.exportItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getFilesOps();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(imExDataMocked, times(1)).getValidPath(obMocked);
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "list_viewed");
    ignore = verify(filesOpsMocked, times(1))
        .exportItemsToFile(logger, "/valid/path", itemsStored);
    ignore = verify(filesOpsMocked, times(1))
        .exportItemsToFile(logger, "/valid/path.viewed.txt", itemsViewed);
  }

  @Test
  public void testExportItems_inputIsNull() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(imExDataMocked.getValidPath(obMocked)).thenReturn(null);

    ExportItems.exportItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getFilesOps();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(imExDataMocked, times(1)).getValidPath(obMocked);
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
    ignore = verify(filesOpsMocked, never())
        .exportItemsToFile(anyObject(), anyString(), anyObject());
  }

  @Test
  public void testExportItems_getAllItemsThrowsException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(imExDataMocked.getValidPath(obMocked)).thenReturn("/valid/path");
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getAllItems(obMocked, "list");

    ExportItems.exportItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getFilesOps();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(imExDataMocked, times(1)).getValidPath(obMocked);
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "list");
    ignore = verify(readMocked, never()).getAllItems(obMocked, "list_viewed");
    ignore = verify(filesOpsMocked, never())
        .exportItemsToFile(anyObject(), anyString(), anyObject());
  }

  @Test
  public void testExportItems_exportItemsToFileFailed() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(imExDataMocked.getValidPath(obMocked)).thenReturn("/valid/path");
    List<String> itemsStored = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewed = Arrays.asList("1st viewed item", "2nd viewed item");
    when(readMocked.getAllItems(obMocked, "list")).thenReturn(itemsStored);
    when(readMocked.getAllItems(obMocked, "list_viewed")).thenReturn(itemsViewed);
    when(filesOpsMocked.exportItemsToFile(logger, "/valid/path", itemsStored))
        .thenReturn(0);
    when(filesOpsMocked.exportItemsToFile(logger, "/valid/path.viewed.txt", itemsViewed))
        .thenReturn(0);

    ExportItems.exportItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getFilesOps();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(imExDataMocked, times(1)).getValidPath(obMocked);
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "list_viewed");
    ignore = verify(filesOpsMocked, times(1))
        .exportItemsToFile(logger, "/valid/path", itemsStored);
    ignore = verify(filesOpsMocked, times(1))
        .exportItemsToFile(logger, "/valid/path.viewed.txt", itemsViewed);
  }

  @Test
  public void testExportItems_itemsViewedEmpty() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(imExDataMocked.getValidPath(obMocked)).thenReturn("/valid/path");
    List<String> itemsStored = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewed = new LinkedList<>();
    when(readMocked.getAllItems(obMocked, "list")).thenReturn(itemsStored);
    when(readMocked.getAllItems(obMocked, "list_viewed")).thenReturn(itemsViewed);
    when(filesOpsMocked.exportItemsToFile(logger, "/valid/path", itemsStored))
        .thenReturn(2);

    ExportItems.exportItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getFilesOps();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(imExDataMocked, times(1)).getValidPath(obMocked);
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "list_viewed");
    ignore = verify(filesOpsMocked, times(1))
        .exportItemsToFile(logger, "/valid/path", itemsStored);
    ignore = verify(filesOpsMocked, never())
        .exportItemsToFile(logger, "/valid/path.viewed.txt", itemsViewed);
  }
}
