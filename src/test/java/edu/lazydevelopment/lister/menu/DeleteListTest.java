package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.logic.ConsoleInput;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code DeleteList} class.
 */
public class DeleteListTest extends GeneralTest {

  @Mock
  private ListerObjects obMocked;
  @Mock
  private Store storeMocked;
  @Mock
  private ConsoleInput conInMocked;

  @BeforeMethod
  public void reset() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testDeleteList_correct() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getConfirmation(obMocked)).thenReturn(false);
    when(storeMocked.dropTableList(obMocked, "list")).thenReturn(true);

    DeleteList.deleteList(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getConfirmation(obMocked);
    ignore = verify(storeMocked, times(1)).dropTableList(obMocked, "list");
  }

  @Test
  public void testDeleteList_declined() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getConfirmation(obMocked)).thenReturn(true);

    DeleteList.deleteList(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getConfirmation(obMocked);
    ignore = verify(storeMocked, never()).dropTableList(anyObject(), anyString());
  }

  @Test
  public void testDeleteList_dropTableListFailed() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getConfirmation(obMocked)).thenReturn(false);
    when(storeMocked.dropTableList(obMocked, "list")).thenReturn(false);

    DeleteList.deleteList(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getConfirmation(obMocked);
    ignore = verify(storeMocked, times(1)).dropTableList(obMocked, "list");
  }
}
