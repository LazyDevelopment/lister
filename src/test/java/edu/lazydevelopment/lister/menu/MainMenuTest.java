package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.logic.ConsoleInput;
import edu.lazydevelopment.lister.logic.ListerLists;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code MainMenu} class.
 */
public class MainMenuTest extends GeneralTest {

  @Mock
  private ListerObjects obMocked;
  @Mock
  private ConsoleInput conInMocked;
  @Mock
  private ListerLists listsMocked;
  @Mock
  private Read readMocked;

  @BeforeMethod
  public void reset() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testShow_correctCreate() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("1")).thenCallRealMethod();
    when(obMocked.getLists()).thenReturn(listsMocked);
    final List<String> existingLists = Arrays.asList("1stList", "2ndList");
    when(listsMocked.showLists(obMocked)).thenReturn(existingLists);
    when(conInMocked.getNameOfNewList(obMocked, existingLists)).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("ExIT".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    MainMenu.show(obMocked);
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(3)).getLogger();
    ignore = verify(obMocked, times(3)).getConIn();
    ignore = verify(conInMocked, times(2)).getByteValue(obMocked);
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(conInMocked, times(1))
        .getNameOfNewList(obMocked, existingLists);
  }

  @Test
  public void testShow_correctSelect() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("2")).thenCallRealMethod();
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    final List<String> existingLists = Arrays.asList("1stList", "2ndList");
    when(listsMocked.showLists(obMocked)).thenReturn(existingLists);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("0"));

    MainMenu.show(obMocked);
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
  }

  @Test
  public void testShow_correctSound() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("3"))
        .thenReturn(Byte.valueOf("0"));
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(true);

    MainMenu.show(obMocked);
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(2)).getByteValue(obMocked);
    ignore = verify(obMocked, times(1)).isSoundEnabled(obMocked);
  }

  @Test
  public void testShow_wrongChoiceAllTimes() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("4"));

    MainMenu.show(obMocked);

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(11)).getConIn();
    ignore = verify(conInMocked, times(11)).getByteValue(obMocked);
  }

  @Test
  public void testShow_getByteValueThrowsException() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    ignore = doAnswer(invocation -> {
      throw new NumberFormatException();
    })
        .when(conInMocked).getByteValue(obMocked);

    MainMenu.show(obMocked);

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getByteValue(obMocked);
  }
}
