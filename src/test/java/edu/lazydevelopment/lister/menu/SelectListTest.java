package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Connect;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.files.FilesOps;
import edu.lazydevelopment.lister.logic.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code SelectList} class.
 */
public class SelectListTest extends GeneralTest {

  @Mock
  private ListerObjects obMocked;
  @Mock
  private ConsoleInput conInMocked;
  @Mock
  private ListerLists listsMocked;
  @Mock
  private Store storeMocked;
  @Mock
  private Read readMocked;
  @Mock
  private Clipboard clipboardMocked;
  @Mock
  private ImportExportData imExDataMocked;
  @Mock
  private Connect cnMocked;
  @Mock
  private FilesOps filesOpsMocked;

  @BeforeMethod
  public void reset() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testSelectList_correctGetStoreItem() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("1"));

    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(listsMocked).readListData(obMocked, "2ndExistingList");

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(3)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(2)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);

    ignore = verify(listsMocked, times(1))
        .readListData(obMocked, "2ndExistingList");
  }

  @Test
  public void testSelectList_correctImportItems() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("2"));

    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getRegex(cnMocked, "2ndExistingList", encryptor);

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(3)).getLogger();
    ignore = verify(obMocked, times(2)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);

    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "2ndExistingList", encryptor);
  }

  @Test
  public void testSelectList_correctExportItems() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("3"));

    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(imExDataMocked.getValidPath(obMocked)).thenReturn(null);

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(3)).getLogger();
    ignore = verify(obMocked, times(2)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);

    ignore = verify(obMocked, times(1)).getFilesOps();
    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(imExDataMocked, times(1)).getValidPath(obMocked);
  }

  @Test
  public void testSelectList_correctUpdateRegEx() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("4"));

    when(listsMocked.storeRegEx(obMocked, "2ndExistingList")).thenReturn(true);

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(2)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);

    ignore = verify(listsMocked, times(1))
        .storeRegEx(obMocked, "2ndExistingList");
  }

  @Test
  public void testSelectList_updateRegExFailed() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("4"));

    when(listsMocked.storeRegEx(obMocked, "2ndExistingList")).thenReturn(false);

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(2)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);

    ignore = verify(listsMocked, times(1))
        .storeRegEx(obMocked, "2ndExistingList");
  }

  @Test
  public void testSelectList_correctRenameList() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("5"));

    when(conInMocked.getConfirmation(obMocked)).thenReturn(true);

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(3)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(3)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);

    ignore = verify(conInMocked, times(1)).getConfirmation(obMocked);
  }

  @Test
  public void testSelectList_correctDeleteList() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("6"));

    when(obMocked.getStore()).thenReturn(storeMocked);
    when(conInMocked.getConfirmation(obMocked)).thenReturn(true);

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(3)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(3)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);

    ignore = verify(conInMocked, times(1)).getConfirmation(obMocked);
  }

  @Test
  public void testSelectList_wrongChoiceAllTimes() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked))
        .thenReturn(Byte.valueOf("-1")).thenReturn(Byte.valueOf("7"));

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(11)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(10)).getByteValue(obMocked);
  }

  @Test
  public void testSelectList_exitWhenChooseWhatToDo() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("0"));

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);
  }

  @Test
  public void testSelectList_getByteValueThrowsExceptionAllTimes() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    ignore = doAnswer(invocation -> {
      throw new NumberFormatException();
    })
        .when(conInMocked).getByteValue(obMocked);

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(11)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(10)).getByteValue(obMocked);
  }

  @Test
  public void testSelectList_showRegExReturnsTrue() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList_viewed"))
        .thenReturn(Arrays.asList("1st viewed item", "2nd viewed item", "3rd viewed item"));
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(true);

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, never()).getByteValue(anyObject());
  }

  @Test
  public void testSelectList_getAllItemsThrowsException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("2"));
    when(readMocked.getAllItems(obMocked, "2ndExistingList"))
        .thenReturn(Arrays.asList("1st item", "2nd item"));
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getAllItems(obMocked, "2ndExistingList_viewed");
    when(listsMocked.showRegEx(obMocked, "2ndExistingList")).thenReturn(false);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("4"));

    when(listsMocked.storeRegEx(obMocked, "2ndExistingList")).thenReturn(true);

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(2)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "2ndExistingList_viewed");
    ignore = verify(listsMocked, times(1))
        .showRegEx(obMocked, "2ndExistingList");
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);

    ignore = verify(listsMocked, times(1))
        .storeRegEx(obMocked, "2ndExistingList");
  }

  @Test
  public void testSelectList_exitWhenChooseList() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("0"));

    SelectList.selectList(obMocked);
    assertThat("MainMenu finish property wasn't set to true", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
    ignore = verify(listsMocked, never()).showRegEx(anyObject(), anyString());
    ignore = verify(conInMocked, never()).getByteValue(anyObject());
  }

  @Test
  public void testSelectList_choiceManyErrors() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked))
        .thenReturn(Arrays.asList("1stExistingList", "2ndExistingList"));
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getUserChoice(obMocked, 2)).thenReturn(Byte.valueOf("-1"));

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getUserChoice(obMocked, 2);
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
    ignore = verify(listsMocked, never()).showRegEx(anyObject(), anyString());
    ignore = verify(conInMocked, never()).getByteValue(anyObject());
  }

  @Test
  public void testSelectList_listsAreEmpty() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.showLists(obMocked)).thenReturn(new LinkedList<>());

    SelectList.selectList(obMocked);

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, never()).getConIn();
    ignore = verify(conInMocked, never()).getUserChoice(anyObject(), anyByte());
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
    ignore = verify(listsMocked, never()).showRegEx(anyObject(), anyString());
    ignore = verify(conInMocked, never()).getByteValue(anyObject());
  }
}
