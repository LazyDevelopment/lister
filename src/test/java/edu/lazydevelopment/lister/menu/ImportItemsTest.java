package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Connect;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.files.FilesOps;
import edu.lazydevelopment.lister.logic.ImportExportData;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ImportItems} class.
 */
public class ImportItemsTest extends GeneralTest {

  @Mock
  private ListerObjects obMocked;
  @Mock
  private ImportExportData imExDataMocked;
  @Mock
  private Read readMocked;
  @Mock
  private Connect cnMocked;
  @Mock
  private FilesOps filesOpsMocked;

  @BeforeMethod
  public void reset() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testImportItems_correct() throws SQLException {
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "list", encryptor)).thenReturn("The regex");
    when(imExDataMocked.getFilePath(obMocked)).thenReturn("/file/path");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(filesOpsMocked.doesFileExist("/file/path.viewed.txt", logger)).thenReturn(true);
    when(imExDataMocked
        .importItemsFromFileToDB(obMocked, "list", "/file/path.viewed.txt", true))
        .thenReturn(true);
    when(imExDataMocked
        .importItemsFromFileToDB(obMocked, "list", "/file/path", false))
        .thenReturn(true);

    ImportItems.importItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "list", encryptor);
    ignore = verify(imExDataMocked, times(1)).getFilePath(obMocked);
    ignore = verify(obMocked, times(1)).getFilesOps();
    ignore = verify(filesOpsMocked, times(1))
        .doesFileExist("/file/path.viewed.txt", logger);
    ignore = verify(imExDataMocked, times(1))
        .importItemsFromFileToDB(obMocked, "list", "/file/path.viewed.txt", true);
    ignore = verify(imExDataMocked, times(1))
        .importItemsFromFileToDB(obMocked, "list", "/file/path", false);
  }

  @Test
  public void testImportItems_getRegexThrowsException() throws SQLException {
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getRegex(cnMocked, "list", encryptor);

    ImportItems.importItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "list", encryptor);
    ignore = verify(imExDataMocked, never()).getFilePath(anyObject());
    ignore = verify(obMocked, never()).getFilesOps();
    ignore = verify(filesOpsMocked, never()).doesFileExist(anyString(), anyObject());
    ignore = verify(imExDataMocked, never())
        .importItemsFromFileToDB(anyObject(), anyString(), anyString(), anyBoolean());
  }

  @Test
  public void testImportItems_finish() throws SQLException {
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "list", encryptor)).thenReturn("The regex");
    when(imExDataMocked.getFilePath(obMocked)).thenReturn("0");

    ImportItems.importItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "list", encryptor);
    ignore = verify(imExDataMocked, times(1)).getFilePath(obMocked);
    ignore = verify(obMocked, never()).getFilesOps();
    ignore = verify(filesOpsMocked, never()).doesFileExist(anyString(), anyObject());
    ignore = verify(imExDataMocked, never())
        .importItemsFromFileToDB(anyObject(), anyString(), anyString(), anyBoolean());
  }

  @Test
  public void testImportItems_viewedFileNotExists() throws SQLException {
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "list", encryptor)).thenReturn("The regex");
    when(imExDataMocked.getFilePath(obMocked)).thenReturn("/file/path");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(filesOpsMocked.doesFileExist("/file/path.viewed.txt", logger)).thenReturn(false);
    when(imExDataMocked
        .importItemsFromFileToDB(obMocked, "list", "/file/path", false))
        .thenReturn(true);

    ImportItems.importItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "list", encryptor);
    ignore = verify(imExDataMocked, times(1)).getFilePath(obMocked);
    ignore = verify(obMocked, times(1)).getFilesOps();
    ignore = verify(filesOpsMocked, times(1))
        .doesFileExist("/file/path.viewed.txt", logger);
    ignore = verify(imExDataMocked, never())
        .importItemsFromFileToDB(obMocked, "list", "/file/path.viewed.txt", true);
    ignore = verify(imExDataMocked, times(1))
        .importItemsFromFileToDB(obMocked, "list", "/file/path", false);
  }

  @Test
  public void testImportItems_importItemsFailed() throws SQLException {
    when(obMocked.getImExData()).thenReturn(imExDataMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "list", encryptor)).thenReturn("The regex");
    when(imExDataMocked.getFilePath(obMocked)).thenReturn("/file/path");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(filesOpsMocked.doesFileExist("/file/path.viewed.txt", logger)).thenReturn(true);
    when(imExDataMocked
        .importItemsFromFileToDB(obMocked, "list", "/file/path.viewed.txt", true))
        .thenReturn(false);
    when(imExDataMocked
        .importItemsFromFileToDB(obMocked, "list", "/file/path", false))
        .thenReturn(false);

    ImportItems.importItems(obMocked, "list");

    ignore = verify(obMocked, times(1)).getImExData();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "list", encryptor);
    ignore = verify(imExDataMocked, times(1)).getFilePath(obMocked);
    ignore = verify(obMocked, times(1)).getFilesOps();
    ignore = verify(filesOpsMocked, times(1))
        .doesFileExist("/file/path.viewed.txt", logger);
    ignore = verify(imExDataMocked, times(1))
        .importItemsFromFileToDB(obMocked, "list", "/file/path.viewed.txt", true);
    ignore = verify(imExDataMocked, times(1))
        .importItemsFromFileToDB(obMocked, "list", "/file/path", false);
  }
}
