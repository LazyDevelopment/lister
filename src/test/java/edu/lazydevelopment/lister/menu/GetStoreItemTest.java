package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.logic.Clipboard;
import edu.lazydevelopment.lister.logic.ListerLists;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jetbrains.annotations.NotNull;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code GetStoreItem} class.
 */
public class GetStoreItemTest extends GeneralTest {

  @Mock
  private ListerObjects obMocked;
  @Mock
  private Clipboard clipboardMocked;
  @Mock
  private ListerLists listsMocked;

  @NotNull
  @DataProvider
  private Object[] boolValues() {
    return Stream.of(true, false).toArray();
  }

  @BeforeMethod
  public void reset() {
    MockitoAnnotations.initMocks(this);
  }

  @Test(dataProvider = "boolValues")
  public void testWorkWithList_correct(boolean value) throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getClipboard()).thenReturn(clipboardMocked);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    final String regex = "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$";
    final List<String> itemsStored = Arrays.asList("1st item", "2nd item");
    final List<String> itemsViewed = Arrays.asList("1st viewed item", "2nd viewed item");
    final Map<String, List<String>> result = new HashMap<>();
    result.put("regex", Collections.singletonList(regex));
    result.put("itemsStored", itemsStored);
    result.put("itemsViewed", itemsViewed);
    when(listsMocked.readListData(obMocked, "list")).thenReturn(result);

    GetStoreItem.workWithList(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getClipboard();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(obMocked, times(1)).isSoundEnabled(obMocked);
    ignore = verify(listsMocked, times(1)).readListData(obMocked, "list");
    verify(clipboardMocked, times(1)).clipboardOps(obMocked, "list", result);
  }

  @Test
  public void testWorkWithList_readListDataThrowsException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getLists()).thenReturn(listsMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(listsMocked).readListData(obMocked, "list");

    GetStoreItem.workWithList(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).readListData(obMocked, "list");
    verify(clipboardMocked, never()).clipboardOps(anyObject(), anyString(), anyObject());
  }
}
