package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.logic.ConsoleInput;
import edu.lazydevelopment.lister.logic.ListerLists;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code CreateNewList} class.
 */
public final class CreateNewListTest extends GeneralTest {

  @Mock
  private Store storeMocked;
  @Mock
  private ConsoleInput conInMocked;
  @Mock
  private ListerObjects obMocked;
  @Mock
  private ListerLists listsMocked;

  @BeforeMethod
  public void reset() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testCreateList_correct() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getLists()).thenReturn(listsMocked);
    final List<String> existingLists = Arrays.asList("1stList", "2ndList");
    when(listsMocked.showLists(obMocked)).thenReturn(existingLists);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getNameOfNewList(obMocked, existingLists)).thenReturn("NewList");
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.createTableList(obMocked, "NewList")).thenReturn(true);
    when(listsMocked.storeRegEx(obMocked, "NewList")).thenReturn(true);

    CreateNewList.createList(obMocked);
    assertThat("MainMenu.finish property was set to true", MainMenu.isFinish(),
        is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1))
        .getNameOfNewList(obMocked, existingLists);
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .createTableList(obMocked, "NewList");
    ignore = verify(listsMocked, times(1))
        .storeRegEx(obMocked, "NewList");
  }

  @Test
  public void testCreateList_listNameIsNull() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getLists()).thenReturn(listsMocked);
    final List<String> existingLists = Arrays.asList("1stList", "2ndList");
    when(listsMocked.showLists(obMocked)).thenReturn(existingLists);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getNameOfNewList(obMocked, existingLists)).thenReturn(null);

    CreateNewList.createList(obMocked);
    assertThat("MainMenu.finish property was set to true", MainMenu.isFinish(),
        is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1))
        .getNameOfNewList(obMocked, existingLists);
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never()).createTableList(anyObject(), anyString());
    ignore = verify(listsMocked, never()).storeRegEx(anyObject(), anyString());
  }

  @Test
  public void testCreateList_createTableListFailed() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getLists()).thenReturn(listsMocked);
    final List<String> existingLists = Arrays.asList("1stList", "2ndList");
    when(listsMocked.showLists(obMocked)).thenReturn(existingLists);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getNameOfNewList(obMocked, existingLists)).thenReturn("NewList");
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.createTableList(obMocked, "NewList")).thenReturn(false);

    CreateNewList.createList(obMocked);
    assertThat("MainMenu.finish property was set to true", MainMenu.isFinish(),
        is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1))
        .getNameOfNewList(obMocked, existingLists);
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .createTableList(obMocked, "NewList");
    ignore = verify(listsMocked, never()).storeRegEx(anyObject(), anyString());
  }

  @Test
  public void testCreateList_storeRegExFailed() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getLists()).thenReturn(listsMocked);
    final List<String> existingLists = Arrays.asList("1stList", "2ndList");
    when(listsMocked.showLists(obMocked)).thenReturn(existingLists);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getNameOfNewList(obMocked, existingLists)).thenReturn("NewList");
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.createTableList(obMocked, "NewList")).thenReturn(true);
    when(listsMocked.storeRegEx(obMocked, "NewList")).thenReturn(false);

    CreateNewList.createList(obMocked);
    assertThat("MainMenu.finish property was set to true", MainMenu.isFinish(),
        is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1))
        .getNameOfNewList(obMocked, existingLists);
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .createTableList(obMocked, "NewList");
    ignore = verify(listsMocked, times(1))
        .storeRegEx(obMocked, "NewList");
  }

  @Test
  public void testCreateList_finish() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getLists()).thenReturn(listsMocked);
    final List<String> existingLists = Arrays.asList("1stList", "2ndList");
    when(listsMocked.showLists(obMocked)).thenReturn(existingLists);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getNameOfNewList(obMocked, existingLists)).thenReturn("NewList");
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.createTableList(obMocked, "NewList")).thenReturn(true);
    when(listsMocked.storeRegEx(obMocked, "NewList")).thenReturn(false);

    MainMenu.setFinish(true);
    CreateNewList.createList(obMocked);
    assertThat("MainMenu.finish property was set to false", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1))
        .getNameOfNewList(obMocked, existingLists);
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .createTableList(obMocked, "NewList");
    ignore = verify(listsMocked, times(1))
        .storeRegEx(obMocked, "NewList");
  }

  @Test
  public void testCreateList_finish2() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getLists()).thenReturn(listsMocked);
    final List<String> existingLists = Arrays.asList("1stList", "2ndList");
    when(listsMocked.showLists(obMocked)).thenReturn(existingLists);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getNameOfNewList(obMocked, existingLists)).thenReturn("NewList");
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.createTableList(obMocked, "NewList")).thenReturn(true);
    when(listsMocked.storeRegEx(obMocked, "NewList")).thenReturn(true);

    MainMenu.setFinish(true);
    CreateNewList.createList(obMocked);
    assertThat("MainMenu.finish property was set to false", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1))
        .getNameOfNewList(obMocked, existingLists);
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .createTableList(obMocked, "NewList");
    ignore = verify(listsMocked, times(1))
        .storeRegEx(obMocked, "NewList");
  }
}
