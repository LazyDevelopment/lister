package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.logic.ConsoleInput;
import edu.lazydevelopment.lister.logic.ListerLists;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code RenameList} class.
 */
public class RenameListTest extends GeneralTest {

  @Mock
  private ListerObjects obMocked;
  @Mock
  private ConsoleInput conInMocked;
  @Mock
  private ListerLists listsMocked;
  @Mock
  private Store storeMocked;

  @BeforeMethod
  public void reset() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testRenameList_correct() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getConfirmation(obMocked)).thenReturn(false);
    when(obMocked.getLists()).thenReturn(listsMocked);
    List<String> lists = Arrays.asList("1stExistingList", "2ndExistingList");
    when(listsMocked.showLists(obMocked)).thenReturn(lists);
    when(conInMocked.getNameOfNewList(obMocked, lists)).thenReturn("NewList");
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.renameTableList(obMocked, "list", "NewList"))
        .thenReturn(true);

    RenameList.renameList(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getConfirmation(obMocked);
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(conInMocked, times(1)).getNameOfNewList(obMocked, lists);
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .renameTableList(obMocked, "list", "NewList");
  }

  @Test
  public void testRenameList_declined() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getConfirmation(obMocked)).thenReturn(true);

    RenameList.renameList(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getConfirmation(obMocked);
    ignore = verify(obMocked, never()).getLists();
    ignore = verify(listsMocked, never()).showLists(obMocked);
    ignore = verify(conInMocked, never()).getNameOfNewList(anyObject(), anyObject());
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never()).renameTableList(anyObject(), anyString(), anyString());
  }

  @Test
  public void testRenameList_newNameNull() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getConfirmation(obMocked)).thenReturn(false);
    when(obMocked.getLists()).thenReturn(listsMocked);
    List<String> lists = Arrays.asList("1stExistingList", "2ndExistingList");
    when(listsMocked.showLists(obMocked)).thenReturn(lists);
    when(conInMocked.getNameOfNewList(obMocked, lists)).thenReturn(null);

    RenameList.renameList(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getConfirmation(obMocked);
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(conInMocked, times(1)).getNameOfNewList(obMocked, lists);
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never()).renameTableList(anyObject(), anyString(), anyString());
  }

  @Test
  public void testRenameList_storeFailed() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getConfirmation(obMocked)).thenReturn(false);
    when(obMocked.getLists()).thenReturn(listsMocked);
    List<String> lists = Arrays.asList("1stExistingList", "2ndExistingList");
    when(listsMocked.showLists(obMocked)).thenReturn(lists);
    when(conInMocked.getNameOfNewList(obMocked, lists)).thenReturn("NewList");
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.renameTableList(obMocked, "list", "NewList"))
        .thenReturn(false);

    RenameList.renameList(obMocked, "list");

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getConfirmation(obMocked);
    ignore = verify(obMocked, times(1)).getLists();
    ignore = verify(listsMocked, times(1)).showLists(obMocked);
    ignore = verify(conInMocked, times(1)).getNameOfNewList(obMocked, lists);
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .renameTableList(obMocked, "list", "NewList");
  }
}
