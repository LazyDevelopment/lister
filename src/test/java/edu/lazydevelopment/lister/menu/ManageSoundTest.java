package edu.lazydevelopment.lister.menu;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.logic.ConsoleInput;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ManageSound} class.
 */
public class ManageSoundTest extends GeneralTest {

  @Mock
  private ListerObjects obMocked;
  @Mock
  private ConsoleInput conInMocked;

  @BeforeMethod
  public void reset() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testExit() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(false);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("0"));

    ManageSound.manageSound(obMocked);
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);
    ignore = verify(obMocked, times(1)).isSoundEnabled(obMocked);
    verify(obMocked, never()).switchSoundState(obMocked);
  }

  @Test
  public void testChangeState() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(false);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("1"));

    ManageSound.manageSound(obMocked);
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);
    ignore = verify(obMocked, times(1)).isSoundEnabled(obMocked);
    verify(obMocked, times(1)).switchSoundState(obMocked);
  }

  @Test
  public void testGetBack() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(true);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("2"));

    ManageSound.manageSound(obMocked);
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);
    ignore = verify(obMocked, times(1)).isSoundEnabled(obMocked);
    verify(obMocked, never()).switchSoundState(obMocked);
  }

  @Test
  public void testWrongChoiceAllTimes() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(false);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("3"));

    ManageSound.manageSound(obMocked);
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getByteValue(obMocked);
    ignore = verify(obMocked, times(1)).isSoundEnabled(obMocked);
    verify(obMocked, never()).switchSoundState(obMocked);
  }

  @Test
  public void testExceptionAllTimes() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(false);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    ignore = doAnswer(invocation -> {
      throw new NumberFormatException();
    })
        .when(conInMocked).getByteValue(obMocked);

    ManageSound.manageSound(obMocked);
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getByteValue(obMocked);
    ignore = verify(obMocked, times(1)).isSoundEnabled(obMocked);
    verify(obMocked, never()).switchSoundState(obMocked);
  }
}
