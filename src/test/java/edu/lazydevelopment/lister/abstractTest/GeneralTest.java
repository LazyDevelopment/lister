package edu.lazydevelopment.lister.abstractTest;

import ch.qos.logback.classic.LoggerContext;
import edu.lazydevelopment.lister.db.Connect;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.jasypt.util.text.AES256TextEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.sql.SQLException;

/**
 * Provides common test configuration for all tests, extending this class.
 */
public abstract class GeneralTest {

  protected static ListerObjects lisOb;
  protected static Connect cn;
  protected static AES256TextEncryptor encryptor;
  protected static Logger logger;
  protected static Object ignore;

  @BeforeSuite(alwaysRun = true)
  public final void suiteSetUp() throws SQLException, ClassNotFoundException {
    lisOb = new ListerObjects();
    encryptor = new AES256TextEncryptor();
    encryptor.setPassword("1324#TestP@ssw0rd!");
    lisOb.setEncryptor(encryptor);
    cn = new Connect("lister.db");
    lisOb.setConnect(cn);
    logger = lisOb.getLogger();
    lisOb.getStore().createTableList(lisOb, "_settings");
    lisOb.getStore().storeItem(lisOb.getConnect(), "Validation string for password check",
        "_settings", encryptor);
  }

  @AfterSuite(alwaysRun = true)
  public final void suiteTearDown() throws SQLException {
    lisOb.getConnect().close();
    try {
      Files.delete(Path.of("lister.db"));
    } catch (NoSuchFileException x) {
      logger.error("no such 'lister.db' file or directory", x);
    } catch (DirectoryNotEmptyException x) {
      logger.error("Directory 'lister.db' not empty", x);
    } catch (IOException x) {
      logger.error("Not enough permissions to delete 'lister.db'", x);
    }
    LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    loggerContext.stop();
  }
}
