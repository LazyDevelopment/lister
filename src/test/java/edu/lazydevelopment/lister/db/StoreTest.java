package edu.lazydevelopment.lister.db;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Tests methods from {@code Store} class.
 */
public final class StoreTest extends GeneralTest {

  private final List<String> items = new ArrayList<>();
  private Store storeTested;

  private List<String> getTablesFromDB() throws SQLException {
    final Connect cn = lisOb.getConnect();
    if (!cn.verifyConnection()) {
      throw new SQLException("DB connection is wrong");
    }

    List<String> list = new LinkedList<>();
    try (ResultSet result = cn.getStatement()
        .executeQuery("SELECT name FROM sqlite_master WHERE type='table';")) {
      while (result.next()) {
        list.add(result.getString("name"));
      }
    }
    return list;
  }

  @BeforeClass
  public void setUp() throws SQLException {
    storeTested = new Store();

    if (!cn.verifyConnection()) {
      cn.reconnect();
    }

    storeTested.renameTableList(lisOb, "_settings", "_settings_original");

    items.add("http://www.1st.item/mocked!*'();:@&=+$,/?#[]");
    items.add("http://wWw.2ND.item/moCKed_.~-%");
  }

  @Test
  public void testCreateTableList() throws Exception {
    assertThat("Failed to create TableList with name 'TestList'",
        storeTested.createTableList(lisOb, "TestList"), is(true));
    assertThat("TableList doesn't exist in DB", getTablesFromDB(),
        hasItems("TestList", "TestList_viewed"));

    assertThat("Failed to create TableList with name '_settings'",
        storeTested.createTableList(lisOb, "_settings"), is(true));
    assertThat("TableList doesn't exist in DB", getTablesFromDB(), hasItem("_settings"));
  }

  @Test(dependsOnMethods = {"testCreateTableList"})
  public void testRenameTableList() throws Exception {
    assertThat("Unable to rename TableList with original name 'TestList'",
        storeTested.renameTableList(lisOb, "TestList", "TestList1"),
        is(true));
    assertThat("TableList with new name doesn't exist in DB", getTablesFromDB(),
        hasItems("TestList1", "TestList1_viewed"));
    assertThat("TableList with old name exists in DB", getTablesFromDB(),
        not(hasItems("TestList", "TestList_viewed")));

    assertThat("Unable to rename TableList with original name '_settings'",
        storeTested.renameTableList(lisOb, "_settings", "_settings1"),
        is(true));
    assertThat("TableList with new name doesn't exist in DB", getTablesFromDB(),
        hasItem("_settings1"));
    assertThat("TableList with old name exists in DB", getTablesFromDB(),
        not(hasItem("_settings")));
  }

  @Test(dependsOnMethods = {"testRenameTableList"})
  public void testStoreItem() throws Exception {
    assertThat("Unable to store RegEx item into TableList 'TestList1'",
        storeTested.storeItem(lisOb.getConnect(),
            "^https\\:\\/\\/www\\.(?:.*)\\.item\\/mocked(?:.*)$", "TestList1",
            lisOb.getEncryptor()), is(true));
    assertThat("RegEx isn't in TableList", lisOb.getRead()
            .getRegex(lisOb.getConnect(), "TestList1", lisOb.getEncryptor()),
        equalTo("^https\\:\\/\\/www\\.(?:.*)\\.item\\/mocked(?:.*)$"));
  }

  @Test(dependsOnMethods = {"testStoreItem"})
  public void testStoreItemsList() throws Exception {
    assertThat("Unable to store list of items in the TableList 'TestList1'",
        storeTested.storeItemsList(lisOb, items, "TestList1"), is(true));
    assertThat("Items aren't in the TableList", lisOb.getRead()
        .getAllItems(lisOb, "TestList1"), hasItems(items.get(0), items.get(1)));
  }

  @Test(dependsOnMethods = {"testStoreItem"})
  public void testUpdateRegex() throws Exception {
    assertThat("Unable to update RegEx for TableList 'TestList1'",
        storeTested.updateRegex("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", lisOb,
            "TestList1"), is(true));
    assertThat("Replacing RegEx by the same one shouldn't rewrite it",
        storeTested.updateRegex("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", lisOb,
            "TestList1"), is(false));
    assertThat("RegEx isn't the same as was provided",
        lisOb.getRead().getRegex(lisOb.getConnect(), "TestList1", lisOb.getEncryptor()),
        equalTo("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$"));
  }

  @Test(dependsOnMethods = {"testStoreItemsList", "testUpdateRegex"})
  public void testDropTableList() throws Exception {
    assertThat("'TestList1' TableList wasn't dropped",
        storeTested.dropTableList(lisOb, "TestList1"), is(true));
    assertThat("'TestList1' TableList exists in DB", getTablesFromDB(),
        not(hasItems("TestList1", "TestList1_viewed")));

    assertThat("'_settings1' TableList wasn't dropped",
        storeTested.dropTableList(lisOb, "_settings1"), is(true));
    assertThat("'_settings1' TableList exists in DB", getTablesFromDB(),
        not(hasItem("_settings1")));
  }

  @AfterClass
  public void tearDown() {
    storeTested.dropTableList(lisOb, "TestList");
    storeTested.dropTableList(lisOb, "TestList1");
    storeTested.dropTableList(lisOb, "TestList_viewed");
    storeTested.dropTableList(lisOb, "TestList1_viewed");
    storeTested.dropTableList(lisOb, "_settings");
    storeTested.dropTableList(lisOb, "_settings1");
    storeTested.renameTableList(lisOb, "_settings_original", "_settings");
  }
}
