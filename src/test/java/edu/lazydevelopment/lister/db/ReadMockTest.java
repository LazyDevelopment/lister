package edu.lazydevelopment.lister.db;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.*;
import java.util.Collections;
import java.util.LinkedList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code Read} class.
 */
public class ReadMockTest extends GeneralTest {

  private Read readTested;

  @Mock
  private Connect cnMocked;
  @Mock
  private ListerObjects obMocked;
  @Mock
  private Statement stMocked;
  @Mock
  private ResultSet rsMocked;
  @Mock
  private Connection connectionMocked;
  @Mock
  private PreparedStatement psMocked;
  @Mock
  private Store storeMocked;
  @Mock
  private Read readMocked;

  @BeforeClass
  public void setUp() throws SQLException {
    readTested = new Read();

    if (!cn.verifyConnection()) {
      cn.reconnect();
    }
  }

  @BeforeMethod
  private void reset() {
    MockitoAnnotations.initMocks(this);
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetTables_verifyConnectionThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();

    readTested.getTables(obMocked);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getStatement();
    ignore = verify(stMocked, never()).executeQuery(anyString());
    ignore = verify(rsMocked, never()).next();
    ignore = verify(rsMocked, never()).getString(anyString());
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetTables_verifyConnectionReturnsFalse() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(cnMocked.verifyConnection()).thenReturn(false);

    readTested.getTables(obMocked);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getStatement();
    ignore = verify(stMocked, never()).executeQuery(anyString());
    ignore = verify(rsMocked, never()).next();
    ignore = verify(rsMocked, never()).getString(anyString());
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test
  public void testGetTables_listIsEmpty() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getStatement()).thenReturn(stMocked);
    when(stMocked.executeQuery(anyString())).thenReturn(rsMocked);
    when(rsMocked.next()).thenReturn(false);
    when(obMocked.getLogger()).thenReturn(logger);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.isClosed()).thenReturn(false);

    assertThat("Not empty list returned", readTested.getTables(obMocked), empty());

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getStatement();
    ignore = verify(stMocked, times(1)).executeQuery(anyString());
    ignore = verify(rsMocked, times(1)).next();
    ignore = verify(rsMocked, never()).getString(anyString());
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).isClosed();
    verify(cnMocked, times(1)).close();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetSomeItemAndRemove_verifyConnectionThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList"))
        .thenReturn(Collections.singletonList("item"));
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();

    readTested.getSomeItemAndRemove(obMocked, "TestList", true);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(obMocked, never()).getRead();
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).executeQuery();
    ignore = verify(rsMocked, never()).next();
    ignore = verify(rsMocked, never()).getString(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetSomeItemAndRemove_verifyConnectionReturnsFalse() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList"))
        .thenReturn(Collections.singletonList("item"));
    when(cnMocked.verifyConnection()).thenReturn(false);

    readTested.getSomeItemAndRemove(obMocked, "TestList", true);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(obMocked, never()).getRead();
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).executeQuery();
    ignore = verify(rsMocked, never()).next();
    ignore = verify(rsMocked, never()).getString(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test
  public void testGetSomeItemAndRemove_listEmpty() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList")).thenReturn(new LinkedList<>());
    when(obMocked.getLogger()).thenReturn(logger);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.isClosed()).thenReturn(false);

    readTested.getSomeItemAndRemove(obMocked, "TestList", true);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, never()).verifyConnection();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
    ignore = verify(cnMocked, never()).getConnection();
    verify(cnMocked, times(1)).close();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(connectionMocked, never()).isClosed();
    ignore = verify(psMocked, never()).executeQuery();
    ignore = verify(rsMocked, never()).getString(anyString());
    ignore = verify(rsMocked, never()).next();
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test
  public void testGetSomeItemAndRemove_itemIsNull() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList"))
        .thenReturn(Collections.singletonList("item"));
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.getString(anyString())).thenReturn(null);
    when(obMocked.getLogger()).thenReturn(logger);

    readTested.getSomeItemAndRemove(obMocked, "TestList", true);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(2)).getString(anyString());
    ignore = verify(rsMocked, never()).next();
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test
  public void testGetSomeItemAndRemove_itemIsEmpty() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList"))
        .thenReturn(Collections.singletonList("item"));
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.getString(anyString())).thenReturn(encryptor.encrypt(""));
    when(obMocked.getLogger()).thenReturn(logger);

    readTested.getSomeItemAndRemove(obMocked, "TestList", true);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(2)).getString(anyString());
    ignore = verify(rsMocked, never()).next();
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test
  public void testGetSomeItemAndRemove_idIsNull() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList"))
        .thenReturn(Collections.singletonList("item"));
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.getString("item")).thenReturn(encryptor.encrypt("item"));
    when(rsMocked.getString("id")).thenReturn(null);
    when(obMocked.getLogger()).thenReturn(logger);

    assertThat("Not empty string returned",
        readTested.getSomeItemAndRemove(obMocked, "TestList", true), isEmptyString());

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(2)).getString(anyString());
    ignore = verify(rsMocked, never()).next();
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test
  public void testGetSomeItemAndRemove_idIsZero() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList"))
        .thenReturn(Collections.singletonList("item"));
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.getString("item")).thenReturn(encryptor.encrypt("item"));
    when(rsMocked.getString("id")).thenReturn("0");
    when(obMocked.getLogger()).thenReturn(logger);

    assertThat("Not empty string returned",
        readTested.getSomeItemAndRemove(obMocked, "TestList", true), isEmptyString());

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(2)).getString(anyString());
    ignore = verify(rsMocked, never()).next();
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "Unable to store 'item' in the 'TestList_viewed' list")
  public void testGetSomeItemAndRemove_storeIsFailed() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList"))
        .thenReturn(Collections.singletonList("item"));
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.getString("item")).thenReturn(encryptor.encrypt("item"));
    when(rsMocked.getString("id")).thenReturn("2");
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeItem(cnMocked, "item", "TestList_viewed", encryptor))
        .thenReturn(false);

    assertThat("Not empty string returned",
        readTested.getSomeItemAndRemove(obMocked, "TestList", true), isEmptyString());

    ignore = verify(obMocked, times(2)).getConnect();
    ignore = verify(obMocked, times(2)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
    ignore = verify(cnMocked, times(2)).getConnection();
    ignore = verify(connectionMocked, times(2)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(2)).getString(anyString());
    ignore = verify(rsMocked, never()).next();
    ignore = verify(psMocked, times(1)).execute();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetSomeItemAndRemove_storeCnFailed() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true).thenReturn(false);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList"))
        .thenReturn(Collections.singletonList("item"));
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.getString("item")).thenReturn(encryptor.encrypt("item"));
    when(rsMocked.getString("id")).thenReturn("2");
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeItem(cnMocked, "item", "TestList_viewed", encryptor))
        .thenReturn(false);

    assertThat("Not empty string returned",
        readTested.getSomeItemAndRemove(obMocked, "TestList", true), isEmptyString());

    ignore = verify(obMocked, times(2)).getConnect();
    ignore = verify(obMocked, times(2)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
    ignore = verify(cnMocked, times(2)).getConnection();
    ignore = verify(connectionMocked, times(2)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(2)).getString(anyString());
    ignore = verify(rsMocked, never()).next();
    ignore = verify(psMocked, times(1)).execute();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetAllItems_verifyConnectionThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();

    readTested.getAllItems(obMocked, "TestList");

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).executeQuery();
    ignore = verify(rsMocked, never()).next();
    ignore = verify(rsMocked, never()).getString(anyString());
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetAllItems_verifyConnectionReturnsFalse() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(false);

    readTested.getAllItems(obMocked, "TestList");

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).executeQuery();
    ignore = verify(rsMocked, never()).next();
    ignore = verify(rsMocked, never()).getString(anyString());
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetRegex_verifyConnectionThrowsException() throws SQLException {
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();

    readTested.getRegex(cnMocked, "TestList", encryptor);

    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).executeQuery();
    ignore = verify(rsMocked, never()).isClosed();
    ignore = verify(rsMocked, never()).getString(anyString());
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetRegex_verifyConnectionReturnsFalse() throws SQLException {
    when(cnMocked.verifyConnection()).thenReturn(false);

    readTested.getRegex(cnMocked, "TestList", encryptor);

    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).executeQuery();
    ignore = verify(rsMocked, never()).isClosed();
    ignore = verify(rsMocked, never()).getString(anyString());
  }

  @Test
  public void testGetRegex_resultIsClosed() throws SQLException {
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.isClosed()).thenReturn(true);

    assertThat("Not empty string returned",
        readTested.getRegex(cnMocked, "TestList", encryptor), isEmptyString());

    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(1)).isClosed();
    ignore = verify(rsMocked, never()).getString(anyString());
  }

  @Test
  public void testGetRegex_encryptorIsNull() throws SQLException {
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.isClosed()).thenReturn(false);
    String encrypted = encryptor.encrypt("item");
    when(rsMocked.getString("item")).thenReturn(encrypted);

    String result = readTested.getRegex(cnMocked, "TestList", null);
    assertThat("Not encrypted string returned", result, is(encrypted));
    assertThat("Wrong encrypted string returned", encryptor.decrypt(result),
        equalTo("item"));

    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(1)).isClosed();
    ignore = verify(rsMocked, times(1)).getString(anyString());
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetPassValidationString_verifyConnectionThrowsException() throws SQLException {
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();

    readTested.getPassValidationString(cnMocked, encryptor);

    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).executeQuery();
    ignore = verify(rsMocked, never()).isClosed();
    ignore = verify(rsMocked, never()).getString(anyString());
  }

  @Test
  public void testGetPassValidationString_happyCase() throws SQLException {
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.isClosed()).thenReturn(false);
    String encrypted = encryptor.encrypt("item");
    when(rsMocked.getString("item")).thenReturn(encrypted);

    String result = readTested.getPassValidationString(cnMocked, encryptor);
    assertThat("Encrypted string returned", result, not(encrypted));
    assertThat("Wrong string returned", result, equalTo("item"));

    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(1)).isClosed();
    ignore = verify(rsMocked, times(1)).getString(anyString());
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetSoundState_sqlException() throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();

    readTested.getSoundState(obMocked);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testGetSoundState_wrongConnection()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(false);

    readTested.getSoundState(obMocked);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
  }

  @Test(expectedExceptions = NoSuchFieldException.class,
      expectedExceptionsMessageRegExp = "Sound option is not set in DB")
  public void testGetSoundState_resultClosed()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.isClosed()).thenReturn(true);

    readTested.getSoundState(obMocked);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(1)).isClosed();
    ignore = verify(rsMocked, never()).getString("item");
  }

  @Test(expectedExceptions = NoSuchMethodException.class,
      expectedExceptionsMessageRegExp = "Sound option has wrong value in DB: test string")
  public void testGetSoundState_wrongValue()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.isClosed()).thenReturn(false);
    when(rsMocked.getString("item")).thenReturn(encryptor.encrypt("test string"));

    readTested.getSoundState(obMocked);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(1)).isClosed();
    ignore = verify(rsMocked, times(1)).getString("item");
  }

  @Test
  public void testGetSoundState_enabled()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.isClosed()).thenReturn(false);
    when(rsMocked.getString("item")).thenReturn(encryptor.encrypt("sound enabled"));

    boolean result = readTested.getSoundState(obMocked);
    assertThat("Sound is disabled", result, is(true));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(1)).isClosed();
    ignore = verify(rsMocked, times(1)).getString("item");
  }

  @Test
  public void testGetSoundState_disabled()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.isClosed()).thenReturn(false);
    when(rsMocked.getString("item")).thenReturn(encryptor.encrypt("sound disabled"));

    boolean result = readTested.getSoundState(obMocked);
    assertThat("Sound is enabled", result, is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(1)).isClosed();
    ignore = verify(rsMocked, times(1)).getString("item");
  }
}
