package edu.lazydevelopment.lister.db;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Tests methods from {@code Connect} class.
 */
public final class ConnectTest {

  private Connect connectTested;

  @BeforeClass
  public void setUp() throws SQLException, ClassNotFoundException {
    connectTested = new Connect("lister.db");
  }

  @Test
  public void testReconnect() throws SQLException {
    assertThat("Reconnect was not successful", connectTested.reconnect(), is(true));
  }

  @Test(dependsOnMethods = {"testClose"})
  public void testReconnect_closed() throws SQLException {
    assertThat("Reconnect was not successful", connectTested.reconnect(), is(true));
  }

  @Test
  public void testVerifyConnection() throws SQLException {
    assertThat("Connection is read-only or closed", connectTested.verifyConnection(),
        is(true));
  }

  @Test(dependsOnMethods = {"testReconnect_closed"})
  public void testVerifyConnection_closed() throws SQLException {
    connectTested.close();
    assertThat("Connection is read-only or closed", connectTested.verifyConnection(),
        is(true));
  }

  @Test
  public void testGetConnection() throws SQLException {
    assertThat("Connection is null", connectTested.getConnection(), notNullValue());
    assertThat("Connection is open", connectTested.getConnection().isClosed(), is(true));
    assertThat("Connection is read-only", connectTested.getConnection().isReadOnly(),
        is(false));
  }

  @Test
  public void testGetConnectionReconnected() throws SQLException {
    connectTested.reconnect();
    assertThat("Connection is null", connectTested.getConnection(), notNullValue());
    assertThat("Connection is closed", connectTested.getConnection().isClosed(), is(false));
    assertThat("Connection is read-only", connectTested.getConnection().isReadOnly(),
        is(false));
    assertThat("Connection is invalid", connectTested.getConnection().isValid(3),
        is(true));
    connectTested.close();
  }

  @Test
  public void testGetStatement() throws SQLException {
    assertThat("Statement is null", connectTested.getStatement(), notNullValue());
    assertThat("Statement is closed", connectTested.getStatement().isClosed(), is(false));
  }

  @Test(dependsOnMethods = {"testReconnect", "testVerifyConnection", "testGetConnection",
      "testGetStatement"})
  public void testClose() throws SQLException {
    connectTested.close();
    assertThat("Connection wasn't closed", connectTested.getConnection().isClosed(),
        is(true));
  }

  @Test(dependsOnMethods = {"testClose"})
  public void testClose_notAutoCommit() throws SQLException {
    connectTested.reconnect();
    connectTested.getConnection().setAutoCommit(false);
    connectTested.close();
    assertThat("Connection wasn't closed", connectTested.getConnection().isClosed(),
        is(true));
  }

  @AfterClass
  public void tearDown() throws SQLException {
    if (!connectTested.getConnection().isClosed()) {
      connectTested.close();
    }
  }
}
