package edu.lazydevelopment.lister.db;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Tests methods from {@code Read} class.
 */
public final class ReadTest extends GeneralTest {

  private Read readTested;

  @BeforeClass
  public void setUp() throws SQLException {
    readTested = new Read();
    Store store = lisOb.getStore();

    if (!cn.verifyConnection()) {
      cn.reconnect();
    }

    store.createTableList(lisOb, "ReadTestTable1");
    store.createTableList(lisOb, "ReadTestTable2");
    store.storeItem(cn, "^http\\:\\/\\/(?:.*)$", "ReadTestTable1", encryptor);
    store.storeItem(cn, "^http\\:\\/\\/(?:.*)$", "ReadTestTable2", encryptor);
    store.storeItemsList(lisOb, Arrays.asList("http://www.1st.item/mocked!*'();:@&=+$,/?#[]",
        "http://wWw.2ND.item/moCKed_.~-%"), "ReadTestTable1");
    store.storeItemsList(lisOb, Arrays.asList("http://www.1st.item/mocked!*'();:@&=+$,/?#[]",
        "http://wWw.2ND.item/moCKed_.~-%"), "ReadTestTable2");
  }

  @Test
  public void testGetTables() throws SQLException {
    List<String> tables = readTested.getTables(lisOb);
    assertThat("List of tables is null or empty", tables,
        allOf(notNullValue(), hasSize(greaterThan(0))));
    assertThat("List of tables doesn't contain expected items", tables,
        hasItems("ReadTestTable1", "ReadTestTable2"));
  }

  @Test
  public void testGetAllItems() throws SQLException {
    List<String> items = readTested.getAllItems(lisOb, "ReadTestTable1");
    assertThat("List of items is null, empty or contains wrong # of items", items,
        allOf(notNullValue(), hasSize(2)));
    assertThat("List doesn't contain expected items", items,
        hasItems("http://www.1st.item/mocked!*'();:@&=+$,/?#[]",
            "http://wWw.2ND.item/moCKed_.~-%"));
    assertThat("List of viewed items contains some data", readTested.getAllItems(lisOb,
        "ReadTestTable1_viewed"), allOf(notNullValue(), hasSize(0)));
  }

  @Test(dependsOnMethods = {"testGetAllItems"})
  public void testGetLastItemAndRemove() throws SQLException {
    String lastItem = readTested.getSomeItemAndRemove(lisOb, "ReadTestTable1", true);
    assertThat("Received item is wrong", lastItem, allOf(not(isEmptyOrNullString()),
        is("http://wWw.2ND.item/moCKed_.~-%")));
    List<String> allItems = readTested.getAllItems(lisOb, "ReadTestTable1");
    List<String> viewedItems = readTested.getAllItems(lisOb, "ReadTestTable1_viewed");
    assertThat("The table contains wrong # of items", allItems, allOf(notNullValue(),
        hasSize(1)));
    assertThat("The table doesn't contain expected item", allItems,
        hasItem("http://www.1st.item/mocked!*'();:@&=+$,/?#[]"));
    assertThat("The table of viewed items contains wrong # of items", viewedItems,
        allOf(notNullValue(), hasSize(1)));
    assertThat("The table of viewed items doesn't contain expected item", viewedItems,
        hasItem("http://wWw.2ND.item/moCKed_.~-%"));
  }

  @Test
  public void testGet1stItemAndRemove() throws SQLException {
    String firstItem = readTested.getSomeItemAndRemove(lisOb, "ReadTestTable2", false);
    assertThat("Received item is wrong", firstItem, allOf(not(isEmptyOrNullString()),
        is("http://www.1st.item/mocked!*'();:@&=+$,/?#[]")));
    List<String> allItems = readTested.getAllItems(lisOb, "ReadTestTable2");
    List<String> viewedItems = readTested.getAllItems(lisOb, "ReadTestTable2_viewed");
    assertThat("The table contains wrong # of items", allItems, allOf(notNullValue(),
        hasSize(1)));
    assertThat("The table doesn't contain expected item", allItems,
        hasItem("http://wWw.2ND.item/moCKed_.~-%"));
    assertThat("The table of viewed items contains wrong # of items", viewedItems,
        allOf(notNullValue(), hasSize(1)));
    assertThat("The table of viewed items doesn't contain expected item", viewedItems,
        hasItem("http://www.1st.item/mocked!*'();:@&=+$,/?#[]"));
  }

  @Test
  public void testGetRegex() throws SQLException {
    String regex1 = readTested.getRegex(lisOb.getConnect(), "ReadTestTable1",
        lisOb.getEncryptor());
    String regex2 = readTested.getRegex(lisOb.getConnect(), "ReadTestTable2",
        lisOb.getEncryptor());
    assertThat("Regex from 1st table is null, empty or wrong", regex1,
        allOf(not(isEmptyOrNullString()), equalTo(regex2), is("^http\\:\\/\\/(?:.*)$")));
  }

  @Test
  public void testGetPassValidationString() throws SQLException {
    String result = readTested.getPassValidationString(cn, encryptor);
    assertThat("Wrong string returned", result,
        equalTo("Validation string for password check"));
  }

  @AfterClass
  public void tearDown() throws SQLException {
    lisOb.getStore().dropTableList(lisOb, "ReadTestTable1");
    lisOb.getStore().dropTableList(lisOb, "ReadTestTable2");
  }
}
