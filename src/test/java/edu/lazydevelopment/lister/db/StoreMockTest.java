package edu.lazydevelopment.lister.db;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.logic.ListerObjects;
import edu.lazydevelopment.lister.sounds.SoundsCollection;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code Store} class.
 */
public class StoreMockTest extends GeneralTest {

  private final List<String> items = new ArrayList<>();
  private Store storeTested;

  @Mock
  private Connect cnMocked;
  @Mock
  private ListerObjects obMocked;
  @Mock
  private Connection connectionMocked;
  @Mock
  private PreparedStatement psMocked;
  @Mock
  private Read readMocked;
  @Mock
  private Store storeMocked;
  @Mock
  private ResultSet rsMocked;
  @Mock
  private SoundsCollection soundsMocked;

  @BeforeClass
  public void setUp() throws SQLException {
    storeTested = new Store();

    if (!cn.verifyConnection()) {
      cn.reconnect();
    }

    items.add("http://www.1st.item/mocked!*'();:@&=+$,/?#[]");
    items.add("http://wWw.2ND.item/moCKed_.~-%");
  }

  @BeforeMethod
  private void reset() {
    MockitoAnnotations.initMocks(this);
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testStoreItemsList_verifyConnectionThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();

    storeTested.storeItemsList(obMocked, items, "TestList");

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).getAutoCommit();
    verify(connectionMocked, never()).setAutoCommit(anyBoolean());
    ignore = verify(obMocked, never()).getEncryptor();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    verify(connectionMocked, never()).rollback();
    ignore = verify(obMocked, never()).getLogger();
    verify(connectionMocked, never()).commit();
    ignore = verify(obMocked, never()).getRead();
    ignore = verify(readMocked, never()).getAllItems(obMocked, "TestList");
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testStoreItemsList_verifyConnectionReturnsFalse() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(cnMocked.verifyConnection()).thenReturn(false);

    storeTested.storeItemsList(obMocked, items, "TestList");

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).getAutoCommit();
    verify(connectionMocked, never()).setAutoCommit(anyBoolean());
    ignore = verify(obMocked, never()).getEncryptor();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    verify(connectionMocked, never()).rollback();
    ignore = verify(obMocked, never()).getLogger();
    verify(connectionMocked, never()).commit();
    ignore = verify(obMocked, never()).getRead();
    ignore = verify(readMocked, never()).getAllItems(obMocked, "TestList");
  }

  @Test
  public void testStoreItemsList_prepareStatementThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.getAutoCommit()).thenReturn(false);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(connectionMocked).prepareStatement(anyString());
    when(obMocked.getLogger()).thenReturn(logger);

    assertThat("Method finished successfully, but shouldn't",
        storeTested.storeItemsList(obMocked, items, "TestList"), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(4)).getConnection();
    ignore = verify(connectionMocked, times(1)).getAutoCommit();
    verify(connectionMocked, times(1)).setAutoCommit(true);
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    verify(connectionMocked, times(1)).rollback();
    ignore = verify(obMocked, times(1)).getLogger();
    verify(connectionMocked, never()).commit();
    ignore = verify(obMocked, never()).getRead();
    ignore = verify(readMocked, never()).getAllItems(obMocked, "TestList");
  }

  @Test
  public void testStoreItemsList_executeThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.getAutoCommit()).thenReturn(false);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(psMocked).execute();
    when(obMocked.getLogger()).thenReturn(logger);

    assertThat("Method finished successfully, but shouldn't",
        storeTested.storeItemsList(obMocked, items, "TestList"), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(4)).getConnection();
    ignore = verify(connectionMocked, times(1)).getAutoCommit();
    verify(connectionMocked, times(1)).setAutoCommit(true);
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).execute();
    verify(connectionMocked, times(1)).rollback();
    ignore = verify(obMocked, times(1)).getLogger();
    verify(connectionMocked, never()).commit();
    ignore = verify(obMocked, never()).getRead();
    ignore = verify(readMocked, never()).getAllItems(obMocked, "TestList");
  }

  @Test
  public void testStoreItemsList_setAutoCommitFalseTrueAndItemsNotStored() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.getAutoCommit()).thenReturn(false, true);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getAllItems(obMocked, "TestList")).thenReturn(new LinkedList<>());

    assertThat("Method finished successfully, but shouldn't",
        storeTested.storeItemsList(obMocked, items, "TestList"), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(4)).getConnection();
    ignore = verify(connectionMocked, times(2)).getAutoCommit();
    verify(connectionMocked, never()).setAutoCommit(anyBoolean());
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(connectionMocked, times(2)).prepareStatement(anyString());
    ignore = verify(psMocked, times(2)).execute();
    verify(connectionMocked, never()).rollback();
    ignore = verify(obMocked, never()).getLogger();
    verify(connectionMocked, never()).commit();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
  }

  @Test
  public void testRenameTableList_verifyConnectionThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();
    when(obMocked.getLogger()).thenReturn(logger);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.isClosed()).thenReturn(true);

    assertThat("renameTableList() renamed tables, but shouldn't",
        storeTested.renameTableList(obMocked, "old name", "new name"), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(connectionMocked, never()).isClosed();
    verify(cnMocked, times(1)).close();
  }

  @Test
  public void testRenameTableList_verifyConnectionReturnsFalse() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(cnMocked.verifyConnection()).thenReturn(false);
    when(obMocked.getLogger()).thenReturn(logger);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.isClosed()).thenReturn(false);

    assertThat("renameTableList() renamed tables, but shouldn't",
        storeTested.renameTableList(obMocked, "old name", "new name"), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(connectionMocked, never()).isClosed();
    verify(cnMocked, times(1)).close();
  }

  @Test
  public void testDropTableList_verifyConnectionThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();
    when(obMocked.getLogger()).thenReturn(logger);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.isClosed()).thenReturn(false);

    assertThat("dropTableList() dropped table, but shouldn't",
        storeTested.dropTableList(obMocked, "TestList"), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(connectionMocked, never()).isClosed();
    verify(cnMocked, times(1)).close();
  }

  @Test
  public void testDropTableList_verifyConnectionReturnsFalse() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(cnMocked.verifyConnection()).thenReturn(false);
    when(obMocked.getLogger()).thenReturn(logger);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.isClosed()).thenReturn(true);

    assertThat("dropTableList() dropped table, but shouldn't",
        storeTested.dropTableList(obMocked, "TestList"), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(connectionMocked, never()).isClosed();
    verify(cnMocked, times(1)).close();
  }

  @Test
  public void testCreateTableList_verifyConnectionThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();
    when(obMocked.getLogger()).thenReturn(logger);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.isClosed()).thenReturn(false);

    assertThat("createTableList() created table, but shouldn't",
        storeTested.createTableList(obMocked, "TestList"), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(connectionMocked, never()).isClosed();
    verify(cnMocked, times(1)).close();
  }

  @Test
  public void testCreateTableList_verifyConnectionReturnsFalse() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(cnMocked.verifyConnection()).thenReturn(false);
    when(obMocked.getLogger()).thenReturn(logger);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.isClosed()).thenReturn(true);

    assertThat("createTableList() created table, but shouldn't",
        storeTested.createTableList(obMocked, "TestList"), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).isClosed();
    verify(cnMocked, times(1)).close();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testUpdateRegex_verifyConnectionThrowsException() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("fake regex");
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.isClosed()).thenReturn(true);

    storeTested.updateRegex("new regex", obMocked, "TestList");

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getLogger();
    ignore = verify(readMocked, never()).getRegex(cnMocked, "TestList", encryptor);
    ignore = verify(connectionMocked, never()).isClosed();
    verify(cnMocked, never()).close();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testUpdateRegex_verifyConnectionReturnsFalse() throws SQLException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("fake regex");
    when(cnMocked.verifyConnection()).thenReturn(false);

    storeTested.updateRegex("new regex", obMocked, "TestList");

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getLogger();
    ignore = verify(readMocked, times(1)).getRegex(cnMocked, "TestList", encryptor);
    ignore = verify(connectionMocked, never()).isClosed();
    verify(connectionMocked, never()).close();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testStoreItem_verifyConnectionThrowsException() throws SQLException {
    ignore = doAnswer(invocation -> {
      throw new SQLException("DB connection is wrong");
    })
        .when(cnMocked).verifyConnection();

    storeTested.storeItem(cnMocked, "item", "TestList", encryptor);

    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(psMocked, never()).executeQuery();
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testStoreItem_verifyConnectionReturnsFalse() throws SQLException {
    when(cnMocked.verifyConnection()).thenReturn(false);

    storeTested.storeItem(cnMocked, "item", "TestList", encryptor);

    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(psMocked, never()).executeQuery();
  }

  @Test
  public void testStoreItem_storedItemNotEqualToRequestedOne() throws SQLException {
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(psMocked.executeQuery()).thenReturn(rsMocked);
    when(rsMocked.getString(anyString())).thenReturn(encryptor.encrypt("item2"));

    assertThat("storeItem() returned that strings are equal, when they are not",
        storeTested.storeItem(cnMocked, "item", "TestList", encryptor), is(false));

    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(2)).getConnection();
    ignore = verify(connectionMocked, times(2)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).execute();
    ignore = verify(psMocked, times(1)).executeQuery();
    ignore = verify(rsMocked, times(1)).getString(anyString());
  }

  @Test
  public void testStoreSoundState_1stTime() throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getLogger()).thenReturn(logger);
    ignore = doAnswer(invocation -> {
      throw new NoSuchFieldException("Sound option is not set in DB");
    })
        .when(readMocked).getSoundState(obMocked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeItem(anyObject(), anyString(), anyString(), anyObject()))
        .thenReturn(true);

    assertThat("New sound state wasn't stored in DB",
        storeTested.storeSoundState(true, obMocked), is(true));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(readMocked, times(1)).getSoundState(obMocked);
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(cnMocked, never()).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getSounds();
    verify(soundsMocked, never()).playSoundOn(obMocked);
  }

  @Test(expectedExceptions = SQLException.class)
  public void testStoreSoundState_wrongData() throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getLogger()).thenReturn(logger);
    ignore = doAnswer(invocation -> {
      throw new NoSuchMethodException("Sound option has wrong value in DB");
    })
        .when(readMocked).getSoundState(obMocked);

    storeTested.storeSoundState(true, obMocked);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(readMocked, times(1)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(cnMocked, never()).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getSounds();
    verify(soundsMocked, never()).playSoundOn(obMocked);
  }

  @Test
  public void testStoreSoundState_nothingToDo() throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getLogger()).thenReturn(logger);
    when(readMocked.getSoundState(obMocked)).thenReturn(true);
    when(obMocked.getSounds()).thenReturn(soundsMocked);

    assertThat("Something wrong in the method's logic",
        storeTested.storeSoundState(true, obMocked), is(true));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(readMocked, times(1)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(cnMocked, never()).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, times(1)).getSounds();
    verify(soundsMocked, times(1)).playSoundOn(obMocked);
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "DB connection is wrong")
  public void testStoreSoundState_wrongConnection()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getLogger()).thenReturn(logger);
    when(readMocked.getSoundState(obMocked)).thenReturn(false);
    when(cnMocked.verifyConnection()).thenReturn(false);

    storeTested.storeSoundState(true, obMocked);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(readMocked, times(1)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, never()).getConnection();
    ignore = verify(connectionMocked, never()).prepareStatement(anyString());
    ignore = verify(psMocked, never()).execute();
    ignore = verify(obMocked, never()).getSounds();
    verify(soundsMocked, never()).playSoundOn(obMocked);
  }

  @Test(expectedExceptions = SQLException.class)
  public void testStoreSoundState_unableToStore()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getLogger()).thenReturn(logger);
    when(readMocked.getSoundState(obMocked)).thenReturn(false)
        .thenThrow(ReflectiveOperationException.class);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(obMocked.getSounds()).thenReturn(soundsMocked);

    storeTested.storeSoundState(true, obMocked);

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(2)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(readMocked, times(2)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).execute();
    ignore = verify(obMocked, times(1)).getSounds();
    verify(soundsMocked, times(1)).playSoundOn(obMocked);
  }

  @Test
  public void testStoreSoundState_NotStored() throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getLogger()).thenReturn(logger);
    when(readMocked.getSoundState(obMocked)).thenReturn(false);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(obMocked.getSounds()).thenReturn(soundsMocked);

    assertThat("New sound state wasn't stored in DB",
        storeTested.storeSoundState(true, obMocked), is(false));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(2)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(readMocked, times(2)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).execute();
    ignore = verify(obMocked, times(1)).getSounds();
    verify(soundsMocked, times(1)).playSoundOn(obMocked);
  }

  @Test
  public void testStoreSoundState_storedEnable() throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getLogger()).thenReturn(logger);
    when(readMocked.getSoundState(obMocked)).thenReturn(false).thenReturn(true);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(obMocked.getSounds()).thenReturn(soundsMocked);

    assertThat("New sound state wasn't stored in DB",
        storeTested.storeSoundState(true, obMocked), is(true));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(2)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(readMocked, times(2)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).execute();
    ignore = verify(obMocked, times(1)).getSounds();
    verify(soundsMocked, times(1)).playSoundOn(obMocked);
  }

  @Test
  public void testStoreSoundState_storedDisable() throws SQLException, ReflectiveOperationException {
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(obMocked.getLogger()).thenReturn(logger);
    when(readMocked.getSoundState(obMocked)).thenReturn(true).thenReturn(false);
    when(cnMocked.verifyConnection()).thenReturn(true);
    when(cnMocked.getConnection()).thenReturn(connectionMocked);
    when(connectionMocked.prepareStatement(anyString())).thenReturn(psMocked);
    when(obMocked.getSounds()).thenReturn(soundsMocked);

    assertThat("New sound state wasn't stored in DB",
        storeTested.storeSoundState(false, obMocked), is(true));

    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(2)).getRead();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(readMocked, times(2)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(cnMocked, times(1)).verifyConnection();
    ignore = verify(cnMocked, times(1)).getConnection();
    ignore = verify(connectionMocked, times(1)).prepareStatement(anyString());
    ignore = verify(psMocked, times(1)).execute();
    ignore = verify(obMocked, times(1)).getSounds();
    verify(soundsMocked, times(1)).playSoundOn(obMocked);
  }
}
