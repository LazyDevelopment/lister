package edu.lazydevelopment.lister.files;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static java.nio.file.Files.delete;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Tests methods from {@code FilesOps} class.
 */
public final class FilesOpsTest extends GeneralTest {

  private static final String WRITABLE_FILE_PATH = "target/writable.txt";
  private static final String EMPTY_FILE_PATH = "target/empty.txt";
  private static final String NOT_EXIST_CREATE_FILE_PATH = "target/not-exist-create.txt";
  private static final String NOT_EXIST_EXPORT_FILE_PATH = "target/not-exist-export.txt";
  private static final String NOT_EXIST_FILE_PATH = "target/not-exist/file.txt";
  private static final String WRONG_FILE_PATH = "\\\\//Wrong:path";
  private FilesOps filesOpsTested;
  private final List<String> items = new ArrayList<>();

  @BeforeClass
  public void setUp() throws IOException {
    filesOpsTested = new FilesOps();

    items.add("http://www.1st.item/mocked!*'();:@&=+$,/?#[]");
    items.add("http://www.2ND.item/mocked_.~-%");

    File writableFile = new File(WRITABLE_FILE_PATH);
    File emptyFile = new File(EMPTY_FILE_PATH);

    if (!emptyFile.createNewFile()) {
      logger.warn("File '{}' already exists or couldn't be created", emptyFile);
    }
    if (!writableFile.createNewFile()) {
      logger.warn("File '{}' already exists or couldn't be created", WRITABLE_FILE_PATH);
    }
    if (!writableFile.setReadable(true)) {
      logger.warn("Unable to set '{}' file readable permissions", WRITABLE_FILE_PATH);
    }
    if (!writableFile.setWritable(true)) {
      logger.warn("Unable to set '{}' file writable permissions", WRITABLE_FILE_PATH);
    }
  }

  @Test
  public void testCanFileBeWrittenOrCreated() {
    assertThat("Not existing file can't be written",
        filesOpsTested.canFileBeWrittenOrCreated(NOT_EXIST_CREATE_FILE_PATH, logger),
        is(true));
    assertThat("Writable file can't be written",
        filesOpsTested.canFileBeWrittenOrCreated(WRITABLE_FILE_PATH, logger), is(true));
    assertThat("File with wrong path can be created",
        filesOpsTested.canFileBeWrittenOrCreated(WRONG_FILE_PATH, logger), is(false));
  }

  @Test
  public void testDoesFileExist() {
    assertThat("Not existing file was found",
        filesOpsTested.doesFileExist(NOT_EXIST_FILE_PATH, logger), is(false));
    assertThat("File with wrong path was found",
        filesOpsTested.doesFileExist(WRONG_FILE_PATH, logger), is(false));
    assertThat("Empty file wasn't found",
        filesOpsTested.doesFileExist(EMPTY_FILE_PATH, logger), is(true));
    assertThat("Writable file wasn't found",
        filesOpsTested.doesFileExist(WRITABLE_FILE_PATH, logger), is(true));
  }

  @Test
  public void testExportItemsToFile() {
    assertThat("Items were exported to file in not existing folder",
        filesOpsTested.exportItemsToFile(logger, NOT_EXIST_FILE_PATH, items), is(0));
    assertThat("Items were exported to file with wrong path",
        filesOpsTested.exportItemsToFile(logger, WRONG_FILE_PATH, items), is(0));
    assertThat("Items were not exported to not existing file",
        filesOpsTested.exportItemsToFile(logger, NOT_EXIST_EXPORT_FILE_PATH, items), is(2));
    assertThat("Items were not exported to writable file",
        filesOpsTested.exportItemsToFile(logger, WRITABLE_FILE_PATH, items), is(2));
  }

  @Test(dependsOnMethods = {"testCanFileBeWrittenOrCreated", "testExportItemsToFile"})
  public void testImportLinesFromFileToList_notExistFile() {
    Map<String, List<String>> result = filesOpsTested
        .importLinesFromFileToList(NOT_EXIST_FILE_PATH, logger,
            "^http\\:\\/\\/www\\.(?:.*)\\.item\\/mocked(?:.*)$", new HashSet<>(),
            new HashSet<>());
    assertThat("No error was returned in case of importing from not existing file",
        result.get("digits").get(0), is("-1"));
  }

  @Test(dependsOnMethods = {"testCanFileBeWrittenOrCreated", "testExportItemsToFile"})
  public void testImportLinesFromFileToList_wrongPath() {
    Map<String, List<String>> result = filesOpsTested
        .importLinesFromFileToList(WRONG_FILE_PATH, logger,
            "^http\\:\\/\\/www\\.(?:.*)\\.item\\/mocked(?:.*)$", new HashSet<>(),
            new HashSet<>());
    assertThat("No error was returned in case of importing from not existing file",
        result.get("digits").get(0), is("-1"));
  }

  @Test(dependsOnMethods = {"testCanFileBeWrittenOrCreated", "testExportItemsToFile"})
  public void testImportLinesFromFileToList_emptyFile() {
    Map<String, List<String>> result = filesOpsTested.importLinesFromFileToList(EMPTY_FILE_PATH,
        logger, "^http\\:\\/\\/www\\.(?:.*)\\.item\\/mocked(?:.*)$", new HashSet<>(),
        new HashSet<>());
    assertThat("Import from empty file: errcount != 0", result.get("digits").get(0),
        is("0"));
    assertThat("Import from empty file: linesProcessed != 0", result.get("digits").get(1),
        is("0"));
    assertThat("Import from empty file: linesStored != 0", result.get("digits").get(2),
        is("0"));
    assertThat("Import from empty file: List of failed lines isn't empty",
        result.get("failed"), empty());
    assertThat("Import from empty file: List of toStore lines isn't empty",
        result.get("toStore"), empty());
  }

  @Test(dependsOnMethods = {"testCanFileBeWrittenOrCreated", "testExportItemsToFile"})
  public void testImportLinesFromFileToList_emptyLine() {
    Map<String, List<String>> result = filesOpsTested
        .importLinesFromFileToList(NOT_EXIST_CREATE_FILE_PATH, logger,
            "^http\\:\\/\\/www\\.(?:.*)\\.item\\/mocked(?:.*)$", new HashSet<>(),
            new HashSet<>());
    assertThat("Import from empty-line file: errcount != 1", result.get("digits").get(0),
        is("1"));
    assertThat("Import from empty-line file: linesProcessed != 1", result.get("digits").get(1),
        is("1"));
    assertThat("Import from empty-line file: linesStored != 0", result.get("digits").get(2),
        is("0"));
    assertThat("Import from empty-line file: Size of list of failed lines != 1",
        result.get("failed"), hasSize(1));
    assertThat("Import from empty-line file: List of failed lines doesn't contain empty line",
        result.get("failed"), hasItem(""));
    assertThat("Import from empty-line file: List of toStore lines isn't empty",
        result.get("toStore"), empty());
  }

  @Test(dependsOnMethods = {"testCanFileBeWrittenOrCreated", "testExportItemsToFile"})
  public void testImportLinesFromFileToList_viewedLines() {
    Map<String, List<String>> result = filesOpsTested.importLinesFromFileToList(WRITABLE_FILE_PATH,
        logger, "^http\\:\\/\\/www\\.(?:.*)\\.item\\/mocked(?:.*)$", new HashSet<>(),
        new HashSet<>(items));
    assertThat("Import viewed lines: errcount != 3", result.get("digits").get(0),
        is("3"));
    assertThat("Import viewed lines: linesProcessed != 3", result.get("digits").get(1),
        is("3"));
    assertThat("Import viewed lines: linesStored != 0", result.get("digits").get(2),
        is("0"));
    assertThat("Import viewed lines: Size of list of failed lines != 3",
        result.get("failed"), hasSize(3));
    assertThat("Import viewed lines: List of failed lines doesn't contain empty line and 2 "
            + "lines from List<String> items",
        result.get("failed"), containsInAnyOrder("", items.get(0), items.get(1)));
    assertThat("Import viewed lines: List of toStore lines isn't empty",
        result.get("toStore"), empty());
  }

  @Test(dependsOnMethods = {"testCanFileBeWrittenOrCreated", "testExportItemsToFile"})
  public void testImportLinesFromFileToList_storedLines() {
    Map<String, List<String>> result = filesOpsTested.importLinesFromFileToList(WRITABLE_FILE_PATH,
        logger, "^http\\:\\/\\/www\\.(?:.*)\\.item\\/mocked(?:.*)$",
        new HashSet<>(items), new HashSet<>());
    assertThat("Import stored lines: errcount != 3", result.get("digits").get(0),
        is("3"));
    assertThat("Import stored lines: linesProcessed != 3", result.get("digits").get(1),
        is("3"));
    assertThat("Import stored lines: linesStored != 0", result.get("digits").get(2),
        is("0"));
    assertThat("Import stored lines: Size of list of failed lines != 3",
        result.get("failed"), hasSize(3));
    assertThat("Import stored lines: List of failed lines doesn't contain empty line and 2 "
            + "lines from List<String> items",
        result.get("failed"), containsInAnyOrder("", items.get(0), items.get(1)));
    assertThat("Import stored lines: List of toStore lines isn't empty",
        result.get("toStore"), empty());
  }

  @Test(dependsOnMethods = {"testCanFileBeWrittenOrCreated", "testExportItemsToFile"})
  public void testImportLinesFromFileToList_correctLines() {
    Map<String, List<String>> result = filesOpsTested.importLinesFromFileToList(WRITABLE_FILE_PATH,
        logger, "^http\\:\\/\\/www\\.(?:.*)\\.item\\/mocked(?:.*)$", new HashSet<>(),
        new HashSet<>());
    assertThat("Import correct lines: errcount != 1", result.get("digits").get(0),
        is("1"));
    assertThat("Import correct lines: linesProcessed != 3", result.get("digits").get(1),
        is("3"));
    assertThat("Import correct lines: linesStored != 2", result.get("digits").get(2),
        is("2"));
    assertThat("Import correct lines: Size of list of failed lines != 1",
        result.get("failed"), hasSize(1));
    assertThat("Import correct lines: List of failed lines doesn't contain empty line",
        result.get("failed"), hasItem(""));
    assertThat("Import correct lines: Size of list of toStore lines != 2",
        result.get("toStore"), hasSize(2));
    assertThat("Import correct lines: List of toStore lines doesn't contain lines from "
            + "List<String> items", result.get("toStore"),
        containsInAnyOrder(items.get(0), items.get(1)));
  }

  @Test(dependsOnMethods = {"testCanFileBeWrittenOrCreated", "testExportItemsToFile"})
  public void testImportLinesFromFileToList_specificRegEx() {
    Map<String, List<String>> result = filesOpsTested.importLinesFromFileToList(WRITABLE_FILE_PATH,
        logger, "^http\\:\\/\\/www\\.1(?:.*)\\.item\\/mocked(?:.*)$", new HashSet<>(),
        new HashSet<>());
    assertThat("Import with specific regex: errcount != 2", result.get("digits").get(0),
        is("2"));
    assertThat("Import with specific regex: linesProcessed != 3", result.get("digits").get(1),
        is("3"));
    assertThat("Import with specific regex: linesStored != 1", result.get("digits").get(2),
        is("1"));
    assertThat("Import with specific regex: Size of list of failed lines != 2",
        result.get("failed"), hasSize(2));
    assertThat("Import with specific regex: List of failed lines doesn't contain empty line",
        result.get("failed"), containsInAnyOrder("", items.get(1)));
    assertThat("Import with specific regex: Size of list of toStore lines != 1",
        result.get("toStore"), hasSize(1));
    assertThat("Import with specific regex: List of toStore lines doesn't contain 1st line "
        + "from List<String> items", result.get("toStore"), hasItem(items.get(0)));
  }

  @AfterClass
  public void tearDown() {
    try {
      File notExistCreate = new File(NOT_EXIST_CREATE_FILE_PATH);
      if (notExistCreate.exists()) {
        delete(Paths.get(NOT_EXIST_CREATE_FILE_PATH));
      }
      File notExistExport = new File(NOT_EXIST_EXPORT_FILE_PATH);
      if (notExistExport.exists()) {
        delete(Paths.get(NOT_EXIST_EXPORT_FILE_PATH));
      }
      File notExistDir = new File(NOT_EXIST_FILE_PATH);
      if (notExistDir.exists()) {
        delete(Paths.get(NOT_EXIST_FILE_PATH));
      }
      File writable = new File(WRITABLE_FILE_PATH);
      if (writable.exists()) {
        delete(Paths.get(WRITABLE_FILE_PATH));
      }
      File empty = new File(EMPTY_FILE_PATH);
      if (empty.exists()) {
        delete(Paths.get(EMPTY_FILE_PATH));
      }
    } catch (Exception e) {
      logger.warn("Got next exception while trying to remove files in 'tearDown' method", e);
    }
  }
}
