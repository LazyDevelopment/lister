package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.menu.MainMenu;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ConsoleInput} class.
 */
public class ConInNameOfNewListCharsValidationTest extends GeneralTest {

  private ConsoleInput conInTested;
  private List<String> existing;

  @Mock
  private ListerObjects obMocked;
  @Mock
  private ConsoleInput conInMocked;

  @BeforeClass
  public void setUp() {
    conInTested = new ConsoleInput();

    existing = new LinkedList<>();
    existing.add("ExistingList1");
    existing.add("ExistingList2");
    existing.add("ExistingList3");
  }

  @DataProvider
  private Object[] negativeStrings() {
    return
        Stream.of(
            Arrays.asList("_", "`", "|", "~", "!", "№", "@", "#", "$", "%"),
            Arrays.asList("&", "*", "(", ")", "+", "=", "\b", "\\", "[", "{"),
            Arrays.asList("]", "}", "\\\\", "'", "<", ",", ".", ">", "?", "/"),
            Arrays.asList("\"", ";", ":", "\n", " ", "\r", "^", "-", "@#$% %&**^ fdgh$%&SDFG?",
                "dfSF#$^13345,./:|FGJ35 dgfSDG")).toArray();
  }

  @BeforeMethod
  public void reset() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test(dataProvider = "negativeStrings")
  public void testGetNameOfNewList_matchesRegExAllTimes(final List<String> strings) {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn(strings.get(0)).thenReturn(strings.get(5))
        .thenReturn(strings.get(1)).thenReturn(strings.get(6))
        .thenReturn(strings.get(2)).thenReturn(strings.get(7))
        .thenReturn(strings.get(3)).thenReturn(strings.get(8))
        .thenReturn(strings.get(4)).thenReturn(strings.get(9));

    assertThat("Not null was returned for match RegEx case",
        conInTested.getNameOfNewList(obMocked, existing), nullValue());
    assertThat("MainMenu.finish was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getStringValue(obMocked);
    ignore = verify(obMocked, never()).getLogger();
  }
}
