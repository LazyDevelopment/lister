package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Connect;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.files.FilesOps;
import edu.lazydevelopment.lister.menu.MainMenu;
import edu.lazydevelopment.lister.security.Login;
import edu.lazydevelopment.lister.sounds.SoundsCollection;
import org.jasypt.util.text.AES256TextEncryptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ListerObjects} class.
 */
public final class ListerObjectsTest extends GeneralTest {

  private ListerObjects lisObTested;

  @Mock
  private SoundsCollection soundsMocked;
  @Mock
  private ListerObjects obMocked;
  @Mock
  private Read readMocked;
  @Mock
  private Store storeMocked;

  @BeforeMethod
  public void setUp() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testConstructor() {
    assertThat("Instance of ListerObjects before running constructor is not Null",
        lisObTested, nullValue());
    lisObTested = new ListerObjects();
    assertThat("Instance of ListerObjects after running constructor is still Null",
        lisObTested, both(notNullValue()).and(instanceOf(ListerObjects.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testIsSoundEnabled_notNull() {
    lisObTested.rawSetSoundStateFlag(true);
    assertThat("Getter returned incorrect value", lisObTested.isSoundEnabled(obMocked),
        is(true));
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    lisObTested.rawSetSoundStateFlag(false);
    assertThat("Getter returned incorrect value", lisObTested.isSoundEnabled(obMocked),
        is(false));
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testIsSoundEnabled_read() throws SQLException, ReflectiveOperationException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getSoundState(obMocked)).thenReturn(true);

    lisObTested.rawSetSoundStateFlag(null);
    assertThat("Wrong value returned by mock", lisObTested.isSoundEnabled(obMocked),
        is(true));
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).setSoundState(anyBoolean(), anyObject());
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testIsSoundEnabled_sqlException() throws SQLException, ReflectiveOperationException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException("Unable to get sound state from DB");
    })
        .when(readMocked).getSoundState(obMocked);

    lisObTested.rawSetSoundStateFlag(null);
    assertThat("Wrong value returned by mock", lisObTested.isSoundEnabled(obMocked),
        is(true));
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).setSoundState(anyBoolean(), anyObject());
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testIsSoundEnabled_methodException()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    ignore = doAnswer(invocation -> {
      throw new NoSuchMethodException("Value of sound state variable is wrong");
    })
        .when(readMocked).getSoundState(obMocked);

    lisObTested.rawSetSoundStateFlag(null);
    assertThat("Wrong value returned by mock", lisObTested.isSoundEnabled(obMocked),
        is(true));
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getSoundState(obMocked);
    ignore = verify(obMocked, never()).setSoundState(anyBoolean(), anyObject());
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testIsSoundEnabled_fieldExceptionFail()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    ignore = doAnswer(invocation -> {
      throw new NoSuchFieldException("Value of sound state doesn't exist");
    })
        .when(readMocked).getSoundState(obMocked);
    when(obMocked.setSoundState(true, obMocked)).thenReturn(false);

    lisObTested.rawSetSoundStateFlag(null);
    assertThat("Wrong value returned by mock", lisObTested.isSoundEnabled(obMocked),
        is(true));
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getSoundState(obMocked);
    ignore = verify(obMocked, times(1))
        .setSoundState(anyBoolean(), anyObject());
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testIsSoundEnabled_fieldExceptionPass()
      throws SQLException, ReflectiveOperationException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    ignore = doAnswer(invocation -> {
      throw new NoSuchFieldException("Value of sound state doesn't exist");
    })
        .when(readMocked).getSoundState(obMocked);
    ignore = doAnswer(invocation -> {
      lisObTested.rawSetSoundStateFlag(true);
      return true;
    })
        .when(obMocked).setSoundState(true, obMocked);

    lisObTested.rawSetSoundStateFlag(null);
    assertThat("Wrong value returned by mock", lisObTested.isSoundEnabled(obMocked),
        is(true));
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(readMocked, times(1)).getSoundState(obMocked);
    ignore = verify(obMocked, times(1))
        .setSoundState(anyBoolean(), anyObject());
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testSetSoundState_falseFailed() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getSounds()).thenReturn(null).thenReturn(soundsMocked);
    when(obMocked.setSoundState(false, obMocked)).thenReturn(true);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeSoundState(anyBoolean(), anyObject())).thenReturn(false);

    assertThat("Value was stored", lisObTested.setSoundState(false, obMocked),
        is(false));
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, never()).getSounds();
    ignore = verify(obMocked, never()).setSoundState(anyBoolean(), anyObject());
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .storeSoundState(anyBoolean(), anyObject());
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testSetSoundState_falseSqlException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getSounds()).thenReturn(null).thenReturn(soundsMocked);
    when(obMocked.setSoundState(false, obMocked)).thenReturn(true);
    when(obMocked.getStore()).thenReturn(storeMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException("Unable to store value");
    })
        .when(storeMocked).storeSoundState(anyBoolean(), anyObject());

    assertThat("Value was stored", lisObTested.setSoundState(false, obMocked),
        is(false));
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, never()).getSounds();
    ignore = verify(obMocked, never()).setSoundState(anyBoolean(), anyObject());
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .storeSoundState(anyBoolean(), anyObject());
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testSwitchSoundState_trueSwitched() {
    when(obMocked.setSoundState(anyBoolean(), anyObject())).thenReturn(true);
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(false).thenReturn(true);
    when(obMocked.getLogger()).thenReturn(logger);

    lisObTested.switchSoundState(obMocked);

    ignore = verify(obMocked, times(1))
        .setSoundState(anyBoolean(), anyObject());
    ignore = verify(obMocked, times(2)).isSoundEnabled(obMocked);
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testSwitchSoundState_true() {
    when(obMocked.setSoundState(anyBoolean(), anyObject())).thenReturn(true);
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(false);
    when(obMocked.getLogger()).thenReturn(logger);

    lisObTested.switchSoundState(obMocked);

    ignore = verify(obMocked, times(1))
        .setSoundState(anyBoolean(), anyObject());
    ignore = verify(obMocked, times(2)).isSoundEnabled(obMocked);
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testSwitchSoundState_false() {
    when(obMocked.setSoundState(anyBoolean(), anyObject())).thenReturn(false);
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(false);
    when(obMocked.getLogger()).thenReturn(logger);

    lisObTested.switchSoundState(obMocked);

    ignore = verify(obMocked, times(1))
        .setSoundState(anyBoolean(), anyObject());
    ignore = verify(obMocked, times(1)).isSoundEnabled(obMocked);
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetImExData() {
    assertThat("Instance of ImportExportData is Null", lisObTested.getImExData(),
        both(notNullValue()).and(instanceOf(ImportExportData.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetClipboard() {
    assertThat("Instance of Clipboard is Null", lisObTested.getClipboard(),
        both(notNullValue()).and(instanceOf(Clipboard.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetLists() {
    assertThat("Instance of ListerLists is Null", lisObTested.getLists(),
        both(notNullValue()).and(instanceOf(ListerLists.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetFilesOps() {
    assertThat("Instance of FilesOps is Null", lisObTested.getFilesOps(),
        both(notNullValue()).and(instanceOf(FilesOps.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetLogger() {
    assertThat("Instance of Logger is Null", lisObTested.getLogger(),
        both(notNullValue()).and(instanceOf(Logger.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetStdIn() {
    assertThat("Instance of InputStream is Null", lisObTested.getStdIn(),
        both(notNullValue()).and(instanceOf(InputStream.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetRead() {
    assertThat("Instance of Read is Null", lisObTested.getRead(),
        both(notNullValue()).and(instanceOf(Read.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetStore() {
    assertThat("Instance of Store is Null", lisObTested.getStore(),
        both(notNullValue()).and(instanceOf(Store.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetLogin() {
    assertThat("Instance of Login is Null", lisObTested.getLogin(),
        both(notNullValue()).and(instanceOf(Login.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetConIn() {
    assertThat("Instance of ConsoleInput is Null", lisObTested.getConIn(),
        both(notNullValue()).and(instanceOf(ConsoleInput.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetSetEncryptor() {
    assertThat("Not Null value was returned by getter before init was called for Encryptor",
        lisObTested.getEncryptor(), nullValue());
    AES256TextEncryptor localEncryptor = new AES256TextEncryptor();
    localEncryptor.setPassword("1234#TestP@ssw0rd!");
    lisObTested.setEncryptor(localEncryptor);
    assertThat("Instance of AES256TextEncryptor is Null after valid object was provided "
            + "to setter", lisObTested.getEncryptor(),
        both(notNullValue()).and(instanceOf(AES256TextEncryptor.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetSetConnect() throws SQLException, ClassNotFoundException {
    assertThat("Not Null value was returned by getter before init was called for Connect",
        lisObTested.getConnect(), nullValue());
    Connect connect = new Connect("lister.db");
    lisObTested.setConnect(connect);
    assertThat("Instance of Connect is Null after valid object was provided to setter",
        lisObTested.getConnect(), both(notNullValue()).and(instanceOf(Connect.class)));
  }

  /**
   * This test is disabled because {@code SoundsCollection} isn't supported inside containers.
   */
  @Test(dependsOnMethods = {"testConstructor"})
  public void testGetSetSounds() {
    assertThat("Not Null value was returned by getter before init was called for Sounds",
        lisObTested.getSounds(), nullValue());
    lisObTested.setSounds(soundsMocked);
    assertThat("Instance of SoundsCollection is Null after valid object was provided "
            + "to setter", lisObTested.getSounds(),
        both(notNullValue()).and(instanceOf(SoundsCollection.class)));
  }
}
