package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.menu.MainMenu;
import edu.lazydevelopment.lister.sounds.SoundsCollection;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ListerObjects} class.
 */
public final class ListerObjectsSoundsTest extends GeneralTest {

  private ListerObjects lisObTested;

  @Mock
  private SoundsCollection soundsMocked;
  @Mock
  private ListerObjects obMocked;
  @Mock
  private Store storeMocked;

  @BeforeMethod
  public void setUp() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testConstructor() {
    assertThat("Instance of ListerObjects before running constructor is not Null",
        lisObTested, nullValue());
    lisObTested = new ListerObjects();
    assertThat("Instance of ListerObjects after running constructor is still Null",
        lisObTested, both(notNullValue()).and(instanceOf(ListerObjects.class)));
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testSetSoundState_trueSoundsNullIoException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getSounds()).thenReturn(null).thenReturn(soundsMocked);
    doAnswer(invocation -> {
      throw new IOException("Unable to init sounds");
    })
        .when(obMocked).setSounds(anyObject());
    when(obMocked.setSoundState(false, obMocked)).thenReturn(true);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeSoundState(anyBoolean(), anyObject())).thenReturn(true);

    assertThat("Value wasn't stored", lisObTested.setSoundState(true, obMocked),
        is(true));
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getSounds();
    verify(obMocked, times(1)).setSounds(anyObject());
    ignore = verify(obMocked, times(1))
        .setSoundState(anyBoolean(), anyObject());
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeSoundState(anyBoolean(), anyObject());
  }

  @Test(dependsOnMethods = {"testConstructor"})
  public void testSetSoundState_trueSoundsNullStored() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getSounds()).thenReturn(null).thenReturn(soundsMocked);
    when(obMocked.setSoundState(false, obMocked)).thenReturn(true);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeSoundState(anyBoolean(), anyObject())).thenReturn(true);

    assertThat("Value wasn't stored", lisObTested.setSoundState(true, obMocked),
        is(true));
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, never()).getLogger();
    ignore = verify(obMocked, times(2)).getSounds();
    ignore = verify(obMocked, never()).setSoundState(anyBoolean(), anyObject());
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(storeMocked, times(1))
        .storeSoundState(anyBoolean(), anyObject());
  }
}
