package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Connect;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.files.FilesOps;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ImportExportData} class.
 */
public class ImportExportDataTest extends GeneralTest {

  private ImportExportData imExTested;

  @Mock
  private ListerObjects obMocked;
  @Mock
  private ConsoleInput conInMocked;
  @Mock
  private FilesOps filesOpsMocked;
  @Mock
  private Read readMocked;
  @Mock
  private Connect cnMocked;
  @Mock
  private Store storeMocked;

  @BeforeClass
  public void setUp() {
    imExTested = new ImportExportData();
  }

  @BeforeMethod
  public void reset() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testGetFilePath_correct() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("./target/file");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(filesOpsMocked.doesFileExist("./target/file", logger)).thenReturn(true);

    assertThat("Path, different from './target/file' was returned",
        imExTested.getFilePath(obMocked), is("./target/file"));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, times(1))
        .doesFileExist("./target/file", logger);
  }

  @Test
  public void testGetFilePath_pathIsZero() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("0");

    assertThat("Path, different from '0' was returned",
        imExTested.getFilePath(obMocked), is("0"));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, never()).getFilesOps();
    ignore = verify(filesOpsMocked, never()).doesFileExist("0", logger);
  }

  @Test
  public void testGetFilePath_notExistsAllTimes() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("./target/file");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(filesOpsMocked.doesFileExist("./target/file", logger)).thenReturn(false);

    assertThat("Path, different from '0' was returned",
        imExTested.getFilePath(obMocked), is("0"));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(10)).getStringValue(obMocked);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, times(10))
        .doesFileExist("./target/file", logger);
  }

  @Test
  public void testGetFilePath_correctIs2nd() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("./target/file");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(filesOpsMocked.doesFileExist("./target/file", logger))
        .thenReturn(false, true);

    assertThat("Path, different from './target/file' was returned",
        imExTested.getFilePath(obMocked), is("./target/file"));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(2)).getStringValue(obMocked);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, times(2))
        .doesFileExist("./target/file", logger);
  }

  @Test
  public void testGetValidPath_correct() {
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("./target/file");
    when(filesOpsMocked.canFileBeWrittenOrCreated(anyString(), anyObject())).thenReturn(true);

    assertThat("Path, different from './target/file' was returned",
        imExTested.getValidPath(obMocked), is("./target/file"));

    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(filesOpsMocked, times(1))
        .canFileBeWrittenOrCreated("./target/file", logger);
    ignore = verify(filesOpsMocked, times(1))
        .canFileBeWrittenOrCreated("./target/file.viewed.txt", logger);
  }

  @Test
  public void testGetValidPath_pathIsZero() {
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("0");

    assertThat("Not Null path was returned",
        imExTested.getValidPath(obMocked), nullValue());

    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(filesOpsMocked, never())
        .canFileBeWrittenOrCreated(anyString(), anyObject());
  }

  @Test
  public void testGetValidPath_notValidAllTimes() {
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("./target/file");
    when(filesOpsMocked.canFileBeWrittenOrCreated(anyString(), anyObject())).thenReturn(false);

    assertThat("Not Null path was returned",
        imExTested.getValidPath(obMocked), nullValue());

    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(10)).getStringValue(obMocked);
    ignore = verify(filesOpsMocked, times(10))
        .canFileBeWrittenOrCreated("./target/file", logger);
    ignore = verify(filesOpsMocked, times(10))
        .canFileBeWrittenOrCreated("./target/file.viewed.txt", logger);
  }

  @Test
  public void testGetValidPath_correctIs3rd() {
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("./target/file");
    when(filesOpsMocked.canFileBeWrittenOrCreated("./target/file", logger))
        .thenReturn(true, false, true);
    when(filesOpsMocked.canFileBeWrittenOrCreated("./target/file.viewed.txt", logger))
        .thenReturn(false, true, true);

    assertThat("Path, different from './target/file' was returned",
        imExTested.getValidPath(obMocked), is("./target/file"));

    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(3)).getStringValue(obMocked);
    ignore = verify(filesOpsMocked, times(3))
        .canFileBeWrittenOrCreated("./target/file", logger);
    ignore = verify(filesOpsMocked, times(3))
        .canFileBeWrittenOrCreated("./target/file.viewed.txt", logger);
  }

  @Test
  public void testImportItemsFromFileToDB_correct() throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "Test list", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    Map<String, List<String>> fromFileFaked = new HashMap<>();
    List<String> toStoreFaked = Arrays.asList("1st new item", "2nd new item");
    fromFileFaked.put("digits", Arrays.asList("0", "3", "3"));
    fromFileFaked.put("failed", new LinkedList<>());
    fromFileFaked.put("toStore", toStoreFaked);
    when(filesOpsMocked.importLinesFromFileToList("./target/file", logger,
        "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked))).thenReturn(fromFileFaked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeItemsList(obMocked, toStoreFaked, "Test list")).thenReturn(true);

    assertThat("Procedure was interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", false), is(true));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, atLeastOnce()).importLinesFromFileToList("./target/file",
        logger, "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked));
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(storeMocked, atLeastOnce())
        .storeItemsList(obMocked, toStoreFaked, "Test list");
  }

  @Test
  public void testImportItemsFromFileToDB_correctViewed() throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "Test list", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    Map<String, List<String>> fromFileFaked = new HashMap<>();
    List<String> toStoreFaked = Arrays.asList("1st new item", "2nd new item");
    fromFileFaked.put("digits", Arrays.asList("0", "3", "3"));
    fromFileFaked.put("failed", new LinkedList<>());
    fromFileFaked.put("toStore", toStoreFaked);
    when(filesOpsMocked.importLinesFromFileToList("./target/file", logger,
        "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked))).thenReturn(fromFileFaked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeItemsList(obMocked, toStoreFaked, "Test list_viewed")).thenReturn(true);

    assertThat("Procedure was interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", true), is(true));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, atLeastOnce()).importLinesFromFileToList("./target/file",
        logger, "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked));
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(storeMocked, atLeastOnce())
        .storeItemsList(obMocked, toStoreFaked, "Test list_viewed");
  }

  @Test
  public void testImportItemsFromFileToDB_correctMixedWithIncorrect() throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "Test list", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    Map<String, List<String>> fromFileFaked = new HashMap<>();
    List<String> toStoreFaked = Arrays.asList("1st new item", "2nd new item");
    fromFileFaked.put("digits", Arrays.asList("0", "3", "3"));
    fromFileFaked.put("failed", Arrays.asList("1st failed item", "2nd failed item"));
    fromFileFaked.put("toStore", toStoreFaked);
    when(filesOpsMocked.importLinesFromFileToList("./target/file", logger,
        "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked))).thenReturn(fromFileFaked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeItemsList(obMocked, toStoreFaked, "Test list")).thenReturn(true);

    assertThat("Procedure was interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", false), is(true));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, atLeastOnce()).importLinesFromFileToList("./target/file",
        logger, "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked));
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(storeMocked, atLeastOnce())
        .storeItemsList(obMocked, toStoreFaked, "Test list");
  }

  @Test
  public void testImportItemsFromFileToDB_getAllStoredItemsThrowsException() throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getAllItems(obMocked, "Test list");

    assertThat("Procedure wasn't interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", false), is(false));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, never())
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, never()).getConnect();
    ignore = verify(obMocked, never()).getEncryptor();
    ignore = verify(readMocked, never()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, never()).getFilesOps();
    ignore = verify(filesOpsMocked, never()).importLinesFromFileToList(anyString(),
        anyObject(), anyString(), anyObject(), anyObject());
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItemsList(anyObject(), anyObject(), anyString());
  }

  @Test
  public void testImportItemsFromFileToDB_getAllViewedItemsThrowsException() throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getAllItems(obMocked, "Test list_viewed");

    assertThat("Procedure wasn't interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", false), is(false));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, never()).getConnect();
    ignore = verify(obMocked, never()).getEncryptor();
    ignore = verify(readMocked, never()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, never()).getFilesOps();
    ignore = verify(filesOpsMocked, never()).importLinesFromFileToList(anyString(),
        anyObject(), anyString(), anyObject(), anyObject());
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItemsList(anyObject(), anyObject(), anyString());
  }

  @Test
  public void testImportItemsFromFileToDB_getRegExThrowsException() throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getRegex(cnMocked, "Test list", encryptor);

    assertThat("Procedure wasn't interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", false), is(false));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, never()).getFilesOps();
    ignore = verify(filesOpsMocked, never()).importLinesFromFileToList(anyString(),
        anyObject(), anyString(), anyObject(), anyObject());
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItemsList(anyObject(), anyObject(), anyString());
  }

  @Test
  public void testImportItemsFromFileToDB_fileNotFound() throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "Test list", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    Map<String, List<String>> fromFileFaked = new HashMap<>();
    fromFileFaked.put("digits", Collections.singletonList("-1"));
    when(filesOpsMocked.importLinesFromFileToList("./target/file", logger,
        "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked))).thenReturn(fromFileFaked);

    assertThat("Procedure wasn't interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", true), is(false));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, times(1))
        .importLinesFromFileToList("./target/file", logger,
            "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
            new HashSet<>(itemsViewedFaked));
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItemsList(anyObject(), anyObject(), anyString());
  }

  @Test
  public void testImportItemsFromFileToDB_nothingToImport() throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "Test list", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    Map<String, List<String>> fromFileFaked = new HashMap<>();
    List<String> toStoreFaked = new LinkedList<>();
    fromFileFaked.put("digits", Arrays.asList("0", "3", "3"));
    fromFileFaked.put("failed", Arrays.asList("1st new item", "2nd new item"));
    fromFileFaked.put("toStore", toStoreFaked);
    when(filesOpsMocked.importLinesFromFileToList("./target/file", logger,
        "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked))).thenReturn(fromFileFaked);

    assertThat("Procedure wasn't interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", false), is(false));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, atLeastOnce()).importLinesFromFileToList("./target/file",
        logger, "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked));
    ignore = verify(obMocked, never()).getStore();
    ignore = verify(storeMocked, never())
        .storeItemsList(obMocked, toStoreFaked, "Test list");
  }

  @Test
  public void testImportItemsFromFileToDB_storeProcessedData_storeItemsListThrowsException()
      throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "Test list", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    Map<String, List<String>> fromFileFaked = new HashMap<>();
    List<String> toStoreFaked = Arrays.asList("1st new item", "2nd new item");
    fromFileFaked.put("digits", Arrays.asList("0", "3", "3"));
    fromFileFaked.put("failed", new LinkedList<>());
    fromFileFaked.put("toStore", toStoreFaked);
    when(filesOpsMocked.importLinesFromFileToList("./target/file", logger,
        "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked))).thenReturn(fromFileFaked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(storeMocked).storeItemsList(obMocked, toStoreFaked, "Test list");

    assertThat("Procedure wasn't interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", false), is(false));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, atLeastOnce()).importLinesFromFileToList("./target/file",
        logger, "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked));
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(storeMocked, times(1))
        .storeItemsList(obMocked, toStoreFaked, "Test list");
  }

  @Test
  public void testImportItemsFromFileToDB_storeProcessedData_storeVievedItemsListThrowsException()
      throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "Test list", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    Map<String, List<String>> fromFileFaked = new HashMap<>();
    List<String> toStoreFaked = Arrays.asList("1st new item", "2nd new item");
    fromFileFaked.put("digits", Arrays.asList("0", "3", "3"));
    fromFileFaked.put("failed", new LinkedList<>());
    fromFileFaked.put("toStore", toStoreFaked);
    when(filesOpsMocked.importLinesFromFileToList("./target/file", logger,
        "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked))).thenReturn(fromFileFaked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(storeMocked).storeItemsList(obMocked, toStoreFaked, "Test list_viewed");

    assertThat("Procedure wasn't interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", true), is(false));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, atLeastOnce()).importLinesFromFileToList("./target/file",
        logger, "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked));
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(storeMocked, times(1))
        .storeItemsList(obMocked, toStoreFaked, "Test list_viewed");
  }

  @Test
  public void testImportItemsFromFileToDB_storeProcessedData_storeItemsListFailed()
      throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "Test list", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    Map<String, List<String>> fromFileFaked = new HashMap<>();
    List<String> toStoreFaked = Arrays.asList("1st new item", "2nd new item");
    fromFileFaked.put("digits", Arrays.asList("0", "3", "3"));
    fromFileFaked.put("failed", new LinkedList<>());
    fromFileFaked.put("toStore", toStoreFaked);
    when(filesOpsMocked.importLinesFromFileToList("./target/file", logger,
        "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked))).thenReturn(fromFileFaked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeItemsList(obMocked, toStoreFaked, "Test list")).thenReturn(false);

    assertThat("Procedure wasn't interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", false), is(false));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(2))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, atLeastOnce()).importLinesFromFileToList("./target/file",
        logger, "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked));
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(storeMocked, times(1))
        .storeItemsList(obMocked, toStoreFaked, "Test list");
  }

  @Test
  public void testImportItemsFromFileToDB_storeProcessedData_storeVievedItemsListFailed()
      throws SQLException {
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getLogger()).thenReturn(logger);
    List<String> itemsStoredFaked = Arrays.asList("1st item", "2nd item");
    List<String> itemsViewedFaked = Arrays
        .asList("1st viewed item", "2nd viewed item", "3rd viewed item");
    when(readMocked.getAllItems(obMocked, "Test list"))
        .thenReturn(itemsStoredFaked);
    when(readMocked.getAllItems(obMocked, "Test list_viewed"))
        .thenReturn(itemsViewedFaked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "Test list", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getFilesOps()).thenReturn(filesOpsMocked);
    Map<String, List<String>> fromFileFaked = new HashMap<>();
    List<String> toStoreFaked = Arrays.asList("1st new item", "2nd new item");
    fromFileFaked.put("digits", Arrays.asList("0", "3", "3"));
    fromFileFaked.put("failed", new LinkedList<>());
    fromFileFaked.put("toStore", toStoreFaked);
    when(filesOpsMocked.importLinesFromFileToList("./target/file", logger,
        "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked))).thenReturn(fromFileFaked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(storeMocked.storeItemsList(obMocked, toStoreFaked, "Test list_viewed"))
        .thenReturn(false);

    assertThat("Procedure wasn't interrupted", imExTested.importItemsFromFileToDB(obMocked,
        "Test list", "./target/file", true), is(false));

    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "Test list");
    ignore = verify(readMocked, times(2))
        .getAllItems(obMocked, "Test list_viewed");
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(cnMocked, "Test list", encryptor);
    ignore = verify(obMocked, atLeastOnce()).getFilesOps();
    ignore = verify(filesOpsMocked, atLeastOnce()).importLinesFromFileToList("./target/file",
        logger, "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", new HashSet<>(itemsStoredFaked),
        new HashSet<>(itemsViewedFaked));
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(storeMocked, times(1))
        .storeItemsList(obMocked, toStoreFaked, "Test list_viewed");
  }
}
