package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.menu.MainMenu;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ConsoleInput} class.
 */
public class ConsoleInputTest extends GeneralTest {

  private ConsoleInput conInTested;
  private List<String> existing;

  @Mock
  private ListerObjects obMocked;
  @Mock
  private ConsoleInput conInMocked;

  @BeforeClass
  public void setUp() {
    conInTested = new ConsoleInput();

    existing = new LinkedList<>();
    existing.add("ExistingList1");
    existing.add("ExistingList2");
    existing.add("ExistingList3");
  }

  @BeforeMethod
  public void reset() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test(expectedExceptions = NumberFormatException.class)
  public void testGetByteValue_throwsException() {
    InputStream stdInStub = new ByteArrayInputStream("not A number".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    conInTested.getByteValue(obMocked);
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getStdIn();
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test
  public void testGetByteValue_exit() {
    InputStream stdInStub = new ByteArrayInputStream("ExIT".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);
    when(obMocked.getLogger()).thenReturn(logger);

    assertThat("Wrong # returned for exit case",
        conInTested.getByteValue(obMocked), is(Byte.valueOf("0")));
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getStdIn();
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test
  public void testGetByteValue_exit0() {
    InputStream stdInStub = new ByteArrayInputStream("0".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);
    when(obMocked.getLogger()).thenReturn(logger);

    assertThat("Wrong # returned for exit case",
        conInTested.getByteValue(obMocked), is(Byte.valueOf("0")));
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getStdIn();
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test
  public void testGetByteValue() {
    InputStream stdInStub = new ByteArrayInputStream("12".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Not typed # returned for positive case",
        conInTested.getByteValue(obMocked), is(Byte.valueOf("12")));
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getStdIn();
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test
  public void testGetStringValue_exit() {
    InputStream stdInStub = new ByteArrayInputStream(" ExIT ".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);
    when(obMocked.getLogger()).thenReturn(logger);

    assertThat("Wrong # returned for exit case",
        conInTested.getStringValue(obMocked), is("0"));
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getStdIn();
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test
  public void testGetStringValue_exit0() {
    InputStream stdInStub = new ByteArrayInputStream("0".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);
    when(obMocked.getLogger()).thenReturn(logger);

    assertThat("Wrong # returned for exit case",
        conInTested.getStringValue(obMocked), is("0"));
    assertThat("Finish property was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getStdIn();
    ignore = verify(obMocked, times(1)).getLogger();
  }

  @Test
  public void testGetStringValue() {
    InputStream stdInStub = new ByteArrayInputStream(" Normal string. ".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Not typed # returned for positive case",
        conInTested.getStringValue(obMocked), is("Normal string."));
    assertThat("Finish property was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getStdIn();
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test
  public void testGetUserChoice() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("5"));

    assertThat("Wrong # was returned for positive case",
        conInTested.getUserChoice(obMocked, 5), is(Byte.valueOf("5")));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getByteValue(obMocked);
  }

  @Test
  public void testGetUserChoice_wrongInputAllTimes() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked)).thenReturn(Byte.valueOf("-5"));

    assertThat("Wrong # was returned for error case",
        conInTested.getUserChoice(obMocked, 5), is(Byte.valueOf("-1")));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getByteValue(obMocked);
  }

  @Test
  public void testGetUserChoice_getByteValueThrowsException() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    ignore = doAnswer(invocation -> {
      throw new NumberFormatException();
    })
        .when(conInMocked).getByteValue(obMocked);

    assertThat("Wrong # was returned for error case",
        conInTested.getUserChoice(obMocked, 5), is(Byte.valueOf("-1")));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getByteValue(obMocked);
  }

  @Test
  public void testGetUserChoice_correctIs3rd() {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getByteValue(obMocked))
        .thenReturn(Byte.valueOf("-1"))
        .thenReturn(Byte.valueOf("6"))
        .thenReturn(Byte.valueOf("0"));

    assertThat("Wrong # was returned for positive case",
        conInTested.getUserChoice(obMocked, 5), is(Byte.valueOf("0")));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(3)).getConIn();
    ignore = verify(conInMocked, times(3)).getByteValue(obMocked);
  }

  @Test
  public void testGetConfirmation_confirm() {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("yEs");

    assertThat("true was returned for confirm case", conInTested.getConfirmation(obMocked),
        is(false));

    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
  }

  @Test
  public void testGetConfirmation_decline() {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("nO");

    assertThat("false was returned for decline case", conInTested.getConfirmation(obMocked),
        is(true));

    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
  }

  @Test
  public void testGetConfirmation_exit() {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("0");

    assertThat("false was returned for exit case", conInTested.getConfirmation(obMocked),
        is(true));

    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
  }

  @Test
  public void testGetConfirmation_wrongInputAllTimes() {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("  wrong input ", "");

    assertThat("false was returned for wrong input case",
        conInTested.getConfirmation(obMocked), is(true));

    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getStringValue(obMocked);
  }

  @Test
  public void testGetConfirmation_correctInputIs2nd() {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn("  wrong input ").thenReturn("YES");

    assertThat("true was returned for confirm case", conInTested.getConfirmation(obMocked),
        is(false));

    ignore = verify(obMocked, times(2)).getConIn();
    ignore = verify(conInMocked, times(2)).getStringValue(obMocked);
  }

  @Test
  public void testGetNameOfNewList_correct() {
    String name = "1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm"
        + "ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁйцукенгшщзхъфывапролджэячсмитьбюё";
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn(name);

    assertThat("Wrong string was returned for positive case",
        conInTested.getNameOfNewList(obMocked, existing), is(name));
    assertThat("MainMenu.finish was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test
  public void testGetNameOfNewList_existingNameAllTimes() {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("ExistingList2");
    when(obMocked.getLogger()).thenReturn(logger);

    assertThat("Not null was returned for duplicate case",
        conInTested.getNameOfNewList(obMocked, existing), nullValue());
    assertThat("MainMenu.finish was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getStringValue(obMocked);
    ignore = verify(obMocked, times(10)).getLogger();
  }

  @Test
  public void testGetNameOfNewList_matchesRegExAllTimes() {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn("name with spaces").thenReturn("!@#$%^&")
        .thenReturn("(&&**%$^)").thenReturn("*()_+-=")
        .thenReturn("~`[]{}").thenReturn(":\"|;'\\")
        .thenReturn(",./<>?").thenReturn("/*-+$%&^")
        .thenReturn("№$#%*(").thenReturn("~hg&^*gdfg^(&bxc@#$$");

    assertThat("Not null was returned for match RegEx case",
        conInTested.getNameOfNewList(obMocked, existing), nullValue());
    assertThat("MainMenu.finish was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getStringValue(obMocked);
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test
  public void testGetNameOfNewList_emptyAllTimes() {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("");

    assertThat("Not null was returned for empty input case",
        conInTested.getNameOfNewList(obMocked, existing), nullValue());
    assertThat("MainMenu.finish was set to true", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, times(10)).getConIn();
    ignore = verify(conInMocked, times(10)).getStringValue(obMocked);
    ignore = verify(obMocked, never()).getLogger();
  }

  @Test
  public void testGetNameOfNewList_finish() {
    when(obMocked.getConIn()).thenReturn(conInMocked);
    MainMenu.setFinish(true);
    when(conInMocked.getStringValue(obMocked)).thenReturn("0");

    assertThat("Not null was returned for finish case",
        conInTested.getNameOfNewList(obMocked, existing), nullValue());
    assertThat("MainMenu.finish was set to false", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, times(1)).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, never()).getLogger();
  }
}
