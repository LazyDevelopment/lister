package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Connect;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.menu.MainMenu;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ListerLists} class.
 */
public class ListerListsTest extends GeneralTest {

  private ListerLists listsTested;

  @Mock
  private ListerObjects obMocked;
  @Mock
  private Connect cnMocked;
  @Mock
  private Read readMocked;
  @Mock
  private Store storeMocked;
  @Mock
  private ListerLists listsMocked;
  @Mock
  private ConsoleInput conInMocked;

  @BeforeClass
  public void setUp() {
    listsTested = new ListerLists();
  }

  @BeforeMethod
  public void reset() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testShowLists_ConnectIsNull() {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getConnect()).thenReturn(null);

    assertThat("Not empty list returned", listsTested.showLists(obMocked), empty());
    assertThat("'Finish' wasn't set to 'true'", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, never()).getRead();
  }

  @Test
  public void testShowLists_getTablesThrowsException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getTables(obMocked);

    assertThat("Not empty list returned", listsTested.showLists(obMocked), empty());
    assertThat("'Finish' wasn't set to 'true'", MainMenu.isFinish(), is(true));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getRead();
  }

  @Test
  public void testShowLists_ListTablesEmpty() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getTables(obMocked)).thenReturn(new LinkedList<>());

    assertThat("Not empty list returned", listsTested.showLists(obMocked), empty());
    assertThat("'Finish' was set to 'true'", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(readMocked, atLeastOnce()).getTables(obMocked);
  }

  @Test
  public void testShowLists_ListTablesWithSettingsOnly() throws SQLException {
    List<String> list = new LinkedList<>();
    list.add("_settings");
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getTables(obMocked)).thenReturn(list);

    assertThat("Not empty list returned", listsTested.showLists(obMocked), empty());
    assertThat("'Finish' was set to 'true'", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(readMocked, atLeastOnce()).getTables(obMocked);
  }

  @Test
  public void testShowLists_ListTablesWithoutSettings() throws SQLException {
    List<String> list = new LinkedList<>();
    list.add("TestList1");
    list.add("TestList2");
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getTables(obMocked)).thenReturn(list);

    assertThat("Empty list returned", listsTested.showLists(obMocked), equalTo(list));
    assertThat("'Finish' was set to 'true'", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(readMocked, atLeastOnce()).getTables(obMocked);
  }

  @Test
  public void testShowLists_ListTablesWithSettings() throws SQLException {
    List<String> list = new LinkedList<>();
    list.add("_settings");
    list.add("TestList1");
    list.add("TestList2");
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(readMocked.getTables(obMocked)).thenReturn(list);

    assertThat("Empty list returned", listsTested.showLists(obMocked), equalTo(list));
    assertThat("'Finish' was set to 'true'", MainMenu.isFinish(), is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(readMocked, atLeastOnce()).getTables(obMocked);
  }

  @Test
  public void testShowRegEx_getRegexThrowsException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getRegex(cnMocked, "TestList", encryptor);

    assertThat("'false' returned", listsTested.showRegEx(obMocked, "TestList"),
        is(true));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(obMocked, never()).getLists();
    ignore = verify(listsMocked, never()).storeRegEx(anyObject(), anyString());
  }

  @Test
  public void testShowRegEx_RegexIsNull() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn(null);
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.storeRegEx(anyObject(), anyString())).thenReturn(false);

    assertThat("'false' returned", listsTested.showRegEx(obMocked, "TestList"),
        is(true));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(obMocked, atLeastOnce()).getLists();
    ignore = verify(listsMocked, atLeastOnce()).storeRegEx(anyObject(), anyString());
  }

  @Test
  public void testShowRegEx_RegexIsEmpty() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("");
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.storeRegEx(anyObject(), anyString())).thenReturn(true);

    assertThat("'true' returned", listsTested.showRegEx(obMocked, "TestList"),
        is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(obMocked, atLeastOnce()).getLists();
    ignore = verify(listsMocked, atLeastOnce()).storeRegEx(anyObject(), anyString());
  }

  @Test
  public void testShowRegEx_RegexIsEmptyAndFinishIsSet() throws SQLException {
    MainMenu.setFinish(true);
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("");
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.storeRegEx(anyObject(), anyString())).thenReturn(false);

    assertThat("'false' returned", listsTested.showRegEx(obMocked, "TestList"),
        is(true));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(obMocked, atLeastOnce()).getLists();
    ignore = verify(listsMocked, atLeastOnce()).storeRegEx(anyObject(), anyString());
  }

  @Test
  public void testShowRegEx_RegexIsEmptyAndFinishIsSet2() throws SQLException {
    MainMenu.setFinish(true);
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("");
    when(obMocked.getLists()).thenReturn(listsMocked);
    when(listsMocked.storeRegEx(anyObject(), anyString())).thenReturn(true);

    assertThat("'false' returned", listsTested.showRegEx(obMocked, "TestList"),
        is(true));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(obMocked, atLeastOnce()).getLists();
    ignore = verify(listsMocked, atLeastOnce()).storeRegEx(anyObject(), anyString());
  }

  @Test
  public void testShowRegEx_RegexIsValid() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");

    assertThat("'true' returned", listsTested.showRegEx(obMocked, "TestList"),
        is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(obMocked, never()).getLists();
    ignore = verify(listsMocked, never()).storeRegEx(anyObject(), anyString());
  }

  @Test
  public void testStoreRegEx_wrongInputAllTimes() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn("Wrong string 1", "^Wrong string 2", "Wrong string 3$");

    assertThat("'true' was returned", listsTested.storeRegEx(obMocked, "TestList"),
        is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(10)).getStringValue(obMocked);
    ignore = verify(obMocked, never()).getRead();
    ignore = verify(obMocked, never()).getConnect();
    ignore = verify(obMocked, never()).getEncryptor();
    ignore = verify(readMocked, never()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(storeMocked, never()).updateRegex(anyString(), anyObject(), anyString());
  }

  @Test
  public void testStoreRegEx_InputIsZero() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked)).thenReturn("0");

    assertThat("'true' was returned", listsTested.storeRegEx(obMocked, "TestList"),
        is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, never()).getRead();
    ignore = verify(obMocked, never()).getConnect();
    ignore = verify(obMocked, never()).getEncryptor();
    ignore = verify(readMocked, never()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(storeMocked, never()).updateRegex(anyString(), anyObject(), anyString());
  }

  @Test
  public void testStoreRegEx_correctInputIsThird() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn("^Wrong string 1", "Wrong string 2$",
            "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("");
    when(storeMocked.storeItem(cnMocked, "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$",
        "TestList", encryptor)).thenReturn(true);

    assertThat("'false' was returned", listsTested.storeRegEx(obMocked, "TestList"),
        is(true));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(3)).getStringValue(obMocked);
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, atLeastOnce())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(storeMocked, never()).updateRegex(anyString(), anyObject(), anyString());
  }

  @Test
  public void testStoreRegEx_getRegExThrowsException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getRegex(cnMocked, "TestList", encryptor);

    assertThat("'true' was returned", listsTested.storeRegEx(obMocked, "TestList"),
        is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(storeMocked, never()).updateRegex(anyString(), anyObject(), anyString());
  }

  @Test
  public void testStoreRegEx_storeItemThrowsException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("");
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(storeMocked).storeItem(cnMocked,
            "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$", "TestList", encryptor);

    assertThat("'true' was returned", listsTested.storeRegEx(obMocked, "TestList"),
        is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, atLeastOnce())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(storeMocked, never()).updateRegex(anyString(), anyObject(), anyString());
  }

  @Test
  public void testStoreRegEx_updateRegEx() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("^old regex$");
    when(storeMocked.updateRegex("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$",
        obMocked, "TestList")).thenReturn(true);

    assertThat("'false' was returned", listsTested.storeRegEx(obMocked, "TestList"),
        is(true));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(storeMocked, atLeastOnce()).updateRegex(anyString(), anyObject(), anyString());
  }

  @Test
  public void testStoreRegEx_updateRegExFailed() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("^old regex$");
    when(storeMocked.updateRegex("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$",
        obMocked, "TestList")).thenReturn(false);

    assertThat("'true' was returned", listsTested.storeRegEx(obMocked, "TestList"),
        is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(storeMocked, atLeastOnce()).updateRegex(anyString(), anyObject(), anyString());
  }

  @Test
  public void testStoreRegEx_updateRegExThrowsException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(lisOb.getLogger());
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(obMocked.getConIn()).thenReturn(conInMocked);
    when(conInMocked.getStringValue(obMocked))
        .thenReturn("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$");
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn("^old regex$");
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(storeMocked).updateRegex("^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$",
            obMocked, "TestList");

    assertThat("'true' was returned", listsTested.storeRegEx(obMocked, "TestList"),
        is(false));

    ignore = verify(obMocked, atLeastOnce()).getLogger();
    ignore = verify(obMocked, atLeastOnce()).getStore();
    ignore = verify(obMocked, atLeastOnce()).getConIn();
    ignore = verify(conInMocked, times(1)).getStringValue(obMocked);
    ignore = verify(obMocked, atLeastOnce()).getRead();
    ignore = verify(obMocked, atLeastOnce()).getConnect();
    ignore = verify(obMocked, atLeastOnce()).getEncryptor();
    ignore = verify(readMocked, atLeastOnce()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
    ignore = verify(storeMocked, atLeastOnce()).updateRegex(anyString(), anyObject(), anyString());
  }

  @Test
  public void testReadListData_correct() throws SQLException {
    final String regex = "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$";
    final List<String> itemsStored = Arrays.asList("1st item", "2nd item");
    final List<String> itemsViewed = Arrays.asList("1st viewed item", "2nd viewed item");
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn(regex);
    when(readMocked.getAllItems(obMocked, "TestList")).thenReturn(itemsStored);
    when(readMocked.getAllItems(obMocked, "TestList_viewed")).thenReturn(itemsViewed);

    final Map<String, List<String>> result = listsTested.readListData(obMocked, "TestList");
    assertThat("Method returned null result", result, notNullValue());
    assertThat("Method returned empty result", result.isEmpty(), is(false));
    assertThat("RegEx not the same as provided", result.get("regex").get(0), is(regex));
    assertThat("List of stored items not the same as provided", result.get("itemsStored"),
        is(itemsStored));
    assertThat("List of viewed items not the same as provided", result.get("itemsViewed"),
        is(itemsViewed));

    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "TestList", encryptor);
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
    ignore = verify(readMocked, times(1))
        .getAllItems(obMocked, "TestList_viewed");
  }

  @Test(expectedExceptions = SQLException.class)
  public void testReadListData_getRegexThrowsException() throws SQLException {
    final String regex = "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$";
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getRegex(cnMocked, "TestList", encryptor);

    assertThat("Method returned not null result",
        listsTested.readListData(obMocked, "TestList"), nullValue());

    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "TestList", encryptor);
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
  }

  @Test(expectedExceptions = SQLException.class)
  public void testReadListData_getAllItemsThrowsException() throws SQLException {
    final String regex = "^https\\:\\/\\/www\\.(?:.*)\\.item\\/(?:.*)$";
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(obMocked.getEncryptor()).thenReturn(encryptor);
    when(readMocked.getRegex(cnMocked, "TestList", encryptor)).thenReturn(regex);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getAllItems(obMocked, "TestList");

    assertThat("Method returned not null result",
        listsTested.readListData(obMocked, "TestList"), nullValue());

    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(obMocked, times(1)).getEncryptor();
    ignore = verify(readMocked, times(1))
        .getRegex(cnMocked, "TestList", encryptor);
    ignore = verify(readMocked, times(1)).getAllItems(obMocked, "TestList");
    ignore = verify(readMocked, never()).getAllItems(anyObject(), anyString());
  }
}
