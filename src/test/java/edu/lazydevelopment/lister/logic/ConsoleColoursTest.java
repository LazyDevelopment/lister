package edu.lazydevelopment.lister.logic;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import org.testng.annotations.Test;

import static edu.lazydevelopment.lister.logic.ConsoleColours.ANSI_RESET;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Backgrounds.*;
import static edu.lazydevelopment.lister.logic.ConsoleColours.Foregrounds.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;

/**
 * Tests constants from {@code ConsoleColours} class.
 */
public final class ConsoleColoursTest {

  @Test
  public void testReset() {
    assertThat("ANSI_RESET constant is empty or NULL", ANSI_RESET, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_black() {
    assertThat("ANSI_BLACK constant is empty or NULL", ANSI_BLACK, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_red() {
    assertThat("ANSI_RED constant is empty or NULL", ANSI_RED, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_green() {
    assertThat("ANSI_GREEN constant is empty or NULL", ANSI_GREEN, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_yellow() {
    assertThat("ANSI_YELLOW constant is empty or NULL", ANSI_YELLOW, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_blue() {
    assertThat("ANSI_BLUE constant is empty or NULL", ANSI_BLUE, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_purple() {
    assertThat("ANSI_PURPLE constant is empty or NULL", ANSI_PURPLE, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_cyan() {
    assertThat("ANSI_CYAN constant is empty or NULL", ANSI_CYAN, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_white() {
    assertThat("ANSI_WHITE constant is empty or NULL", ANSI_WHITE, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_bright_black() {
    assertThat("ANSI_BRIGHT_BLACK constant is empty or NULL",
        ANSI_BRIGHT_BLACK, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_bright_red() {
    assertThat("ANSI_BRIGHT_RED constant is empty or NULL",
        ANSI_BRIGHT_RED, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_bright_green() {
    assertThat("ANSI_BRIGHT_GREEN constant is empty or NULL",
        ANSI_BRIGHT_GREEN, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_bright_yellow() {
    assertThat("ANSI_BRIGHT_YELLOW constant is empty or NULL",
        ANSI_BRIGHT_YELLOW, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_bright_blue() {
    assertThat("ANSI_BRIGHT_BLUE constant is empty or NULL",
        ANSI_BRIGHT_BLUE, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_bright_purple() {
    assertThat("ANSI_BRIGHT_PURPLE constant is empty or NULL",
        ANSI_BRIGHT_PURPLE, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_bright_cyan() {
    assertThat("ANSI_BRIGHT_CYAN constant is empty or NULL",
        ANSI_BRIGHT_CYAN, not(isEmptyString()));
  }

  @Test
  public void testForegroundsAnsi_bright_white() {
    assertThat("ANSI_BRIGHT_WHITE constant is empty or NULL",
        ANSI_BRIGHT_WHITE, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bg_black() {
    assertThat("ANSI_BG_BLACK constant is empty or NULL", ANSI_BG_BLACK, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bg_red() {
    assertThat("ANSI_BG_RED constant is empty or NULL", ANSI_BG_RED, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bg_green() {
    assertThat("ANSI_BG_GREEN constant is empty or NULL", ANSI_BG_GREEN, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bg_yellow() {
    assertThat("ANSI_BG_YELLOW constant is empty or NULL", ANSI_BG_YELLOW, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bg_blue() {
    assertThat("ANSI_BG_BLUE constant is empty or NULL", ANSI_BG_BLUE, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bg_purple() {
    assertThat("ANSI_BG_PURPLE constant is empty or NULL", ANSI_BG_PURPLE, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bg_cyan() {
    assertThat("ANSI_BG_CYAN constant is empty or NULL", ANSI_BG_CYAN, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bg_white() {
    assertThat("ANSI_BG_WHITE constant is empty or NULL", ANSI_BG_WHITE, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bright_bg_black() {
    assertThat("ANSI_BRIGHT_BG_BLACK constant is empty or NULL",
        ANSI_BRIGHT_BG_BLACK, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bright_bg_red() {
    assertThat("ANSI_BRIGHT_BG_RED constant is empty or NULL",
        ANSI_BRIGHT_BG_RED, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bright_bg_green() {
    assertThat("ANSI_BRIGHT_BG_GREEN constant is empty or NULL",
        ANSI_BRIGHT_BG_GREEN, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bright_bg_yellow() {
    assertThat("ANSI_BRIGHT_BG_YELLOW constant is empty or NULL",
        ANSI_BRIGHT_BG_YELLOW, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bright_bg_blue() {
    assertThat("ANSI_BRIGHT_BG_BLUE constant is empty or NULL",
        ANSI_BRIGHT_BG_BLUE, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bright_bg_purple() {
    assertThat("ANSI_BRIGHT_BG_PURPLE constant is empty or NULL",
        ANSI_BRIGHT_BG_PURPLE, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bright_bg_cyan() {
    assertThat("ANSI_BRIGHT_BG_CYAN constant is empty or NULL",
        ANSI_BRIGHT_BG_CYAN, not(isEmptyString()));
  }

  @Test
  public void testBackgroundsAnsi_bright_bg_white() {
    assertThat("ANSI_BRIGHT_BG_WHITE constant is empty or NULL",
        ANSI_BRIGHT_BG_WHITE, not(isEmptyString()));
  }
}
