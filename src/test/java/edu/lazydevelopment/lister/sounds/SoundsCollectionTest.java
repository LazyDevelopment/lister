package edu.lazydevelopment.lister.sounds;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.logic.ListerObjects;
import kuusisto.tinysound.TinySound;
import org.jetbrains.annotations.NotNull;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code SoundsCollection} class.
 */
public final class SoundsCollectionTest extends GeneralTest {

  private SoundsCollection soundsCollectionTested;

  @Mock
  private ListerObjects obMocked;

  @NotNull
  @DataProvider
  private Object[] boolValues() {
    return Stream.of(true, false).toArray();
  }

  @BeforeMethod
  public void reset() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testConstructor() throws IOException {
    assertThat("Sounds lib is initialized before itit was called", TinySound.isInitialized(),
        is(false));
    soundsCollectionTested = new SoundsCollection();
    assertThat("SoundsCollection instance is null", soundsCollectionTested, notNullValue());
    assertThat("Sounds lib wasn't init by constructor", TinySound.isInitialized(),
        is(true));
  }

  @Test(dependsOnMethods = {"testConstructor"}, dataProvider = "boolValues")
  public void testPlayHello(boolean value) {
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    soundsCollectionTested.playHello(obMocked);
    ignore = verify(obMocked, atLeastOnce()).isSoundEnabled(obMocked);
  }

  @Test(dependsOnMethods = {"testConstructor"}, dataProvider = "boolValues")
  public void testPlaySoundOn(boolean value) {
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    soundsCollectionTested.playSoundOn(obMocked);
    ignore = verify(obMocked, atLeastOnce()).isSoundEnabled(obMocked);
  }

  @Test(dependsOnMethods = {"testConstructor"}, dataProvider = "boolValues")
  public void testPlayWatched(boolean value) {
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    soundsCollectionTested.playWatched(obMocked);
    ignore = verify(obMocked, atLeastOnce()).isSoundEnabled(obMocked);
  }

  @Test(dependsOnMethods = {"testConstructor"}, dataProvider = "boolValues")
  public void testPlayDuplicate(boolean value) {
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    soundsCollectionTested.playDuplicate(obMocked);
    ignore = verify(obMocked, atLeastOnce()).isSoundEnabled(obMocked);
  }

  @Test(dependsOnMethods = {"testConstructor"}, dataProvider = "boolValues")
  public void testPlayStart(boolean value) {
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    soundsCollectionTested.playStart(obMocked);
    ignore = verify(obMocked, atLeastOnce()).isSoundEnabled(obMocked);
  }

  @Test(dependsOnMethods = {"testConstructor"}, dataProvider = "boolValues")
  public void testPlayFinish(boolean value) {
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    soundsCollectionTested.playFinish(obMocked);
    ignore = verify(obMocked, atLeastOnce()).isSoundEnabled(obMocked);
  }

  @Test(dependsOnMethods = {"testConstructor"}, dataProvider = "boolValues")
  public void testPlayCopied(boolean value) {
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    soundsCollectionTested.playCopied(obMocked);
    ignore = verify(obMocked, atLeastOnce()).isSoundEnabled(obMocked);
  }

  @Test(dependsOnMethods = {"testConstructor"}, dataProvider = "boolValues")
  public void testPlayError(boolean value) {
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    soundsCollectionTested.playError(obMocked);
    ignore = verify(obMocked, atLeastOnce()).isSoundEnabled(obMocked);
  }

  @Test(dependsOnMethods = {"testConstructor"}, dataProvider = "boolValues")
  public void testPlayStored(boolean value) {
    when(obMocked.isSoundEnabled(obMocked)).thenReturn(value);
    soundsCollectionTested.playStored(obMocked);
    ignore = verify(obMocked, atLeastOnce()).isSoundEnabled(obMocked);
  }

  @Test(dependsOnMethods = {"testConstructor", "testPlayHello", "testPlaySoundOn",
      "testPlayWatched", "testPlayDuplicate", "testPlayStart", "testPlayFinish",
      "testPlayCopied", "testPlayError", "testPlayStored"})
  public void testUnload() {
    assertThat("SoundsCollection instance is null", soundsCollectionTested, notNullValue());
    assertThat("Sounds lib wasn't init by constructor", TinySound.isInitialized(),
        is(true));
    soundsCollectionTested.unload();
    assertThat("Sounds lib is still initialized", TinySound.isInitialized(),
        is(false));
  }
}
