package edu.lazydevelopment.lister.security;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.db.Connect;
import edu.lazydevelopment.lister.db.Read;
import edu.lazydevelopment.lister.db.Store;
import edu.lazydevelopment.lister.logic.ListerObjects;
import edu.lazydevelopment.lister.menu.MainMenu;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.AES256TextEncryptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code Login} class.
 */
public class LoginTest extends GeneralTest {

  private Login loginTested;

  @Mock
  private ListerObjects obMocked;
  @Mock
  private Read readMocked;
  @Mock
  private Store storeMocked;
  @Mock
  private Connect cnMocked;

  @BeforeClass
  public void setUp() {
    loginTested = new Login();
  }

  @BeforeMethod
  public void reset() {
    MainMenu.setFinish(false);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testCreateLogin_correctStdIn() throws IOException, SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(readMocked.getTables(obMocked)).thenReturn(Collections.singletonList("_settings"));
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    InputStream stdInStub2 = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub, stdInStub2);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(readMocked.getPassValidationString(cnMocked, null)).thenReturn("regex");

    assertThat("Wrong object returned", loginTested.createLogin(obMocked),
        both(notNullValue()).and(instanceOf(AES256TextEncryptor.class)));
    assertThat("MainMenu finish property was test to true", MainMenu.isFinish(),
        is(false));

    ignore = verify(obMocked, times(4)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(readMocked, times(1)).getTables(obMocked);
    ignore = verify(storeMocked, never()).createTableList(obMocked, "_settings");
    ignore = verify(obMocked, times(2)).getSysConsole();
    ignore = verify(obMocked, times(2)).getStdIn();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(readMocked, times(1))
        .getPassValidationString(cnMocked, null);
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
  }

  @Test
  public void testCreateLogin_correctStdInStore() throws IOException, SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(readMocked.getTables(obMocked)).thenReturn(Collections.singletonList("_settings"));
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    InputStream stdInStub2 = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub, stdInStub2);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(readMocked.getPassValidationString(cnMocked, null)).thenReturn("");
    when(storeMocked.storeItem(anyObject(), anyString(), anyString(), anyObject()))
        .thenReturn(true);

    assertThat("Wrong object returned", loginTested.createLogin(obMocked),
        both(notNullValue()).and(instanceOf(AES256TextEncryptor.class)));
    assertThat("MainMenu finish property was test to true", MainMenu.isFinish(),
        is(false));

    ignore = verify(obMocked, times(4)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(readMocked, times(1)).getTables(obMocked);
    ignore = verify(storeMocked, never()).createTableList(obMocked, "_settings");
    ignore = verify(obMocked, times(2)).getSysConsole();
    ignore = verify(obMocked, times(2)).getStdIn();
    ignore = verify(obMocked, times(2)).getConnect();
    ignore = verify(readMocked, times(1))
        .getPassValidationString(cnMocked, null);
    ignore = verify(storeMocked, times(1))
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "Unable to store pass phrase in settings DB")
  public void testCreateLogin_stdInStoreFailed() throws IOException, SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(readMocked.getTables(obMocked)).thenReturn(Collections.singletonList("_settings"));
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    InputStream stdInStub2 = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub, stdInStub2);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(readMocked.getPassValidationString(cnMocked, null)).thenReturn("");
    when(storeMocked.storeItem(anyObject(), anyString(), anyString(), anyObject()))
        .thenReturn(false);

    assertThat("Not null object returned", loginTested.createLogin(obMocked),
        nullValue());
    assertThat("MainMenu finish property was test to false", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(4)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(readMocked, times(1)).getTables(obMocked);
    ignore = verify(storeMocked, never()).createTableList(obMocked, "_settings");
    ignore = verify(obMocked, times(2)).getSysConsole();
    ignore = verify(obMocked, times(2)).getStdIn();
    ignore = verify(obMocked, times(2)).getConnect();
    ignore = verify(readMocked, times(1))
        .getPassValidationString(cnMocked, null);
    ignore = verify(storeMocked, times(1))
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
  }

  @Test(expectedExceptions = IOException.class,
      expectedExceptionsMessageRegExp = "Too many errors. Goodbye!")
  public void testCreateLogin_typedPassEmptyAllTimes() throws IOException, SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(readMocked.getTables(obMocked)).thenReturn(Collections.singletonList("_settings"));
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Not null object returned", loginTested.createLogin(obMocked),
        nullValue());
    assertThat("MainMenu finish property was test to false", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(4)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(readMocked, times(1)).getTables(obMocked);
    ignore = verify(storeMocked, never()).createTableList(obMocked, "_settings");
    ignore = verify(obMocked, times(2)).getSysConsole();
    ignore = verify(obMocked, times(11)).getStdIn();
    ignore = verify(obMocked, never()).getConnect();
    ignore = verify(readMocked, never()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
  }

  @Test
  public void testCreateLogin_getTablesEmptyList() throws IOException, SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(readMocked.getTables(obMocked)).thenReturn(new LinkedList<>());
    when(storeMocked.createTableList(obMocked, "_settings")).thenReturn(true);
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    InputStream stdInStub2 = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub, stdInStub2);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(readMocked.getPassValidationString(cnMocked, null)).thenReturn("regex");

    assertThat("Wrong object returned", loginTested.createLogin(obMocked),
        both(notNullValue()).and(instanceOf(AES256TextEncryptor.class)));
    assertThat("MainMenu finish property was test to true", MainMenu.isFinish(),
        is(false));

    ignore = verify(obMocked, times(4)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(readMocked, times(1)).getTables(obMocked);
    ignore = verify(storeMocked, times(1))
        .createTableList(obMocked, "_settings");
    ignore = verify(obMocked, times(2)).getSysConsole();
    ignore = verify(obMocked, times(2)).getStdIn();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(readMocked, times(1))
        .getPassValidationString(cnMocked, null);
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
  }

  @Test(expectedExceptions = IOException.class,
      expectedExceptionsMessageRegExp = "Too many errors. Goodbye!")
  public void testCreateLogin_differentPassAndConfirm1stTime() throws IOException, SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(readMocked.getTables(obMocked)).thenReturn(Collections.singletonList("_settings"));
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    InputStream stdInStub2 = new ByteArrayInputStream("1657&HlfruvR>45\n".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub, stdInStub2);

    assertThat("Not null object returned", loginTested.createLogin(obMocked),
        nullValue());
    assertThat("MainMenu finish property was test to false", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(4)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(readMocked, times(1)).getTables(obMocked);
    ignore = verify(storeMocked, never()).createTableList(obMocked, "_settings");
    ignore = verify(obMocked, times(2)).getSysConsole();
    ignore = verify(obMocked, times(11)).getStdIn();
    ignore = verify(obMocked, never()).getConnect();
    ignore = verify(readMocked, never()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
  }

  @Test(expectedExceptions = SQLException.class,
      expectedExceptionsMessageRegExp = "Unable to create settings DB")
  public void testCreateLogin_createTableListFailed() throws IOException, SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getStore()).thenReturn(storeMocked);
    when(readMocked.getTables(obMocked)).thenReturn(new LinkedList<>());
    when(storeMocked.createTableList(obMocked, "_settings")).thenReturn(false);

    assertThat("Not null object returned", loginTested.createLogin(obMocked),
        nullValue());
    assertThat("MainMenu finish property was test to false", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(4)).getLogger();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getStore();
    ignore = verify(readMocked, times(1)).getTables(obMocked);
    ignore = verify(storeMocked, times(1))
        .createTableList(obMocked, "_settings");
    ignore = verify(obMocked, never()).getSysConsole();
    ignore = verify(obMocked, never()).getStdIn();
    ignore = verify(obMocked, never()).getConnect();
    ignore = verify(readMocked, never()).getRegex(anyObject(), anyString(), anyObject());
    ignore = verify(storeMocked, never())
        .storeItem(anyObject(), anyString(), anyString(), anyObject());
  }

  @Test
  public void testAuthorize_correct() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(readMocked.getPassValidationString(anyObject(), anyObject()))
        .thenReturn("Validation string for password check");

    assertThat("Wrong object returned", loginTested.authorize(obMocked),
        both(notNullValue()).and(instanceOf(AES256TextEncryptor.class)));
    assertThat("MainMenu finish property was test to true", MainMenu.isFinish(),
        is(false));

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(1)).getSysConsole();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(readMocked, times(1))
        .getPassValidationString(anyObject(), anyObject());
  }

  @Test
  public void testAuthorize_validatePassFailed() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    when(readMocked.getPassValidationString(anyObject(), anyObject())).thenReturn("wrong");

    assertThat("Wrong object returned", loginTested.authorize(obMocked),
        both(notNullValue()).and(instanceOf(AES256TextEncryptor.class)));
    assertThat("MainMenu finish property was test to false", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(11)).getLogger();
    ignore = verify(obMocked, times(10)).getSysConsole();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(readMocked, times(1))
        .getPassValidationString(anyObject(), anyObject());
  }

  @Test
  public void testAuthorize_getRegexThrowsException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    ignore = doAnswer(invocation -> {
      throw new SQLException();
    })
        .when(readMocked).getPassValidationString(anyObject(), anyObject());

    assertThat("Wrong object returned", loginTested.authorize(obMocked),
        both(notNullValue()).and(instanceOf(AES256TextEncryptor.class)));
    assertThat("MainMenu finish property was test to false", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(2)).getLogger();
    ignore = verify(obMocked, times(1)).getSysConsole();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(readMocked, times(1))
        .getPassValidationString(anyObject(), anyObject());
  }

  @Test
  public void testAuthorize_getRegexThrowsEncryptionException() throws SQLException {
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getSysConsole()).thenReturn(null);
    InputStream stdInStub = new ByteArrayInputStream("1657&HlsruvR>45\n".getBytes());
    when(obMocked.getStdIn()).thenReturn(stdInStub);
    when(obMocked.getRead()).thenReturn(readMocked);
    when(obMocked.getConnect()).thenReturn(cnMocked);
    ignore = doAnswer(invocation -> {
      throw new EncryptionOperationNotPossibleException();
    })
        .when(readMocked).getPassValidationString(anyObject(), anyObject());

    assertThat("Wrong object returned", loginTested.authorize(obMocked),
        both(notNullValue()).and(instanceOf(AES256TextEncryptor.class)));
    assertThat("MainMenu finish property was test to false", MainMenu.isFinish(),
        is(true));

    ignore = verify(obMocked, times(11)).getLogger();
    ignore = verify(obMocked, times(10)).getSysConsole();
    ignore = verify(obMocked, times(1)).getRead();
    ignore = verify(obMocked, times(1)).getConnect();
    ignore = verify(readMocked, times(1))
        .getPassValidationString(anyObject(), anyObject());
  }
}
