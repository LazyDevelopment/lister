package edu.lazydevelopment.lister.security;

import edu.lazydevelopment.lister.abstractTest.GeneralTest;
import edu.lazydevelopment.lister.logic.ListerObjects;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

/**
 * Tests methods from {@code ReadPasswordFromSysIn} class.
 */
public class ReadPasswordFromSysInTest extends GeneralTest {

  private ReadPasswordFromSysIn readPasswordFromSysIn;

  @Mock
  private ListerObjects obMocked;

  @BeforeClass
  public void setUp() {
    readPasswordFromSysIn = new ReadPasswordFromSysIn();
  }

  @BeforeMethod
  public void reset() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testGetPassword_correct() throws IOException {
    InputStream stdInStub = new ByteArrayInputStream(" soM3 P@z5w0rd! \n".getBytes());
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Returned password don't match typed one",
        readPasswordFromSysIn.getPassword(obMocked), is(" soM3 P@z5w0rd! ".toCharArray()));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStdIn();
  }

  @Test
  public void testGetPassword_correctR() throws IOException {
    InputStream stdInStub = new ByteArrayInputStream(" soM3 P@z5w0rd! \r \n".getBytes());
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Returned password don't match typed one",
        readPasswordFromSysIn.getPassword(obMocked), is(" soM3 P@z5w0rd!  ".toCharArray()));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStdIn();
  }

  @Test
  public void testGetPassword_correctR_() throws IOException {
    InputStream stdInStub = new ByteArrayInputStream(" soM3 P@z5w0rd! \r ".getBytes());
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Returned password don't match typed one",
        readPasswordFromSysIn.getPassword(obMocked), is(" soM3 P@z5w0rd!  ".toCharArray()));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStdIn();
  }

  @Test
  public void testGetPassword_correctReon() throws IOException {
    InputStream stdInStub = new ByteArrayInputStream(" soM3 P@z5w0rd! \r".getBytes());
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Returned password don't match typed one",
        readPasswordFromSysIn.getPassword(obMocked), is(" soM3 P@z5w0rd! ".toCharArray()));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStdIn();
  }

  @Test
  public void testGetPassword_correctRN() throws IOException {
    InputStream stdInStub = new ByteArrayInputStream(" soM3 P@z5w0rd! \r\n".getBytes());
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Returned password don't match typed one",
        readPasswordFromSysIn.getPassword(obMocked), is(" soM3 P@z5w0rd! ".toCharArray()));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStdIn();
  }

  @Test
  public void testGetPassword_empty() throws IOException {
    InputStream stdInStub = new ByteArrayInputStream("".getBytes());
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Returned password don't match typed one",
        readPasswordFromSysIn.getPassword(obMocked), is("".toCharArray()));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStdIn();
  }

  @Test
  public void testGetPassword_long() throws IOException {
    final String pass = "X7~#@n%8B_F./F)bx}\\^@d2W*/98D9YNPFRt3EY='hqUV%th{.**7,Z]D]s_?,"
        + "{Ug3Jj\"kQv~x!E!T^]`-Aw&Rc}&Afuj+r.^qb2%;].pZhRd^jWg^WQr7Vta6F~wfpy";
    InputStream stdInStub = new ByteArrayInputStream(pass.getBytes());
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Returned password don't match typed one",
        readPasswordFromSysIn.getPassword(obMocked), is(pass.toCharArray()));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStdIn();
  }

  @Test
  public void testGetPassword_longer() throws IOException {
    final String pass = "X7~#@n%8B_F./F)bx}\\^@d2W*/98D9YNPFRt3EY='hqUV%th{.**7,Z]D]s_?,"
        + "{Ug3Jj\"kQv~x!E!T^]`-Aw&Rc}&Afuj+r.^qb2%;].pZhRd^jWg^WQr7Vta6F~wfpy7";
    InputStream stdInStub = new ByteArrayInputStream(pass.getBytes());
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Returned password don't match typed one",
        readPasswordFromSysIn.getPassword(obMocked), is(pass.toCharArray()));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStdIn();
  }

  @Test
  public void testGetPassword_longest() throws IOException {
    final String pass = "j#c\\Hz(^#8mz,W*@9!5dK^4XH~eTz87t_8cAnkHVb<vmwQ{Nv'atMq%%\"K{@m!t<PRb$"
        + "?>~Xu{Z]]`\"NK!48}x[^w`;8H%n\"NDQwSu<8=rBHZEd}Evp5RRsaDbc/ybcYspmH&\\*,"
        + "a3+f:vY@+V'7jZDSM>s{n!r:G9AhMhCcZQ^^N`e549p$^^Nr7;"
        + "U>ygRP9'T-5+$\"K*[-?=@t3~/]w_\"mV~^kb-qT!XLBngtc9hF3-qC/9&HyT3EY6=s$yD,v";
    InputStream stdInStub = new ByteArrayInputStream(pass.getBytes());
    when(obMocked.getLogger()).thenReturn(logger);
    when(obMocked.getStdIn()).thenReturn(stdInStub);

    assertThat("Returned password don't match typed one",
        readPasswordFromSysIn.getPassword(obMocked), is(pass.toCharArray()));

    ignore = verify(obMocked, times(1)).getLogger();
    ignore = verify(obMocked, times(1)).getStdIn();
  }
}
