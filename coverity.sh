#!/bin/bash

echo ===================
echo Capture source code
echo -------------------
mvn clean
cov-build --dir /Applications/cov-analysis-macosx-2020.09/idirs/Lister \
  --delete-stale-tus \
  --return-emit-failures \
  mvn -B package -DskipTests
cov-capture --dir /Applications/cov-analysis-macosx-2020.09/idirs/Lister \
  --return-emit-failures --delete-stale-tus \
  --source-dir /Users/kkoval/Documents/Repos/Lister \
  --language php --language ruby --language python \
  --language javascript --language typescript

echo ================================
echo Removing unnecessary source code
echo --------------------------------
cov-manage-emit --dir /Applications/cov-analysis-macosx-2020.09/idirs/Lister \
  --tu-pattern "file('Lister/src/test/')" --tu-pattern "file('\.m2/repository/')" \
  --tu-pattern "file('Lister/repo/')" --tu-pattern "file('Lister/src/test/')" \
  --tu-pattern "file('Lister/target/test-classes/')" --tu-pattern "file('Lister/JavaDock/')" \
  --tu-pattern "file('Applications/cov-analysis-macosx-')" \
  delete

echo ========================
echo Importing SCM blame info
echo ------------------------
cov-import-scm --scm git \
  --dir /Applications/cov-analysis-macosx-2020.09/idirs/Lister \
  --ms-delay 10

echo =====================
echo Analyzing source code
echo ---------------------
cov-analyze --dir /Applications/cov-analysis-macosx-2020.09/idirs/Lister \
  --ticker-mode none \
  -sf /Applications/cov-analysis-macosx-2020.09/bin/license.dat \
  --webapp-security --distrust-database --distrust-filesystem \
  --strip-path /Users/kkoval/Documents/Repos/Lister \
  --all --concurrency --security --enable-parse-warnings --enable-jshint --rule \
  --enable-constraint-fpp --no-field-offset-escape --inherit-taint-from-unions \
  --enable-fb --fb-include low-priority

echo ===========================
echo Commiting results to server
echo ---------------------------
cov-commit-defects --dir /Applications/cov-analysis-macosx-2020.09/idirs/Lister \
  -sf /Applications/cov-analysis-macosx-2020.09/bin/license.dat \
  --description "Manually triggered" \
  --target fixes \
  --version 1.3-SNAPSHOT \
  --url https://sca-w10-vm1.vlab.lohika.com:8443 \
  --stream Lister \
  --auth-key-file /Users/kkoval/Documents/Repos/dr-agent-coverity/authkeys/ak-sca-w10-vm1.vlab.lohika.com-8443-commiter@local \
  --ticker-mode none

echo ========================
echo Running Dependency-check
echo ------------------------
dependency-check.sh Core \
  -s /Users/kkoval/Documents/Repos/Lister/ \
  --project Lister \
  --disableAssembly \
  --enableExperimental \
  -o /Users/kkoval/Documents/Repos/Lister/target \
  -f HTML
